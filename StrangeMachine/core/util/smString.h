#ifndef SM_CORE_UTIL_STRING_H
#define SM_CORE_UTIL_STRING_H

#include "smpch.h"

#include "core/data/smArray.h"
#include "core/smMem.h"

#define SM_STRING i8 *

#define SM_STRING_MALLOC(SIZE)        SM_MALLOC(sizeof(i8) * SIZE);
#define SM_STRING_CALLOC(NMEMB, SIZE) SM_CALLOC(NMEMB, sizeof(i8) * SIZE)
#define SM_STRING_REALLOC(STR, SIZE)  SM_REALLOC(STR, SIZE)
#define SM_STRING_FREE(STR)           SM_FREE(STR)

#ifdef SM_DEBUG
SM_STRING sm__string_dup(const SM_STRING s);
  #define SM_STRING_DUP(STR) sm__string_dup(STR)
#else
  #define SM_STRING_DUP(STR) strdup(STR)
#endif

SM_ARRAY(SM_STRING) strsplit(const SM_STRING string, i8 delim);
SM_STRING str_get_filename(const SM_STRING file_path);
SM_STRING str_get_path(const SM_STRING file_path);
SM_STRING sm_string_trim_white_space(SM_STRING str);

#endif /* SM_CORE_UTIL_STRING_H */
