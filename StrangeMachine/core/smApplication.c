#include "smpch.h"

#include "core/smBase.h"
#include "core/smLog.h"

#include "core/smAssert.h"
#include "core/smMem.h"
#include "core/smStackLayer.h"
#include "core/smTime.h"
#include "core/smTimer.h"
#include "core/smWindow.h"
#include "core/util/smBitMask.h"

#include "renderer/smRenderer.h"

#include "cimgui/smCimgui.h"

#include "resource/smResource.h"

#include "smInput.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "APPLICATION"

#ifdef SM_DEBUG
static void sm_at_exit(void)
{
    sm__mem_print( );
}
#endif

typedef struct sm__application_s
{

    sm_window_s *window;
    struct sm_stack_layer_s *stack;

    sm_cimgui_s *cimgui;

    enum
    {
        SM_APPLICATION_FLAG_RUNNING = 1 << 0,
        SM_APPLICATION_FLAG_MINIMIZED = 1 << 1,

    } flags;

    struct sm_timer_s *timer;

    struct
    {
        const f32 avg_interval_sec;
        u32 num_frames;
        f64 accumulated_time;
        f32 current_fps;
    } fps2;

    f32 desired_fps;
    u64 frames;

    f64 delta;
    f64 fps;

} sm_application_s;

/* private functions */
SM_PRIVATE
void sm__application_cap_frame_rate(sm_application_s *app, long *then, float *remainder);

SM_PRIVATE
void sm__application_status_gui(sm_application_s *app);

sm_application_s *sm_application_new(void)
{

    static b8 instanced = false;
    SM_CORE_ASSERT(!instanced && "application already instanced");

    sm_application_s *app = SM_CALLOC(1, sizeof(sm_application_s));
    SM_CORE_ASSERT(app);

    instanced = true;

    return app;
}

b8 sm_application_on_event(sm_event_s *event, void *user_data);
b8 sm_application_on_window_close(sm_event_s *event, void *user_data);
b8 sm_application_on_window_resize(sm_event_s *event, void *user_data);
void sm_application_push_overlay(sm_application_s *app, struct sm_layer_s *layer);

b8 sm_application_ctor(sm_application_s *app, const char *name)
{

    SM_CORE_ASSERT(app);

#ifdef SM_DEBUG
    atexit(sm_at_exit);
#endif

    /* log_set_level(LOG_DEBUG); */

    app->window = window_new( );
    if (!sm_window_ctor(app->window, name, 800.f, 600.f))
    {
        SM_CORE_LOG_ERROR("failed to initialize window");
        return false;
    }

    sm_window_set_callback(app->window, sm_application_on_event, app);
    sm_window_set_relative_mouse(app->window, true);

    input_init( );
    sm_resource_manager_init("assets/");

    app->cimgui = sm_cimgui_new( );
    if (!sm_cimgui_ctor(app->cimgui, app->window))
    {
        SM_CORE_LOG_ERROR("failed to initialize cimgui");
        return false;
    }

    struct sm_stack_layer_s *stack = sm_stack_layer_new( );
    if (!sm_stack_layer_ctor(stack))
    {
        SM_CORE_LOG_ERROR("failed to initialize stack layer");
        return false;
    }
    app->stack = stack;

    sm_application_push_overlay(app, app->cimgui);

    sm_event_category_e mask = SM_CATEGORY_WINDOW;
    sm_event_set_print_mask(mask);

    app->timer = sm_timer_new( );
    sm_timer_ctor(app->timer);

    sm_renderer_init( );
    sm_renderer2_init( );
    sm_renderer2_on_resize(800, 600);

    app->delta = 0.0;
    app->flags = SM_APPLICATION_FLAG_RUNNING;
    app->desired_fps = 24;

    return true;
}

void sm_application_dtor(sm_application_s *app)
{

    SM_CORE_ASSERT(app);

    sm_renderer2_teardown( );
    sm_renderer_teardown( );

    sm_timer_dtor(app->timer);

    sm_stack_layer_dtor(app->stack);

    sm_cimgui_dtor(app->cimgui);

    sm_resource_manager_teardown( );

    input_tear_down( );

    sm_window_dtor(app->window);

    SM_FREE(app);
}

b8 sm_application_on_event(sm_event_s *event, void *user_data)
{

    sm_event_print(event);

    SM_CORE_ASSERT(event);
    SM_CORE_ASSERT(user_data);

    sm_application_s *app = (sm_application_s *)user_data;

    sm_event_dispatch(event, SM_EVENT_WINDOW_CLOSE, sm_application_on_window_close, app);
    sm_event_dispatch(event, SM_EVENT_WINDOW_RESIZE, sm_application_on_window_resize, NULL);

    if (SM_MASK_CHK(event->category, SM_CATEGORY_KEYBOARD | SM_CATEGORY_MOUSE))
    {

        /* Check if the event was handled by the imgui layer, otherwise pass it to the input system */
        event->handled |= sm_layer_event(app->cimgui, event);
        if (!event->handled) event->handled |= input_on_event(event, NULL);

        return event->handled;
    }

    size_t stack_size = sm_stack_layer_get_size(app->stack);
    for (size_t i = stack_size; i > 0; --i)
    {
        struct sm_layer_s *layer = sm_stack_layer_get_layer(app->stack, i - 1);
        if (event->handled) break;
        sm_layer_event(layer, event);
    }

    return event->handled;
}

void sm_application_do(sm_application_s *app)
{

    SM_CORE_ASSERT(app);

    long then = SM_GET_TICKS( );
    f32 remainder = 0;

    while (SM_MASK_CHK(app->flags, SM_APPLICATION_FLAG_RUNNING))
    {

        sm_timer_reset(app->timer);

        input_do( );
        sm_window_do(app->window);

        static b8 grab_mouse = true;
        if (grab_mouse && input_scan_key_lock(sm_key_escape))
        {
            grab_mouse = false;
            sm_window_set_relative_mouse(app->window, false);
        }
        if (!grab_mouse && input_scan_key_lock(sm_button_left))
        {
            grab_mouse = true;
            sm_window_set_relative_mouse(app->window, true);
        }

        if (!grab_mouse)
        {
            goto swap;
        }

        sm_resource_manager_do((f32)app->delta);
        size_t stack_size = sm_stack_layer_get_size(app->stack);
        for (size_t i = 0; i < stack_size; ++i)
        {
            struct sm_layer_s *layer = sm_stack_layer_get_layer(app->stack, i);
            sm_layer_update(layer, (f32)app->delta);
        }

        sm_cimgui_begin(app->cimgui);

        stack_size = sm_stack_layer_get_size(app->stack);
        for (size_t i = 0; i < stack_size; ++i)
        {
            struct sm_layer_s *layer = sm_stack_layer_get_layer(app->stack, i);
            sm_layer_gui(layer);
        }

        sm__application_status_gui(app);

        sm_cimgui_end(app->cimgui);

    swap:
        sm_window_swap_buffers(app->window);

        sm__application_cap_frame_rate(app, &then, &remainder);

        app->delta = sm_timer_get_elapsed(app->timer);

        if (!(app->frames % 16))
        {
            app->fps = (1.f / app->delta);
        }

        app->frames++;

        if (errno)
        {
            SM_CORE_LOG_ERROR("errno: %d", errno);
            errno = 0;
        }
    }

#ifdef SM_DEBUG

    SM_CORE_ASSERT(!sm_application_is_running(app));

#endif
}

b8 sm_application_on_window_close(sm_event_s *event, void *user_data)
{

    SM_CORE_ASSERT(event);
    SM_CORE_ASSERT(user_data);

    sm_application_s *app = (sm_application_s *)user_data;

    sm_application_set_running(app, false);

    return true;
}

void sm_application_set_running(sm_application_s *app, b8 running)
{
    if (running) SM_MASK_SET(app->flags, SM_APPLICATION_FLAG_RUNNING);
    else SM_MASK_CLR(app->flags, (u32)SM_APPLICATION_FLAG_RUNNING);
}

b8 sm_application_is_running(sm_application_s *app)
{
    return SM_MASK_CHK(app->flags, SM_APPLICATION_FLAG_RUNNING);
}

void sm_application_set_minimized(sm_application_s *app, b8 minimized)
{
    if (minimized) SM_MASK_SET(app->flags, SM_APPLICATION_FLAG_MINIMIZED);
    else SM_MASK_CLR(app->flags, (u32)SM_APPLICATION_FLAG_MINIMIZED);
}

b8 sm_application_is_minimized(sm_application_s *app)
{
    return SM_MASK_CHK(app->flags, SM_APPLICATION_FLAG_MINIMIZED);
}

b8 sm_application_on_window_resize(sm_event_s *event, void *user_data)
{

    SM_CORE_ASSERT(event);

    sm_renderer2_on_resize(event->window.width, event->window.height);
    // sm_renderer_on_resize(event->window.width, event->window.height);

    return true;
}

void sm_application_push_layer(sm_application_s *app, struct sm_layer_s *layer)
{

    SM_CORE_ASSERT(app);
    SM_CORE_ASSERT(layer);

    sm_stack_layer_push(app->stack, layer);
    sm_layer_attach(layer);
}

void sm_application_push_overlay(sm_application_s *app, struct sm_layer_s *layer)
{

    SM_CORE_ASSERT(app);
    SM_CORE_ASSERT(layer);

    sm_stack_layer_push_overlay(app->stack, layer);
    sm_layer_attach(layer);
}

void sm_application_set_desired_fps(sm_application_s *app, f32 fps)
{

    SM_CORE_ASSERT(app);
    SM_CORE_ASSERT(fps > 0);

    app->desired_fps = fps;
}

f32 sm_application_get_desired_fps(sm_application_s *app)
{

    SM_CORE_ASSERT(app);

    return app->desired_fps;
}

SM_PRIVATE
void sm__application_cap_frame_rate(sm_application_s *app, long *then, float *remainder)
{
    long wait, frameTime;
    wait = (long int)((1000.0f / app->desired_fps) + *remainder);

    *remainder -= fabsf(*remainder);

    frameTime = SM_GET_TICKS( ) - *then;

    wait -= frameTime;
    if (wait < 1)
    {
        wait = 1;
    }
    SM_DELAY((u32)wait);

    *remainder += 0.667f;

    *then = SM_GET_TICKS( );
}

SM_PRIVATE
void sm__application_status_gui(sm_application_s *app)
{
    i32 corner = (app != NULL) ? 1 : 0;

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings |
                                    ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
    if (corner != -1)
    {
        const f32 PAD = 10.0f;
        const ImGuiViewport *viewport = igGetMainViewport( );
        ImVec2 work_pos = viewport->WorkPos; // Use work area to avoid menu-bar/task-bar, if any!
        ImVec2 work_size = viewport->WorkSize;
        ImVec2 window_pos, window_pos_pivot;
        window_pos.x = (corner & 1) ? (work_pos.x + work_size.x - PAD) : (work_pos.x + PAD);
        window_pos.y = (corner & 2) ? (work_pos.y + work_size.y - PAD) : (work_pos.y + PAD);
        window_pos_pivot.x = (corner & 1) ? 1.0f : 0.0f;
        window_pos_pivot.y = (corner & 2) ? 1.0f : 0.0f;
        igSetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
        window_flags |= ImGuiWindowFlags_NoMove;
    }

    igSetNextWindowBgAlpha(0.35f); // Transparent background
    if (igBegin("Application status overlay", NULL, window_flags))
    {
        igText("Average %.3f ms/frame (%.1f FPS)", 1000.0 / app->fps, app->fps);
        igText("Delta: %f", app->delta);
        igText("Total Frames: %u", app->frames);
        igText("Vsync: %s", sm_window_is_vsync(app->window) ? "true" : "false");
        igText("Mem: %.2f KB", ((f64)(get_bytes_in_use( )) / ((f64)(1 << 10))));
    }
    igEnd( );
}

#undef SM_MODULE_NAME
