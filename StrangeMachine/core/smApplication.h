#ifndef SM_APPLICATION_H
#define SM_APPLICATION_H

#include "smpch.h"

#include "core/smLayer.h"

struct sm_application_s;

/* Allocate memorty for application */
struct sm__application_s *sm_application_new(void);

/* Constructor */
b8 sm_application_ctor(struct sm__application_s *app, const char *name);

/* Destructor */
void sm_application_dtor(struct sm__application_s *app);

/* Application main loop */
void sm_application_do(struct sm__application_s *app);

void sm_application_push_layer(struct sm__application_s *app, struct sm_layer_s *layer);
void sm_application_push_overlay(struct sm__application_s *app, struct sm_layer_s *layer);
void sm_application_set_desired_fps(struct sm__application_s *app, f32 fps);
f32 sm_application_get_desired_fps(struct sm__application_s *app);

void sm_application_set_running(struct sm__application_s *app, b8 running);
b8 sm_application_is_running(struct sm__application_s *app);
void sm_application_set_minimized(struct sm__application_s *app, b8 minimized);
b8 sm_application_is_minimized(struct sm__application_s *app);

#endif /* SM_APPLICATION_H */
