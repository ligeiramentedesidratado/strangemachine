#ifndef SM_CORE_WINDOW_H
#define SM_CORE_WINDOW_H

#include "smpch.h"

#include "smWindowPub.h"

typedef struct sm__window_s sm_window_s;

/* acllocate memory for window */
sm_window_s *window_new(void);

/* constructor */
b8 sm_window_ctor(sm_window_s *win, const char *name, u32 width, u32 height);

/* destructor */
void sm_window_dtor(sm_window_s *win);

f32 sm_window_get_aspect_ratio(sm_window_s *win);
u32 sm_window_get_width(sm_window_s *win);
u32 sm_window_get_height(sm_window_s *win);
void sm_window_set_vsync(sm_window_s *win, b8 vsync);
b8 sm_window_is_vsync(sm_window_s *win);
void sm_window_set_relative_mouse(sm_window_s *win, b8 rel_mouse);
b8 sm_window_is_relative_mouse(sm_window_s *win);

void sm_window_set_callback(sm_window_s *win, event_callback_f callback, void *user_data);
void sm_window_do(sm_window_s *win);
void sm_window_swap_buffers(sm_window_s *win);
const void *sm_window_get_window_raw(sm_window_s *win);
const void *sm_window_get_context_raw(sm_window_s *win);

#endif /* SM_CORE_WINDOW_H */
