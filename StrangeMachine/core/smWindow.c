#include "smpch.h"

#include "event/smEvent.h"

#include "core/smAssert.h"
#include "core/smBase.h"
#include "core/smMem.h"
#include "core/smWindow.h"
#include "core/smWindowPub.h"

#include "renderer/GL/smGL.h"

#include "cimgui/smCimguiImpl.h"

#include <SDL2/SDL.h>

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "WINDOW"

typedef struct sm__window_s
{

    SDL_Window *raw_window;
    SDL_GLContext raw_context;
    u32 width, height;

    enum
    {
        SM_WINDOW_FLAG_VSYNC = 1 << 0,

        SM_WINDOW_FLAG_RELATIVE_MOUSE = 1 << 6,

    } flags;

    event_callback_f event_callback;
    void *user_data; // temporary

} sm_window_s;

sm_window_s *window_new(void)
{

    sm_window_s *win = SM_CALLOC(1, sizeof(sm_window_s));
    SM_CORE_ASSERT(win);

    return win;
}

b8 sm_window_ctor(sm_window_s *win, const char *name, u32 width, u32 height)
{

    SM_CORE_ASSERT(win);
    SM_CORE_ASSERT(name);

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        SM_CORE_LOG_ERROR("SDL_Init Error: %s", SDL_GetError( ));
        return false;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    win->raw_window =
        SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (i32)width, (i32)height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    if (win->raw_window == NULL)
    {
        SM_CORE_LOG_ERROR("SDL_CreateWindow Error: %s", SDL_GetError( ));
        return false;
    }

    win->raw_context = SDL_GL_CreateContext(win->raw_window);
    if (win->raw_context == NULL)
    {
        SM_CORE_LOG_ERROR("SDL_GL_CreateContext Error: %s", SDL_GetError( ));
        return false;
    }

    /* TODO: LOAD OPENGL FUNCTIONS PROPERLY */
    if (SDL_GL_MakeCurrent(win->raw_window, win->raw_context) != 0)
    {
        SM_CORE_LOG_ERROR("SDL_GL_MakeCurrent Error: %s", SDL_GetError( ));
        return false;
    }

    if (!sm_gl_load_proc(SDL_GL_GetProcAddress))
    {
        SM_CORE_LOG_ERROR("failed to load GL functions: %s\n", SDL_GetError( ));
        return false;
    }

    sm_window_set_vsync(win, true);

    win->width = width;
    win->height = height;

    SM_LOG_INFO("Window created: %s", name);

    return true;
}

void sm_window_dtor(sm_window_s *win)
{

    SM_CORE_ASSERT(win);
    SDL_GL_DeleteContext(win->raw_context);
    SDL_DestroyWindow(win->raw_window);
    SDL_Quit( );

    SM_FREE(win);
}

void sm_window_do(sm_window_s *win)
{

    SM_CORE_ASSERT(win);
    SM_CORE_ASSERT(win->event_callback);

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {

        ImGui_ImplSDL2_ProcessEvent(&event);

        switch (event.type)
        {
        case SDL_QUIT:
            {
                sm_event_s e = SM_EVENT_NEW_WINDOW(SM_EVENT_WINDOW_CLOSE, 0, 0);
                win->event_callback(&e, win->user_data);
                break;
            }
        case SDL_WINDOWEVENT:
            switch (event.window.event)
            {
            case SDL_WINDOWEVENT_RESIZED:
                {
                    win->width = (u32)event.window.data1;
                    win->height = (u32)event.window.data2;
                    sm_event_s e = SM_EVENT_NEW_WINDOW(SM_EVENT_WINDOW_RESIZE, event.window.data1, event.window.data2);
                    win->event_callback(&e, win->user_data);
                    break;
                }
            case SDL_WINDOWEVENT_FOCUS_GAINED:
                {
                    sm_event_s e = SM_EVENT_NEW_WINDOW(SM_EVENT_WINDOW_FOCUS, 0, 0);
                    win->event_callback(&e, win->user_data);
                    break;
                }
            case SDL_WINDOWEVENT_FOCUS_LOST:
                {
                    sm_event_s e = SM_EVENT_NEW_WINDOW(SM_EVENT_WINDOW_UNFOCUS, 0, 0);
                    win->event_callback(&e, win->user_data);
                    break;
                }
            case SDL_WINDOWEVENT_CLOSE:
                {
                    if (event.window.windowID == SDL_GetWindowID(win->raw_window))
                    {
                        sm_event_s e = SM_EVENT_NEW_WINDOW(SM_EVENT_WINDOW_CLOSE, 0, 0);
                        win->event_callback(&e, win->user_data);
                    }
                    break;
                }
            }
            break;
        case SDL_KEYDOWN:
            {
                sm_event_s e = SM_EVENT_NEW_KEY(SM_EVENT_KEY_DOWN, (u16)event.key.keysym.scancode);
                win->event_callback(&e, win->user_data);
                break;
            }
        case SDL_KEYUP:
            {
                sm_event_s e = SM_EVENT_NEW_KEY(SM_EVENT_KEY_UP, (u16)event.key.keysym.scancode);
                win->event_callback(&e, win->user_data);
                break;
            }
        case SDL_MOUSEMOTION:
            {
                sm_event_s e = SM_EVENT_NEW_MOUSE(SM_EVENT_MOUSE_MOVE, 0, event.motion.x, event.motion.y, event.motion.xrel, event.motion.yrel, 0.0f);
                win->event_callback(&e, win->user_data);
                break;
            }
        case SDL_MOUSEBUTTONDOWN:
            {
                sm_event_s e = SM_EVENT_NEW_MOUSE(SM_EVENT_MOUSE_DOWN, event.button.button, event.button.x, event.button.y, 0, 0, 0.0f);
                win->event_callback(&e, win->user_data);
                break;
            }
        case SDL_MOUSEBUTTONUP:
            {
                sm_event_s e = SM_EVENT_NEW_MOUSE(SM_EVENT_MOUSE_UP, event.button.button, event.button.x, event.button.y, 0, 0, 0.0f);
                win->event_callback(&e, win->user_data);
                break;
            }
        case SDL_MOUSEWHEEL:
            {
                sm_event_s e = SM_EVENT_NEW_MOUSE(SM_EVENT_MOUSE_WHEEL, 0, 0, 0, 0, 0, event.wheel.preciseY);
                win->event_callback(&e, win->user_data);
                break;
            }
        }
    }
}

void sm_window_set_callback(sm_window_s *win, event_callback_f callback, void *user_data)
{

    SM_CORE_ASSERT(win);
    SM_CORE_ASSERT(callback);

    win->event_callback = callback;
    win->user_data = user_data;
}

f32 sm_window_get_aspect_ratio(sm_window_s *win)
{

    SM_CORE_ASSERT(win);

    return (f32)win->width / (f32)win->height;
}

u32 sm_window_get_width(sm_window_s *win)
{

    SM_CORE_ASSERT(win);

    return win->width;
}

u32 sm_window_get_height(sm_window_s *win)
{

    SM_CORE_ASSERT(win);

    return win->height;
}

void sm_window_set_vsync(sm_window_s *win, b8 vsync)
{

    SM_CORE_ASSERT(win);

    if (vsync) SM_MASK_SET(win->flags, SM_WINDOW_FLAG_VSYNC);
    else SM_MASK_CLR(win->flags, (u32)SM_WINDOW_FLAG_VSYNC);

    if (SDL_GL_SetSwapInterval(vsync) < 0)
    {
        SM_CORE_LOG_WARN("SDL_GL_SetSwapInterval Error: %s", SDL_GetError( ));
    }
}

b8 sm_window_is_vsync(sm_window_s *win)
{
    SM_CORE_ASSERT(win);

    return SM_MASK_CHK(win->flags, SM_WINDOW_FLAG_VSYNC);
}

void sm_window_set_relative_mouse(sm_window_s *win, b8 rel_mouse)
{

    SM_CORE_ASSERT(win);

    if (rel_mouse) SM_MASK_SET(win->flags, SM_WINDOW_FLAG_RELATIVE_MOUSE);
    else SM_MASK_CLR(win->flags, (u32)SM_WINDOW_FLAG_RELATIVE_MOUSE);

    if (SDL_SetRelativeMouseMode(rel_mouse) < 0)
    {
        SM_CORE_LOG_WARN("SDL_SetRelativeMouseMode Error: %s", SDL_GetError( ));
    }
}

b8 sm_window_is_relative_mouse(sm_window_s *win)
{
    SM_CORE_ASSERT(win);

    return SM_MASK_CHK(win->flags, SM_WINDOW_FLAG_RELATIVE_MOUSE);
}

void sm_window_swap_buffers(sm_window_s *win)
{

    SM_CORE_ASSERT(win);

    SDL_GL_SwapWindow(win->raw_window);
}

const void *sm_window_get_window_raw(sm_window_s *win)
{

    SM_CORE_ASSERT(win);

    return win->raw_window;
}

const void *sm_window_get_context_raw(sm_window_s *win)
{

    SM_CORE_ASSERT(win);

    return win->raw_context;
}

#undef SM_MODULE_NAME
