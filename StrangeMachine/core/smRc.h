#ifndef SM_CORE_REFERENCES_H
#define SM_CORE_REFERENCES_H

#include "smpch.h"

#define SM_ATOMIC 0

typedef struct sm__rc_s
{
#if SM_ATOMIC
    atomic_uint count;
#else
    u32 count;
#endif

} sm_rc_s;

static inline void sm_rc_inc(sm_rc_s *ref)
{
#if SM_ATOMIC
    atomic_fetch_add(&ref->count, 1);
#else
    ref->count++;
#endif
}

static inline void sm_rc_dec(sm_rc_s *ref)
{
#if SM_ATOMIC
    atomic_fetch_sub(&ref->count, 1);
#else
    ref->count--;
#endif
}

static inline void sm_rc_store(sm_rc_s *ref, u32 value)
{
#if SM_ATOMIC
    atomic_store(&ref->count, value);
#else
    ref->count = value;
#endif
}

static inline u32 sm_rc_load(sm_rc_s *ref)
{
#if SM_ATOMIC
    return atomic_load(&ref->count);
#else
    return ref->count;
#endif
}

#undef SM_ATOMIC

#endif /* SM_CORE_REFERENCES_H */
