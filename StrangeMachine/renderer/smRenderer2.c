#include "smpch.h"

#include "core/smCore.h"
#include "ecs/smComponents.h"
#include "math/smMath.h"
#include "renderer/GL/smGL.h"
#include "renderer/smRenderer.h"

typedef struct sm__renderer_vertex_buffer_s
{
    u32 element_count; // Number of elements in the buffer (QUADS)

    SM_ARRAY(sm_vec3) positions;
    SM_ARRAY(sm_vec2) uvs;
    SM_ARRAY(sm_vec4) colors;
    SM_ARRAY(sm_vec3) normals;
    SM_ARRAY(u32) indices;

    u32 vao;     /* openGL vertex array object */
    u32 vbos[4]; /* openGL vertex buffer objects */
    u32 ebo;     /* openGL vertex buffer object */

} sm_renderer_vertex_buffer_s;

typedef struct sm__renderer_draw_call_s
{
    sm_draw_mode_e mode;  // Drawing mode: LINES, TRIANGLES, QUADS
    u32 vertex_counter;   // Number of vertex of the draw
    u32 vertex_alignment; // Number of vertex required for index alignment (LINES, TRIANGLES)

    sm_resource_entity_s texture;

} sm_renderer_draw_call_s;

typedef struct sm__renderer_batch_s
{
    sm_renderer_vertex_buffer_s *vertex_buffer;
    SM_ARRAY(sm_renderer_draw_call_s) draws; // Draw calls array, depends on textureId
    f32 current_depth;                       // Current depth value for next draw

} sm_renderer_batch_s;

sm_renderer_batch_s sm__renderer_batch_new(void);
void sm__renderer_batch_dtor(sm_renderer_batch_s *batch);

void sm_renderer2_mat4_pop(void);
void sm_renderer2_mat4_push(void);

typedef struct sm__renderer_s
{
    sm_renderer_batch_s batch;

    struct
    {
        u32 vertex_counter;
        sm_vec2 uv;
        sm_vec3 normal;
        sm_vec4 color;

        enum sm_matrix_mode_e
        {
            SM_RENDERER_MATRIX_PROJECTION,
            SM_RENDERER_MATRIX_MODEL_VIEW,

            SM__RENDERER_MATRIX_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
        } matrix_mode;

        bool transform_required;
        sm_mat4 transform;

        sm_mat4 *current_matrix;
        sm_mat4 modelview;
        sm_mat4 projection;

        u32 stack_counter;
        f64 a, b, c, d;
        sm_mat4 stack[32]; // matrix stack for push/pop

        i32 shader_locs[SM_SHADER_LOC_MAX];

        sm_resource_entity_s default_texture;
        sm_resource_entity_s default_shader;

    } state;

} sm_renderer_s;

sm_renderer_s *RENDERER = NULL;

void sm_renderer2_print(void)
{

    return;
    printf("STATE:\n");
    printf("\tvertex_counter: %d\n", RENDERER->state.vertex_counter);
    printf("\tmatrix_mode: %d\n", RENDERER->state.matrix_mode);
    sm_mat4_print((*RENDERER->state.current_matrix));
    printf("\ttransform_required: %d\n", RENDERER->state.transform_required);
    printf("\tstack_counter: %d\n", RENDERER->state.stack_counter);
    printf("\n");
    printf("BATCH:\n");
    printf("\tdraw_calls length: %lu\n", SM_ARRAY_LEN(RENDERER->batch.draws));
    for (size_t i = 0; i < SM_ARRAY_LEN(RENDERER->batch.draws); ++i)
    {
        printf("\tDRAW: %zu:\n", i);
        sm_renderer_draw_call_s *d_call = &RENDERER->batch.draws[i];
        printf("\t\tvertex_counter: %u\n", d_call->vertex_counter);
        printf("\t\tertex_alignment: %u\n", d_call->vertex_alignment);
        printf("\t\tmode: %u\n", d_call->mode);
    }
}

void sm_renderer2_init(void)
{

    RENDERER = SM_ALIGNED_ALLOC(16, sizeof(sm_renderer_s));

    memset(RENDERER, 0x0, sizeof(sm_renderer_s));

    // init default white texture
    u8 *pixels = SM_MALLOC(sizeof(u8) * 4);
    memcpy(pixels, (u8[4]){211, 211, 211, 255}, sizeof(u8) * 4);

    sm_resource_texture_s default_texture;

    default_texture.header = sm_resource_header_new(SM_RESOURCE_TYPE_TEXTURE, 24, "DefaultDiffuseMap2");
    default_texture.width = 1;
    default_texture.height = 1;
    default_texture.raw_data = pixels;
    default_texture.format = SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;
    default_texture.handle = 0;
    sm_rc_store(&default_texture.ref_count, 0);
    default_texture.flags = 0x0;

    sm_resource_texture_set_on_cpu(&default_texture, true);

    sm_resource_entity_s default_texture_entt = sm_resource_texture_new_from_mem(&default_texture);
    RENDERER->state.default_texture = sm_resource_texture_make_reference(default_texture_entt);

    sm_resource_texture_s *texture = sm_resource_manager_get_data(RENDERER->state.default_texture);
    sm_resource_texture_load_gpu(texture);

    if (SM_RESOURCE_IS_VALID(RENDERER->state.default_texture))
    {
        SM_LOG_INFO("[IDX %d] default texture loaded successfully to VRAM", RENDERER->state.default_texture.index);
    }
    else
    {
        SM_LOG_WARN("failed to load default texture");
    }

    sm_resource_entity_s default_shader_entt = sm_resource_manager_get_by_name("test", SM_RESOURCE_TYPE_SHADER);
    RENDERER->state.default_shader = sm_resource_shader_make_reference(default_shader_entt);

    sm_resource_shader_s *default_shader = sm_resource_manager_get_data(RENDERER->state.default_shader);
    sm_resource_shader_load_gpu(default_shader);

    if (SM_RESOURCE_IS_VALID(RENDERER->state.default_shader))
    {
        SM_LOG_INFO("[IDX %d] default shader loaded successfully", RENDERER->state.default_shader.index);
    }
    else
    {
        SM_LOG_WARN("failed to load default shader");
    }

    for (u32 i = 0; i < SM_SHADER_LOC_MAX; ++i) RENDERER->state.shader_locs[i] = -1;

    RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_POSITION] = sm_gl_shader_attribute_get_location(default_shader->program, "a_position");
    RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_UV] = sm_gl_shader_attribute_get_location(default_shader->program, "a_uv");
    RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_COLOR] = sm_gl_shader_attribute_get_location(default_shader->program, "a_color");
    RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_NORMAL] = sm_gl_shader_attribute_get_location(default_shader->program, "a_normal");

    // renderer_defaults.shader_locs[SM_SHADER_LOC_MATRIX_MODEL] = sm_gl_shader_uniform_get_location(default_shader->program, "u_model");
    RENDERER->state.shader_locs[SM_SHADER_LOC_MATRIX_PVM] = sm_gl_shader_uniform_get_location(default_shader->program, "u_pvm");
    RENDERER->state.shader_locs[SM_SHADER_LOC_COLOR_DIFFUSE] = sm_gl_shader_uniform_get_location(default_shader->program, "u_color_diffuse");
    RENDERER->state.shader_locs[SM_SHADER_LOC_MAP_DIFFUSE] = sm_gl_shader_uniform_get_location(default_shader->program, "u_tex0");

    // sm_resource_shader_bind(RENDERER->state.default_shader);
    // i32 value = 0;
    // sm_gl_shader_uniform_set(RENDERER->state.shader_locs[SM_SHADER_LOC_MAP_DIFFUSE], SM_I32, &value);
    // sm_resource_shader_ubind( );

    RENDERER->batch = sm__renderer_batch_new( );

    // init stack matrices
    glm_mat4_identity_array(&RENDERER->state.stack[0].data, 32);

    RENDERER->state.modelview = sm_mat4_identity( );
    RENDERER->state.projection = sm_mat4_identity( );
    RENDERER->state.current_matrix = &RENDERER->state.modelview;

    RENDERER->state.transform_required = false;
    RENDERER->state.transform = sm_mat4_identity( );

    // return;

    sm_gl_depth_func(SM_LEQUAL);  // Type of depth testing to apply
    sm_gl_disable(SM_DEPTH_TEST); // Disable depth testing for 2D (only used for 3D)

    // Init state: Blending mode
    sm_gl_blend_func(SM_SRC_ALPHA, SM_ONE_MINUS_SRC_ALPHA); // Color blending function (how colors are mixed)
    sm_gl_enable(SM_BLEND);                                 // Enable color blending (required to work with transparencies)

    // Init state: Culling
    // NOTE: All shapes/models triangles are drawn CCW
    sm_gl_cull_face(SM_BACK);   // Cull the back face (default)
    sm_gl_front_face(SM_CCW);   // Front face are defined counter clockwise (default)
    sm_gl_enable(SM_CULL_FACE); // Enable backface culling

    sm_gl_clear_color(sm_vec4_new(0.0f, 0.0f, 0.0f, 1.0f));
    sm_gl_clear_depth(1.0f);
    sm_gl_clear(SM_DEPTH_BUFFER_BIT | SM_COLOR_BUFFER_BIT);
}

void sm_renderer2_teardown(void)
{

    SM_ASSERT(RENDERER);

    sm__renderer_batch_dtor(&RENDERER->batch);

    SM_ALIGNED_FREE(RENDERER);
}

sm_renderer_batch_s sm__renderer_batch_new(void)
{

    sm_renderer_batch_s batch = {0};

#define QUAD_SIZE        4
#define INDICES_PER_QUAD 6

    sm_renderer_vertex_buffer_s *vertex_buffer = SM_CALLOC(1, sizeof(sm_renderer_vertex_buffer_s));

    vertex_buffer->element_count = 2048;
    SM_ARRAY_CTOR(vertex_buffer->positions, 2048 * QUAD_SIZE);
    SM_ARRAY_CTOR(vertex_buffer->uvs, 2048 * QUAD_SIZE);
    SM_ALIGNED_ARRAY_NEW(vertex_buffer->colors, 16, 2048 * QUAD_SIZE);
    SM_ARRAY_CTOR(vertex_buffer->normals, 2048 * QUAD_SIZE);
    SM_ARRAY_CTOR(vertex_buffer->indices, 2048 * INDICES_PER_QUAD);

    u32 k = 0;
    for (u32 i = 0; i < (2048 * INDICES_PER_QUAD); i += 6)
    {
        vertex_buffer->indices[i] = 4 * k;
        vertex_buffer->indices[i + 1] = 4 * k + 1;
        vertex_buffer->indices[i + 2] = 4 * k + 2;
        vertex_buffer->indices[i + 3] = 4 * k;
        vertex_buffer->indices[i + 4] = 4 * k + 2;
        vertex_buffer->indices[i + 5] = 4 * k + 3;

        k++;
    }

    batch.vertex_buffer = vertex_buffer;

    vertex_buffer->vao = sm_gl_vao_new( );
    sm_gl_vao_bind(vertex_buffer->vao);

    vertex_buffer->vbos[0] = sm_gl_vbo_new(vertex_buffer->positions, sizeof(sm_vec3) * 2048 * QUAD_SIZE, SM_DYNAMIC_DRAW);
    sm_gl_vertex_attribute_pointer((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_POSITION], 3, SM_F32, false, 0, 0);
    sm_gl_vertex_attribute_enable((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_POSITION]);

    vertex_buffer->vbos[1] = sm_gl_vbo_new(vertex_buffer->uvs, sizeof(sm_vec2) * 2048 * QUAD_SIZE, SM_DYNAMIC_DRAW);
    sm_gl_vertex_attribute_enable((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_UV]);
    sm_gl_vertex_attribute_pointer((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_UV], 2, SM_F32, false, 0, NULL);

    vertex_buffer->vbos[2] = sm_gl_vbo_new(vertex_buffer->colors, sizeof(sm_vec4) * 2048 * QUAD_SIZE, SM_DYNAMIC_DRAW);
    sm_gl_vertex_attribute_enable((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_COLOR]);
    sm_gl_vertex_attribute_pointer((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_COLOR], 4, SM_F32, false, 0, NULL);

    vertex_buffer->vbos[3] = sm_gl_vbo_new(vertex_buffer->colors, sizeof(sm_vec3) * 2048 * QUAD_SIZE, SM_DYNAMIC_DRAW);
    sm_gl_vertex_attribute_enable((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_NORMAL]);
    sm_gl_vertex_attribute_pointer((u32)RENDERER->state.shader_locs[SM_SHADER_LOC_VERTEX_NORMAL], 3, SM_F32, false, 0, NULL);

    // generate indices buffer
    vertex_buffer->ebo = sm_gl_ebo_new(vertex_buffer->indices, sizeof(u32) * 2048 * INDICES_PER_QUAD, SM_STATIC_DRAW);

    sm_gl_vao_unbind( );

    // just in case
    sm_gl_vbo_unbind( );
    sm_gl_ebo_unbind( );

#undef QUAD_SIZE
#undef INDICES_PER_QUAD

    if (vertex_buffer->vao > 0)
    {
        SM_LOG_TRACE("verttex buffer loaded successfully to VRAM");
    }
    batch.vertex_buffer = vertex_buffer;

    SM_ARRAY_CTOR(batch.draws, 64);

    for (u32 i = 0; i < SM_ARRAY_CAP(batch.draws); ++i)
    {
        batch.draws[i].mode = SM_QUADS;
        batch.draws[i].vertex_counter = 0;
        batch.draws[i].vertex_alignment = 0;
        batch.draws[i].texture = RENDERER->state.default_texture;
    }

    SM_ARRAY_SET_LEN(batch.draws, 1);

    batch.current_depth = -1.0f; // Reset depth value

    return batch;
}

void sm__renderer_batch_dtor(sm_renderer_batch_s *batch)
{

    sm_gl_vbo_unbind( );
    sm_gl_ebo_unbind( );

    sm_gl_vao_bind(batch->vertex_buffer->vao);
    sm_gl_vertex_attribute_disable(0);
    sm_gl_vertex_attribute_disable(1);
    sm_gl_vertex_attribute_disable(2);
    sm_gl_vertex_attribute_disable(3);
    sm_gl_vertex_attribute_disable(4);

    sm_gl_vao_unbind( );

    sm_gl_vbo_delete(batch->vertex_buffer->vbos[0]);
    sm_gl_vbo_delete(batch->vertex_buffer->vbos[1]);
    sm_gl_vbo_delete(batch->vertex_buffer->vbos[2]);
    sm_gl_vbo_delete(batch->vertex_buffer->vbos[3]);

    sm_gl_vbo_delete(batch->vertex_buffer->ebo);
    sm_gl_vao_delete(batch->vertex_buffer->vao);

    SM_ARRAY_DTOR(batch->vertex_buffer->positions);
    SM_ARRAY_DTOR(batch->vertex_buffer->uvs);
    SM_ALIGNED_ARRAY_DTOR(batch->vertex_buffer->colors);
    SM_ARRAY_DTOR(batch->vertex_buffer->normals);
    SM_ARRAY_DTOR(batch->vertex_buffer->indices);

    SM_FREE(batch->vertex_buffer);
    SM_ARRAY_DTOR(batch->draws);
}

void sm_renderer2_draw_batch( )
{
    if (RENDERER->state.vertex_counter > 0)
    {
        sm_gl_vao_bind(RENDERER->batch.vertex_buffer->vao);

        u32 v_counter = RENDERER->state.vertex_counter;

        sm_gl_vbo_sub_data(RENDERER->batch.vertex_buffer->vbos[0], RENDERER->batch.vertex_buffer->positions, v_counter * sizeof(sm_vec3), 0);
        sm_gl_vbo_sub_data(RENDERER->batch.vertex_buffer->vbos[1], RENDERER->batch.vertex_buffer->uvs, v_counter * sizeof(sm_vec2), 0);
        sm_gl_vbo_sub_data(RENDERER->batch.vertex_buffer->vbos[2], RENDERER->batch.vertex_buffer->colors, v_counter * sizeof(sm_vec4), 0);
        sm_gl_vbo_sub_data(RENDERER->batch.vertex_buffer->vbos[3], RENDERER->batch.vertex_buffer->normals, v_counter * sizeof(sm_vec3), 0);
        sm_gl_vbo_unbind( );

        sm_resource_shader_bind(RENDERER->state.default_shader);

        sm_mat4 mvp;
        glm_mat4_mul(RENDERER->state.projection.data, RENDERER->state.modelview.data, mvp.data);

        sm_gl_shader_uniform_set(RENDERER->state.shader_locs[SM_SHADER_LOC_MATRIX_PVM], SM_MAT4, mvp.float16);
        sm_gl_shader_uniform_set(RENDERER->state.shader_locs[SM_SHADER_LOC_COLOR_DIFFUSE], SM_VEC4, sm_vec4_one( ).data);

        i32 value = 0;
        sm_gl_shader_uniform_set(RENDERER->state.shader_locs[SM_SHADER_LOC_MAP_DIFFUSE], SM_I32, &value);

        sm_gl_texture_activate(0);

        for (u32 i = 0, vertex_offset = 0; i < SM_ARRAY_LEN(RENDERER->batch.draws); ++i)
        {
            sm_renderer_draw_call_s *d_call = &RENDERER->batch.draws[i];

            sm_resource_texture_s *text = sm_resource_manager_get_data(d_call->texture);
            sm_gl_texture_bind(text->handle);

            if (d_call->mode == SM_LINES || d_call->mode == SM_TRIANGLES)
            {
                sm_gl_draw(d_call->mode, (i32)vertex_offset, (i32)d_call->vertex_counter);
            }
            else
            {
                sm_gl_draw_elements(SM_TRIANGLES, d_call->vertex_counter / 4 * 6, (void *)(vertex_offset / 4 * 6 * sizeof(u32)));
            }

            vertex_offset += (d_call->vertex_counter + d_call->vertex_alignment);
        }

        sm_gl_texture_unbind( );
        sm_gl_vao_unbind( );

        sm_gl_shader_unbind( );
    }

    RENDERER->state.vertex_counter = 0;
    RENDERER->batch.current_depth = -1.0f;

    for (u32 i = 0; i < SM_ARRAY_CAP(RENDERER->batch.draws); ++i)
    {
        RENDERER->batch.draws[i].mode = SM_QUADS;
        RENDERER->batch.draws[i].vertex_counter = 0;
        RENDERER->batch.draws[i].texture = RENDERER->state.default_texture;
    }

    SM_ARRAY_SET_LEN(RENDERER->batch.draws, 1);
}

b8 sm_renderer_check_batch_limit(u32 vertices)
{
    SM_ASSERT(RENDERER);

    b8 overflow = false;

    if (RENDERER->state.vertex_counter + vertices >= RENDERER->batch.vertex_buffer->element_count * 4)
    {
        sm_draw_mode_e current_mode = RENDERER->batch.draws[SM_ARRAY_LEN(RENDERER->batch.draws) - 1].mode;
        sm_resource_entity_s current_texture = RENDERER->batch.draws[SM_ARRAY_LEN(RENDERER->batch.draws) - 1].texture;

        overflow = true;
        sm_renderer2_draw_batch( );

        RENDERER->batch.draws[SM_ARRAY_LEN(RENDERER->batch.draws) - 1].mode = current_mode;
        RENDERER->batch.draws[SM_ARRAY_LEN(RENDERER->batch.draws) - 1].texture = current_texture;
    }

    return overflow;
}

void sm_renderer2_begin(sm_draw_mode_e mode)
{

    u32 draw_counter = (u32)SM_ARRAY_LEN(RENDERER->batch.draws);
    sm_renderer_draw_call_s *d_call = &RENDERER->batch.draws[draw_counter - 1];

    if (d_call->mode != mode)
    {
        if (d_call->vertex_counter > 0)
        {

            if (d_call->mode == SM_LINES)
            {
                d_call->vertex_alignment = ((d_call->vertex_counter < 4) ? d_call->vertex_counter : d_call->vertex_counter % 4);
            }
            else if (d_call->mode == SM_TRIANGLES)
            {
                d_call->vertex_alignment = ((d_call->vertex_counter < 4) ? 1 : (4 - (d_call->vertex_counter % 4)));
            }
            else
            {
                d_call->vertex_alignment = 0;
            }

            if (!sm_renderer_check_batch_limit(d_call->vertex_alignment))
            {
                RENDERER->state.vertex_counter += d_call->vertex_alignment;
                draw_counter = (u32)SM_ARRAY_LEN(RENDERER->batch.draws) + 1;
            }
        }

        if (draw_counter >= SM_ARRAY_CAP(RENDERER->batch.draws))
        {
            sm_renderer2_draw_batch( );
        }
        SM_ARRAY_SET_LEN(RENDERER->batch.draws, draw_counter);

        d_call = &RENDERER->batch.draws[draw_counter - 1];

        d_call->mode = mode;
        d_call->vertex_counter = 0;
        d_call->texture = RENDERER->state.default_texture;
    }
}

void sm_renderer2_end(void)
{

    RENDERER->batch.current_depth += (1.0f / 20000.0f);

    if (RENDERER->state.vertex_counter >= (RENDERER->batch.vertex_buffer->element_count * 4 - 4))
    {
        for (u32 i = RENDERER->state.stack_counter; i >= 0; --i)
        {
            sm_renderer2_mat4_pop( );
        }

        sm_renderer2_draw_batch( );
    }
}

void sm_renderer2_vertex_vec3(sm_vec3 vertex)
{
    SM_ASSERT(RENDERER);

    sm_vec3 tvertex = vertex;
    if (RENDERER->state.transform_required)
    {
        glm_mat4_mulv3(RENDERER->state.transform.data, vertex.data, 1.0f, tvertex.data);
    }

    sm_renderer_vertex_buffer_s *v_buf = RENDERER->batch.vertex_buffer;
    if (RENDERER->state.vertex_counter < v_buf->element_count * 4)
    {
        glm_vec3_copy(tvertex.data, v_buf->positions[RENDERER->state.vertex_counter].data);
        glm_vec2_copy(RENDERER->state.uv.data, v_buf->uvs[RENDERER->state.vertex_counter].data);
        glm_vec4_copy(RENDERER->state.color.data, v_buf->colors[RENDERER->state.vertex_counter].data);
        glm_vec3_copy(RENDERER->state.normal.data, v_buf->normals[RENDERER->state.vertex_counter].data);

        RENDERER->state.vertex_counter++;
        RENDERER->batch.draws[SM_ARRAY_LEN(RENDERER->batch.draws) - 1].vertex_counter++;
    }
    else
    {
        SM_LOG_WARN("batch elements overflow");
    }
}

void sm_renderer2_vertex_vec2(sm_vec2 vertex)
{
    sm_renderer2_vertex_vec3(sm_vec3_new(vertex.x, vertex.y, RENDERER->batch.current_depth));
}

void sm_renderer2_vertex_uv_vec2(sm_vec2 uv)
{
    glm_vec2_copy(uv.data, RENDERER->state.uv.data);
}

void sm_renderer2_vertex_color_vec4(sm_vec4 color)
{
    glm_vec4_copy(color.data, RENDERER->state.color.data);
}

void sm_renderer2_mat4_load_identity(void)
{
    SM_ASSERT(RENDERER);

    *RENDERER->state.current_matrix = sm_mat4_identity( );
}

void sm_renderer2_mat4_mul(mat4 mat)
{
    SM_ASSERT(RENDERER);

    glm_mat4_mul(RENDERER->state.current_matrix->data, mat, RENDERER->state.current_matrix->data);
}

void sm_renderer2_mat4_mode(enum sm_matrix_mode_e mode)
{
    SM_ASSERT(RENDERER);

    if (mode == SM_RENDERER_MATRIX_PROJECTION) RENDERER->state.current_matrix = &RENDERER->state.projection;
    else if (mode == SM_RENDERER_MATRIX_MODEL_VIEW) RENDERER->state.current_matrix = &RENDERER->state.modelview;

    RENDERER->state.matrix_mode = mode;
}

void sm_renderer2_mat4_pop(void)
{
    SM_ASSERT(RENDERER);

    if (RENDERER->state.stack_counter > 0)
    {
        sm_mat4 m = RENDERER->state.stack[RENDERER->state.stack_counter - 1];
        *RENDERER->state.current_matrix = m;
        RENDERER->state.stack_counter--;
    }

    if (RENDERER->state.stack_counter == 0 && RENDERER->state.matrix_mode == SM_RENDERER_MATRIX_MODEL_VIEW)
    {
        RENDERER->state.current_matrix = &RENDERER->state.modelview;
        RENDERER->state.transform_required = false;
    }
}

void sm_renderer2_mat4_push(void)
{
    SM_ASSERT(RENDERER);

    if (RENDERER->state.stack_counter >= 32)
    {
        SM_LOG_WARN("matrix stack overflow");
        return;
    }

    if (RENDERER->state.matrix_mode == SM_RENDERER_MATRIX_MODEL_VIEW)
    {
        RENDERER->state.transform_required = true;
        RENDERER->state.current_matrix = &RENDERER->state.transform;
    }

    RENDERER->state.stack[RENDERER->state.stack_counter] = *RENDERER->state.current_matrix;
    RENDERER->state.stack_counter++;
}

void sm_draw_begin(void)
{
    SM_ASSERT(RENDERER);

    sm_renderer2_mat4_load_identity( );
}

void sm_draw_end(void)
{
    SM_ASSERT(RENDERER);

    sm_renderer2_draw_batch( );
}

void sm_renderer2_begin_3d(const sm_camera_s *camera)
{
    SM_ASSERT(RENDERER);

    sm_renderer2_draw_batch( );

    sm_renderer2_mat4_mode(SM_RENDERER_MATRIX_PROJECTION);
    sm_renderer2_mat4_push( );
    sm_renderer2_mat4_load_identity( );

    sm_mat4 proj;
    sm_mat4 view;
#if 1
    sm_camera_component_get_projection(camera, proj.data);
    sm_camera_component_get_view(camera, view.data);
#else
    f32 top = 0.1f * tanf(glm_rad(camera->fov * 05));
    f32 right = top * camera->aspect_ratio;

    glm_frustum(-right, right, -top, top, 0.1f, 100.f, proj.data);
    glm_lookat(camera->position.data, camera->target.data, camera->up.data, view.data);
#endif

    sm_renderer2_mat4_mul(proj.data);

    sm_renderer2_mat4_mode(SM_RENDERER_MATRIX_MODEL_VIEW);
    sm_renderer2_mat4_load_identity( );

    sm_renderer2_mat4_mul(view.data);

    sm_gl_enable(SM_DEPTH_TEST);
}

void sm_renderer2_end_3d(void)
{
    sm_renderer2_draw_batch( ); // Update and draw internal render batch

    sm_renderer2_mat4_mode(SM_RENDERER_MATRIX_PROJECTION);
    sm_renderer2_mat4_pop( ); // Restore previous matrix (projection) from matrix stack

    sm_renderer2_mat4_mode(SM_RENDERER_MATRIX_MODEL_VIEW);
    sm_renderer2_mat4_load_identity( );

    sm_gl_disable(SM_DEPTH_TEST);
}

void sm_renderer2_on_resize(i32 width, i32 height)
{

    sm_gl_viewport(0, 0, width, height);

    sm_renderer2_mat4_mode(SM_RENDERER_MATRIX_PROJECTION);
    sm_renderer2_mat4_load_identity( );

    sm_mat4 ortho;

    glm_ortho(0, width, height, 0, 0.0f, 1.0f, ortho.data);
    sm_renderer2_mat4_mul(ortho.data);

    sm_renderer2_mat4_mode(SM_RENDERER_MATRIX_MODEL_VIEW);
    sm_renderer2_mat4_load_identity( );
}

struct Texture
{
    unsigned int id;      // OpenGL texture id
    f32 width;            // Texture base width
    f32 height;           // Texture base height
    int mipmaps;          // Mipmap levels, 1 by default
    int format;           // Data format (PixelFormat type)
} texS = {1, 1, 1, 1, 7}; // Texture used on shapes drawing (usually a white pixel)

struct Rectangle
{
    f32 x;                            // Rectangle top-left corner position x
    f32 y;                            // Rectangle top-left corner position y
    f32 width;                        // Rectangle width
    f32 height;                       // Rectangle height
} texSRec = {0.0f, 0.0f, 1.0f, 1.0f}; // Texture source rectangle used on shapes drawing

void sm_renderer2_draw_rectangle(sm_vec2 position, sm_vec2 wh, sm_vec4 color)
{
    sm_vec2 topLeft = {0};
    sm_vec2 topRight = {0};
    sm_vec2 bottomLeft = {0};
    sm_vec2 bottomRight = {0};

    sm_vec2 origin = sm_vec2_zero( );

    f32 rotation = 0.0f;

    // Only calculate rotation if needed
    if (rotation == 0.0f)
    {
        f32 x = position.x - origin.x;
        f32 y = position.y - origin.y;
        topLeft = (sm_vec2){x, y};
        topRight = (sm_vec2){x + wh.x, y};
        bottomLeft = (sm_vec2){x, y + wh.y};
        bottomRight = (sm_vec2){x + wh.x, y + wh.y};
    }
    else
    {
        // float sinRotation = sinf(rotation * DEG2RAD);
        // float cosRotation = cosf(rotation * DEG2RAD);
        // float x = rec.x;
        // float y = rec.y;
        // float dx = -origin.x;
        // float dy = -origin.y;
        //
        // topLeft.x = x + dx * cosRotation - dy * sinRotation;
        // topLeft.y = y + dx * sinRotation + dy * cosRotation;
        //
        // topRight.x = x + (dx + rec.width) * cosRotation - dy * sinRotation;
        // topRight.y = y + (dx + rec.width) * sinRotation + dy * cosRotation;
        //
        // bottomLeft.x = x + dx * cosRotation - (dy + rec.height) * sinRotation;
        // bottomLeft.y = y + dx * sinRotation + (dy + rec.height) * cosRotation;
        //
        // bottomRight.x = x + (dx + rec.width) * cosRotation - (dy + rec.height) * sinRotation;
        // bottomRight.y = y + (dx + rec.width) * sinRotation + (dy + rec.height) * cosRotation;
    }

    sm_renderer_check_batch_limit(4);

    // rlSetTexture(texShapes.id);

    sm_renderer2_begin(SM_QUADS);

    // rlNormal3f(0.0f, 0.0f, 1.0f);
    sm_renderer2_vertex_color_vec4(color);

    sm_renderer2_vertex_uv_vec2(sm_vec2_new(texSRec.x / texS.width, texSRec.y / texS.height));
    sm_renderer2_vertex_vec2(sm_vec2_new(topLeft.x, topLeft.y));

    sm_renderer2_vertex_uv_vec2(sm_vec2_new(texSRec.x / texS.width, (texSRec.y + texSRec.height) / texS.height));
    sm_renderer2_vertex_vec2(sm_vec2_new(bottomLeft.x, bottomLeft.y));

    sm_renderer2_vertex_uv_vec2(sm_vec2_new((texSRec.x + texSRec.width) / texS.width, (texSRec.y + texSRec.height) / texS.height));
    sm_renderer2_vertex_vec2(sm_vec2_new(bottomRight.x, bottomRight.y));

    sm_renderer2_vertex_uv_vec2(sm_vec2_new((texSRec.x + texSRec.width) / texS.width, texSRec.y / texS.height));
    sm_renderer2_vertex_vec2(sm_vec2_new(topRight.x, topRight.y));

    sm_renderer2_end( );
}

void sm_renderer2_draw_circle(sm_vec2 center, f32 radius, f32 startAngle, f32 endAngle, u32 segments, sm_vec4 color)
{
    if (radius <= 0.0f) radius = 0.1f; // Avoid div by zero

    // Function expects (endAngle > startAngle)
    if (endAngle < startAngle)
    {
        // Swap values
        float tmp = startAngle;
        startAngle = endAngle;
        endAngle = tmp;
    }

    u32 minSegments = (u32)ceilf((endAngle - startAngle) / 90.0f);

    if (segments < minSegments)
    {
        // Calculate the maximum angle between segments based on the error rate (usually 0.5f)
        f32 th = acosf(2 * powf(1 - 0.5f / radius, 2) - 1);
        segments = (u32)((endAngle - startAngle) * ceilf(2 * GLM_PIf / th) / 360.0f);

        if (segments <= 0) segments = minSegments;
    }

    f32 stepLength = (endAngle - startAngle) / (float)segments;
    f32 angle = startAngle;

    sm_renderer_check_batch_limit(4 * segments / 2);

    // rlSetTexture(texShapes.id);

    sm_renderer2_begin(SM_QUADS);
    // NOTE: Every QUAD actually represents two segments
    for (u32 i = 0; i < segments / 2; i++)
    {
        sm_renderer2_vertex_color_vec4(color);

        sm_renderer2_vertex_uv_vec2(sm_vec2_new(texSRec.x / texS.width, texSRec.y / texS.height));
        sm_renderer2_vertex_vec2(sm_vec2_new(center.x, center.y));

        sm_renderer2_vertex_uv_vec2(sm_vec2_new(texSRec.x / texS.width, (texSRec.y + texSRec.height) / texS.height));
        sm_renderer2_vertex_vec2(sm_vec2_new(center.x + sinf(glm_rad(angle)) * radius, center.y + cosf(glm_rad(angle)) * radius));

        sm_renderer2_vertex_uv_vec2(sm_vec2_new((texSRec.x + texSRec.width) / texS.width, (texSRec.y + texSRec.height) / texS.height));
        sm_renderer2_vertex_vec2(
            sm_vec2_new(center.x + sinf(glm_rad(angle + stepLength)) * radius, center.y + cosf(glm_rad(angle + stepLength)) * radius));

        sm_renderer2_vertex_uv_vec2(sm_vec2_new((texSRec.x + texSRec.width) / texS.width, texSRec.y / texS.height));
        sm_renderer2_vertex_vec2(
            sm_vec2_new(center.x + sinf(glm_rad(angle + stepLength * 2)) * radius, center.y + cosf(glm_rad(angle + stepLength * 2)) * radius));

        angle += (stepLength * 2);
    }

    // NOTE: In case number of segments is odd, we add one last piece to the cake
    if (segments % 2)
    {
        sm_renderer2_vertex_color_vec4(color);

        sm_renderer2_vertex_uv_vec2(sm_vec2_new(texSRec.x / texS.width, texSRec.y / texS.height));
        sm_renderer2_vertex_vec2(sm_vec2_new(center.x, center.y));

        sm_renderer2_vertex_uv_vec2(sm_vec2_new(texSRec.x / texS.width, (texSRec.y + texSRec.height) / texS.height));
        sm_renderer2_vertex_vec2(sm_vec2_new(center.x + sinf(glm_rad(angle)) * radius, center.y + cosf(glm_rad(angle)) * radius));

        sm_renderer2_vertex_uv_vec2(sm_vec2_new((texSRec.x + texSRec.width) / texS.width, (texSRec.y + texSRec.height) / texS.height));
        sm_renderer2_vertex_vec2(
            sm_vec2_new(center.x + sinf(glm_rad(angle + stepLength)) * radius, center.y + cosf(glm_rad(angle + stepLength)) * radius));

        sm_renderer2_vertex_uv_vec2(sm_vec2_new((texSRec.x + texSRec.width) / texS.width, texSRec.y / texS.height));
        sm_renderer2_vertex_vec2(sm_vec2_new(center.x, center.y));
    }

    sm_renderer2_end( );
}

void sm_renderer2_draw_grid(i32 slices, f32 spacing)
{
    i32 halfSlices = slices / 2;

    sm_renderer_check_batch_limit((u32)(slices + 2) * 4);

    sm_renderer2_begin(SM_LINES);
    for (int i = -halfSlices; i <= halfSlices; i++)
    {
        if (i == 0)
        {
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.5f, 0.5f, 0.5f, 1.0f));
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.5f, 0.5f, 0.5f, 1.0f));
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.5f, 0.5f, 0.5f, 1.0f));
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.5f, 0.5f, 0.5f, 1.0f));
        }
        else
        {
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.75f, 0.75f, 0.75f, 1.0f));
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.75f, 0.75f, 0.75f, 1.0f));
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.75f, 0.75f, 0.75f, 1.0f));
            sm_renderer2_vertex_color_vec4(sm_vec4_new(0.75f, 0.75f, 0.75f, 1.0f));
        }

        sm_renderer2_vertex_vec3(sm_vec3_new((float)i * spacing, 0.0f, (float)-halfSlices * spacing));
        sm_renderer2_vertex_vec3(sm_vec3_new((float)i * spacing, 0.0f, (float)halfSlices * spacing));

        sm_renderer2_vertex_vec3(sm_vec3_new((float)-halfSlices * spacing, 0.0f, (float)i * spacing));
        sm_renderer2_vertex_vec3(sm_vec3_new((float)halfSlices * spacing, 0.0f, (float)i * spacing));
    }
    sm_renderer2_end( );
}

void sm_renderer2_draws_phere(sm_vec3 centerPos, f32 radius, u32 rings, u32 slices, sm_vec4 color)
{
    u32 numVertex = (rings + 2) * slices * 6;
    sm_renderer_check_batch_limit(numVertex);

    sm_renderer2_mat4_push( );
    // NOTE: Transformation is applied in inverse order (scale -> translate)
    glm_translate(RENDERER->state.current_matrix->data, centerPos.data);

    sm_vec3 scale;
    glm_vec3_fill(scale.data, radius);
    glm_scale(RENDERER->state.current_matrix->data, scale.data);

    sm_renderer2_begin(SM_TRIANGLES);
    sm_renderer2_vertex_color_vec4(color);

    f32 frings = (f32)rings;
    f32 fslices = (f32)slices;

    for (f32 i = 0; i < (frings + 2); i++)
    {
        for (f32 j = 0; j < fslices; j++)
        {

            f32 v1_x = cosf(glm_rad(270 + (180.0f / (frings + 1)) * i)) * sinf(glm_rad(360.0f * j / fslices));
            f32 v1_y = sinf(glm_rad(270 + (180.0f / (frings + 1)) * i));
            f32 v1_z = cosf(glm_rad(270 + (180.0f / (frings + 1)) * i)) * cosf(glm_rad(360.0f * j / fslices));

            f32 v2_x = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1))) * sinf(glm_rad(360.0f * (j + 1) / fslices));
            f32 v2_y = sinf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1)));
            f32 v2_z = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1))) * cosf(glm_rad(360.0f * (j + 1) / fslices));

            f32 v3_x = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1))) * sinf(glm_rad(360.0f * j / fslices));
            f32 v3_y = sinf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1)));
            f32 v3_z = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1))) * cosf(glm_rad(360.0f * j / fslices));

            sm_vec3 v1 = sm_vec3_new(v1_x, v1_y, v1_z);
            sm_vec3 v2 = sm_vec3_new(v2_x, v2_y, v2_z);
            sm_vec3 v3 = sm_vec3_new(v3_x, v3_y, v3_z);

            sm_renderer2_vertex_vec3(v1);
            sm_renderer2_vertex_vec3(v2);
            sm_renderer2_vertex_vec3(v3);

            v1_x = cosf(glm_rad(270 + (180.0f / (frings + 1)) * i)) * sinf(glm_rad(360.0f * j / fslices));
            v1_y = sinf(glm_rad(270 + (180.0f / (frings + 1)) * i));
            v1_z = cosf(glm_rad(270 + (180.0f / (frings + 1)) * i)) * cosf(glm_rad(360.0f * j / fslices));

            v2_x = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i))) * sinf(glm_rad(360.0f * (j + 1) / fslices));
            v2_y = sinf(glm_rad(270 + (180.0f / (frings + 1)) * (i)));
            v2_z = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i))) * cosf(glm_rad(360.0f * (j + 1) / fslices));

            v3_x = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1))) * sinf(glm_rad(360.0f * (j + 1) / fslices));
            v3_y = sinf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1)));
            v3_z = cosf(glm_rad(270 + (180.0f / (frings + 1)) * (i + 1))) * cosf(glm_rad(360.0f * (j + 1) / fslices));

            v1 = sm_vec3_new(v1_x, v1_y, v1_z);
            v2 = sm_vec3_new(v2_x, v2_y, v2_z);
            v3 = sm_vec3_new(v3_x, v3_y, v3_z);

            sm_renderer2_vertex_vec3(v1);
            sm_renderer2_vertex_vec3(v2);
            sm_renderer2_vertex_vec3(v3);
        }
    }
    sm_renderer2_end( );
    sm_renderer2_mat4_pop( );
}

void sm_renderer2_draw_line_3D(sm_vec3 startPos, sm_vec3 endPos, sm_vec4 color)
{
    // WARNING: Be careful with internal buffer vertex alignment
    // when using RL_LINES or RL_TRIANGLES, data is aligned to fit
    // lines-triangles-quads in the same indexed buffers!!!
    sm_renderer_check_batch_limit(8);

    sm_renderer2_begin(SM_LINES);
    sm_renderer2_vertex_color_vec4(color);
    sm_renderer2_vertex_vec3(startPos);
    sm_renderer2_vertex_vec3(endPos);
    sm_renderer2_end( );
}

void sm_renderer2_draw_cube_wires(sm_vec3 position, f32 width, f32 height, f32 length, sm_vec4 color)
{
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    sm_renderer_check_batch_limit(36);

    sm_renderer2_mat4_push( );
    glm_translate(RENDERER->state.current_matrix->data, position.data);

    sm_renderer2_begin(SM_LINES);
    sm_renderer2_vertex_color_vec4(color);

    // Front face -----------------------------------------------------
    // Bottom line
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y - height / 2, z + length / 2)); // Bottom left
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y - height / 2, z + length / 2)); // Bottom right

    // Left line
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y - height / 2, z + length / 2)); // Bottom right
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y + height / 2, z + length / 2)); // Top right

    // Top line
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y + height / 2, z + length / 2)); // Top right
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y + height / 2, z + length / 2)); // Top left

    // Right line
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y + height / 2, z + length / 2)); // Top left
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y - height / 2, z + length / 2)); // Bottom left

    // Back face ------------------------------------------------------
    // Bottom line
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y - height / 2, z - length / 2)); // Bottom left
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y - height / 2, z - length / 2)); // Bottom right

    // Left line
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y - height / 2, z - length / 2)); // Bottom right
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y + height / 2, z - length / 2)); // Top right

    // Top line
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y + height / 2, z - length / 2)); // Top right
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y + height / 2, z - length / 2)); // Top left

    // Right line
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y + height / 2, z - length / 2)); // Top left
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y - height / 2, z - length / 2)); // Bottom left

    // Top face -------------------------------------------------------
    // Left line
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y + height / 2, z + length / 2)); // Top left front
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y + height / 2, z - length / 2)); // Top left back

    // Right line
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y + height / 2, z + length / 2)); // Top right front
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y + height / 2, z - length / 2)); // Top right back

    // Bottom face  ---------------------------------------------------
    // Left line
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y - height / 2, z + length / 2)); // Top left front
    sm_renderer2_vertex_vec3(sm_vec3_new(x - width / 2, y - height / 2, z - length / 2)); // Top left back

    // Right line
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y - height / 2, z + length / 2)); // Top right front
    sm_renderer2_vertex_vec3(sm_vec3_new(x + width / 2, y - height / 2, z - length / 2)); // Top right back
    sm_renderer2_end( );
    sm_renderer2_mat4_pop( );
}

void sm_renderer2_draw_AABB(sm_bounding_box_s box, sm_vec4 color)
{
    sm_vec3 size = {0};

    size.x = fabsf(box.max.x - box.min.x);
    size.y = fabsf(box.max.y - box.min.y);
    size.z = fabsf(box.max.z - box.min.z);

    sm_vec3 center = sm_vec3_new(box.min.x + size.x / 2.0f, box.min.y + size.y / 2.0f, box.min.z + size.z / 2.0f);

    sm_renderer2_draw_cube_wires(center, size.x, size.y, size.z, color);
}
