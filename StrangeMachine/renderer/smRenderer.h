#ifndef SM_RENDERER_H
#define SM_RENDERER_H

#include "smpch.h"

#include "renderer/GL/smGL.h"

typedef enum
{
    SM_SHADER_LOC_VERTEX_POSITION = 0, // shader location: vertex attribute: position
    SM_SHADER_LOC_VERTEX_UV,           // shader location: vertex attribute: texcoord01
    SM_SHADER_LOC_VERTEX_COLOR,        // shader location: vertex attribute: color
    SM_SHADER_LOC_VERTEX_NORMAL,       // shader location: vertex attribute: normal

    SM_SHADER_LOC_MATRIX_MODEL, // shader location: matrix uniform: model (transform)
    SM_SHADER_LOC_MATRIX_PV,    // shader location: matrix uniform: model-view-projection
    SM_SHADER_LOC_MATRIX_PVM,   // shader location: matrix uniform: model-view-projection

    SM_SHADER_LOC_COLOR_DIFFUSE, // shader location: Color diffuse: albedo
    SM_SHADER_LOC_MAP_DIFFUSE,   // shader location: sampler2d texture: albedo
    SM_SHADER_LOC_MAX,

} sm_renderer_shader_loc_e;

// color blending modes (pre-defined)
typedef enum
{
    SM_BLEND_ALPHA = 0,         // blend textures considering alpha (default)
    SM_BLEND_ADDITIVE,          // blend textures adding colors
    SM_BLEND_MULTIPLIED,        // blend textures multiplying colors
    SM_BLEND_ADD_COLORS,        // blend textures adding colors (alternative)
    SM_BLEND_SUBTRACT_COLORS,   // blend textures subtracting colors (alternative)
    SM_BLEND_ALPHA_PREMULTIPLY, // blend premultiplied textures considering alpha
    SM_BLEND_CUSTOM             // blend textures using custom src/dst factors (use rlSetBlendFactors())

} sm_renderer_blend_mode_e;

typedef struct sm__renderer_defaults_s
{

    sm_resource_entity_s shader;
    i32 shader_locs[SM_SHADER_LOC_MAX];

    sm_resource_entity_s diffuse_map;
    sm_vec4 diffuse_color;

} sm_renderer_defaults_s;

extern sm_renderer_defaults_s renderer_defaults;

void sm_renderer_init(void);
void sm_renderer_teardown(void);

void sm_renderer_clear(sm_vec4 color);
void sm_renderer_on_resize(i32 width, i32 height);
void sm_renderer_draw_scene(sm_resource_scene_s *scene);

void sm_renderer2_init(void);
void sm_renderer2_teardown(void);
void sm_draw_begin(void);
void sm_draw_end(void);
void sm_renderer2_draw_rectangle(sm_vec2 position, sm_vec2 wh, sm_vec4 color);
void sm_renderer2_draw_circle(sm_vec2 center, f32 radius, f32 startAngle, f32 endAngle, u32 segments, sm_vec4 color);
void sm_renderer2_end_3d(void);
void sm_renderer2_begin_3d(const sm_camera_s *camera);
void sm_renderer2_draw_grid(i32 slices, f32 spacing);
void sm_renderer2_draws_phere(sm_vec3 centerPos, f32 radius, u32 rings, u32 slices, sm_vec4 color);
void sm_renderer2_on_resize(i32 width, i32 height);
void sm_renderer2_draw_line_3D(sm_vec3 startPos, sm_vec3 endPos, sm_vec4 color);
void sm_renderer2_draw_cube_wires(sm_vec3 position, f32 width, f32 height, f32 length, sm_vec4 color);
void sm_renderer2_draw_AABB(sm_bounding_box_s box, sm_vec4 color);

#endif
