#include "cglm/vec3.h"
#include "ecs/smComponents.h"
#include "ecs/smECS.h"
#include "smpch.h"

#include "core/smCore.h"

#include "renderer/smRenderer.h"
#include "resource/smResource.h"

#include "renderer/GL/smGL.h"

sm_renderer_defaults_s renderer_defaults = {0};

// set blend mode
void sm_renderer_set_blend_mode(sm_renderer_blend_mode_e mode)
{

    switch (mode)
    {
    case SM_BLEND_ALPHA:
        {
            sm_gl_blend_func(SM_SRC_ALPHA, SM_ONE_MINUS_SRC_ALPHA);
            sm_gl_blend_equation(SM_FUNC_ADD);
            break;
        }
    case SM_BLEND_ADDITIVE:
        {
            sm_gl_blend_func(SM_SRC_ALPHA, SM_ONE);
            sm_gl_blend_equation(SM_FUNC_ADD);
            break;
        }
    case SM_BLEND_MULTIPLIED:
        {
            sm_gl_blend_func(SM_DST_COLOR, SM_ONE_MINUS_SRC_ALPHA);
            sm_gl_blend_equation(SM_FUNC_ADD);
            break;
        }
    case SM_BLEND_ADD_COLORS:
        {
            sm_gl_blend_func(SM_ONE, SM_ONE);
            sm_gl_blend_equation(SM_FUNC_ADD);
            break;
        }
    case SM_BLEND_SUBTRACT_COLORS:
        {
            sm_gl_blend_func(SM_ONE, SM_ONE);
            sm_gl_blend_equation(SM_FUNC_SUBTRACT);
            break;
        }
    case SM_BLEND_ALPHA_PREMULTIPLY:
        {
            sm_gl_blend_func(SM_ONE, SM_ONE_MINUS_SRC_ALPHA);
            sm_gl_blend_equation(SM_FUNC_ADD);
            break;
        }
    case SM_BLEND_CUSTOM:
        {
            // NOTE: Using GL blend src/dst factors and GL equation configured with rlSetBlendFactors()
            // sm_gl_blend_func(RLGL.state.gl_blend_src_factor, RLGL.state.gl_blend_dst_factor);
            // sm_gl_blend_equation(RLGL.state.gl_blend_equation);
        }
        break;
    default: break;
    }
}

void sm_renderer_init(void)
{
    // init default white texture
    u8 *pixels = SM_MALLOC(sizeof(u8) * 4);
    memcpy(pixels, (u8[4]){211, 211, 211, 255}, sizeof(u8) * 4);

    sm_resource_texture_s default_texture;

    default_texture.header = sm_resource_header_new(SM_RESOURCE_TYPE_TEXTURE, 1, "DefaultDiffuseMap");
    default_texture.width = 1;
    default_texture.height = 1;
    default_texture.raw_data = pixels;
    default_texture.format = SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;
    default_texture.handle = 0;
    sm_rc_store(&default_texture.ref_count, 0);
    default_texture.flags = 0x0;

    sm_resource_texture_set_on_cpu(&default_texture, true);

    sm_resource_entity_s default_texture_entt = sm_resource_texture_new_from_mem(&default_texture);
    renderer_defaults.diffuse_map = sm_resource_texture_make_reference(default_texture_entt);

    sm_resource_texture_s *texture = sm_resource_manager_get_data(renderer_defaults.diffuse_map);
    sm_resource_texture_load_gpu(texture);

    if (SM_RESOURCE_IS_VALID(renderer_defaults.diffuse_map))
    {
        SM_LOG_INFO("[IDX %d] default texture loaded successfully", renderer_defaults.diffuse_map.index);
    }
    else
    {
        SM_LOG_WARN("failed to load default texture");
    }

    sm_resource_entity_s default_shader_entt = sm_resource_manager_get_by_name("renderer3D", SM_RESOURCE_TYPE_SHADER);
    renderer_defaults.shader = sm_resource_shader_make_reference(default_shader_entt);

    sm_resource_shader_s *default_shader = sm_resource_manager_get_data(renderer_defaults.shader);
    sm_resource_shader_load_gpu(default_shader);

    if (SM_RESOURCE_IS_VALID(renderer_defaults.shader))
    {
        SM_LOG_INFO("[IDX %d] default shader loaded successfully", renderer_defaults.shader.index);
    }
    else
    {
        SM_LOG_WARN("failed to load default shader");
    }

    for (u32 i = 0; i < SM_SHADER_LOC_MAX; ++i) renderer_defaults.shader_locs[i] = -1;
    renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_POSITION] = sm_gl_shader_attribute_get_location(default_shader->program, "a_position");
    renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_UV] = sm_gl_shader_attribute_get_location(default_shader->program, "a_uv");
    renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_COLOR] = sm_gl_shader_attribute_get_location(default_shader->program, "a_color");
    renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_NORMAL] = sm_gl_shader_attribute_get_location(default_shader->program, "a_normal");

    renderer_defaults.shader_locs[SM_SHADER_LOC_MATRIX_MODEL] = sm_gl_shader_uniform_get_location(default_shader->program, "u_model");
    renderer_defaults.shader_locs[SM_SHADER_LOC_MATRIX_PV] = sm_gl_shader_uniform_get_location(default_shader->program, "u_pv");
    renderer_defaults.shader_locs[SM_SHADER_LOC_COLOR_DIFFUSE] = sm_gl_shader_uniform_get_location(default_shader->program, "u_color_diffuse");
    renderer_defaults.shader_locs[SM_SHADER_LOC_MAP_DIFFUSE] = sm_gl_shader_uniform_get_location(default_shader->program, "u_tex0");
    // renderer_defaults.shader_locs[SM_SHADER_LOC_RESOLUTION] = sm_gl_shader_uniform_get_location(default_shader->program, "u_resolution");
    // renderer_defaults.shader_locs[SM_SHADER_LOC_TIME] = sm_gl_shader_uniform_get_location(default_shader->program, "u_time");

    sm_resource_shader_bind(renderer_defaults.shader);
    i32 value = 0;
    sm_gl_shader_uniform_set(renderer_defaults.shader_locs[SM_SHADER_LOC_MAP_DIFFUSE], SM_I32, &value);
    sm_resource_shader_ubind( );

    glm_vec4_copy(sm_vec4_one( ).data, renderer_defaults.diffuse_color.data);
}

// Vertex Buffer Object deinitialization (memory free)
void sm_renderer_teardown(void) {}

void sm__default_renderer_cb(sm_resource_scene_s *scene, u32 index, void *user_data)
{

    SM_UNUSED(user_data);

    if (index == SM_SCENE_ROOT_NODE) return;

    sm_ecs_world_s *ecs = scene->ecs;

    sm_entity_s cam_entt = sm_resource_scene_get_current_camera(scene);
    const sm_camera_s *camera = sm_ecs_component_get(ecs, cam_entt, SM_CAMERA_COMP);

    sm_mat4 model_matrix, view_matrix, projection_matrix, view_projection_matrix;
    sm_camera_component_get_projection(camera, projection_matrix.data);
    sm_camera_component_get_view(camera, view_matrix.data);

    glm_mat4_mul(projection_matrix.data, view_matrix.data, view_projection_matrix.data);

    sm_entity_s ett = sm_resource_scene_get_entity(scene, index);

    if (!sm_ecs_entity_has_component(ecs, ett, SM_TRANSFORM_COMP | SM_RESOURCE_COMP))
    {
        return;
    }

    const sm_res_s *res = sm_ecs_component_get(ecs, ett, SM_RESOURCE_COMP);
    sm_resource_entity_s res_entt = sm_resource_manager_get_by_uuid(res->uuid);
    if (!SM_MASK_CHK(res_entt.type, SM_RESOURCE_TYPE_MESH)) return;

    sm_resource_mesh_s *mesh = sm_resource_manager_get_data(res_entt);
    if (sm_resource_mesh_is_renderable(mesh))
    {

        sm_resource_entity_s texture = renderer_defaults.diffuse_map;
        sm_resource_entity_s shader = renderer_defaults.shader;
        sm_vec4 diffuse_color = renderer_defaults.diffuse_color;
        b8 twosided = false;

        sm_resource_entity_s material_entt = sm_resource_manager_get_by_uuid(mesh->material);
        if (SM_RESOURCE_IS_VALID(material_entt))
        {

            sm_resource_material_s *material = sm_resource_manager_get_data(material_entt);

            sm_resource_entity_s material_diffuse_map = sm_resource_manager_get_by_uuid(material->diffuse_map);
            if (SM_RESOURCE_IS_VALID(material_diffuse_map))
            {
                texture = material_diffuse_map;
            }

            /* sm_shader_handler_s material_shader = sm_material_manager_get_shader(material); */
            sm_vec4 material_diffuse_color = material->diffuse_color;
            if (!glm_vec4_eq(material_diffuse_color.data, 0.0f))
            {
                diffuse_color.r = material_diffuse_color.r;
                diffuse_color.g = material_diffuse_color.g;
                diffuse_color.b = material_diffuse_color.b;
                diffuse_color.a = material_diffuse_color.a;
            }

            twosided = sm_resource_material_is_twosided(material);
        }

        sm_transform_s *transform = sm_ecs_component_get_mut(ecs, ett, SM_TRANSFORM_COMP);
        if (sm_ecs_entity_has_component(ecs, ett, SM_WORLD_COMP))
        {
            sm_world_s *world = sm_ecs_component_get_mut(ecs, ett, SM_WORLD_COMP);
            model_matrix = world->matrix;
        }
        else
        {
            sm_transform_component_to_mat4(transform, model_matrix.data);
        }

        if (sm_resource_mesh_is_draw_aabb(mesh))
        {
            sm_bounding_box_s aabb = sm_resource_mesh_get_aabb(mesh);

            glm_mat4_mulv3(model_matrix.data, aabb.max.data, 1.0f, aabb.max.data);
            glm_mat4_mulv3(model_matrix.data, aabb.min.data, 1.0f, aabb.min.data);
            sm_renderer2_draw_AABB(aabb, sm_vec4_new(0.2f, 0.9f, 0.1f, 1.0f));
        }

        sm_gl_vao_bind(mesh->vao);
        sm_resource_shader_bind(shader);

        sm_resource_texture_s *text = sm_resource_manager_get_data(texture);

        sm_gl_texture_activate(0);
        sm_gl_texture_bind(text->handle);

        sm_gl_shader_uniform_set(renderer_defaults.shader_locs[SM_SHADER_LOC_MATRIX_MODEL], SM_MAT4, model_matrix.float16);
        sm_gl_shader_uniform_set(renderer_defaults.shader_locs[SM_SHADER_LOC_MATRIX_PV], SM_MAT4, view_projection_matrix.float16);
        sm_gl_shader_uniform_set(renderer_defaults.shader_locs[SM_SHADER_LOC_COLOR_DIFFUSE], SM_VEC4, diffuse_color.data);

        if (twosided) sm_gl_disable(SM_CULL_FACE);

        // sm_gl_enable(SM_BLEND);
        // sm_renderer_set_blend_mode(SM_BLEND_ALPHA);

        sm_gl_draw_elements(SM_TRIANGLES, (i32)SM_ARRAY_LEN(mesh->indices), NULL);

        // sm_gl_disable(SM_BLEND);
        if (twosided) sm_gl_enable(SM_CULL_FACE);

        sm_gl_texture_unbind( );

        sm_resource_shader_ubind( );
        sm_gl_vao_unbind( );
    }
}

void sm_renderer_on_resize(i32 width, i32 height)
{

    sm_gl_viewport(0, 0, width, height);
}

void sm_renderer_clear(sm_vec4 color)
{

    sm_gl_clear_color(color);
    sm_gl_clear(SM_DEPTH_BUFFER_BIT | SM_COLOR_BUFFER_BIT);
}

void sm_renderer_draw_scene(sm_resource_scene_s *scene)
{

    SM_ASSERT(scene);

    // sm_gl_enable(SM_DEPTH_TEST);
    sm_resource_scene_for_each(scene, 0, sm__default_renderer_cb, NULL);
    // sm_gl_disable(SM_DEPTH_TEST);
}
