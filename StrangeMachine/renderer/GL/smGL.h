#ifndef SM_GL_H
#define SM_GL_H

#include "smpch.h"

#include "math/smMath.h"

#include "ecs/smComponents.h"
#include "resource/smResource.h"

// Primitive assembly draw modes
typedef enum
{

    SM_POINTS = 0x0000,
    SM_LINES = 0x0001,
    SM_LINE_LOOP = 0x0002,
    SM_LINE_STRIP = 0x0003,
    SM_TRIANGLES = 0x0004,
    SM_TRIANGLE_STRIP = 0x0005,
    SM_TRIANGLE_FAN = 0x0006,
    SM_QUADS = 0x0007

} sm_draw_mode_e;

typedef enum
{
    SM_ZERO = 0,
    SM_ONE = 1,
    SM_SRC_COLOR = 0x0300,
    SM_GL_ONE_MINUS_SRC_COLOR = 0x0301,
    SM_DST_COLOR = 0x0306,
    SM_ONE_MINUS_DST_COLOR = 0x0307,
    SM_SRC_ALPHA = 0x0302,
    SM_ONE_MINUS_SRC_ALPHA = 0x0303,
    SM_DST_ALPHA = 0x0304,
    SM_ONE_MINUS_DST_ALPHA = 0x0305,
    SM_CONSTANT_COLOR = 0x8001,
    SM_ONE_MINUS_CONSTANT_COLOR = 0x8002,
    SM_CONSTANT_ALPHA = 0x8003,
    SM_ONE_MINUS_CONSTANT_ALPHA = 0x8004,
    SM_SRC_ALPHA_SATURATE = 0x0308,

} sm_blend_factor_e;

typedef enum
{
    SM_FUNC_ADD = 0x8006,
    SM_FUNC_SUBTRACT = 0x800A,
    SM_FUNC_REVERSE_SUBTRACT = 0x800B,
    SM_MIN = 0x8007,
    SM_MAX = 0x8008,

} sm_blend_equation_e;

typedef enum
{
    SM_NEVER = 0x0200,
    SM_LESS = 0x0201,
    SM_EQUAL = 0x0202,
    SM_LEQUAL = 0x0203,
    SM_GREATER = 0x0204,
    SM_NOTEQUAL = 0x0205,
    SM_GEQUAL = 0x0206,
    SM_ALWAYS = 0x0207,

} sm_depth_func_e;

typedef enum
{
    SM_STREAM_DRAW = 0x88E0,
    SM_STATIC_DRAW = 0x88E4,
    SM_DYNAMIC_DRAW = 0x88E8,

} sm_buffer_usage_e;

typedef enum
{
    SM_FRONT = 0x0404,
    SM_BACK = 0x0405,
    SM_FRONT_AND_BACK = 0x0408,

} sm_cull_mode_e;

/* Specifies the orientation of front-facing polygons. clockwise and counter clockwise are accepted.  */
typedef enum
{
    SM_CW = 0x0900,
    SM_CCW = 0x0901,

} sm_front_face_mode_e;

typedef enum
{
    SM_F32 = 0x1406,           // GL_FLOAT,
    SM_I32 = 0x1404,           // GL_INT,
    SM_UNSIGNED_BYTE = 0x1401, // GL_UNSIGNED_BYTE

    SM_IVEC2 = 0x8B53,     // GL_INT_VEC2,
    SM_IVEC3 = 0x8B54,     // GL_INT_VEC3,
    SM_IVEC4 = 0x8B55,     // GL_INT_VEC4,
    SM_VEC2 = 0x8B50,      // GL_FLOAT_VEC2
    SM_VEC3 = 0x8B51,      // GL_FLOAT_VEC3
    SM_VEC4 = 0x8B52,      // GL_FLOAT_VEC4
    SM_MAT2 = 0x8B5A,      // GL_FLOAT_MAT2
    SM_MAT3 = 0x8B5B,      // GL_FLOAT_MAT3
    SM_MAT4 = 0x8B5C,      // GL_FLOAT_MAT4
    SM_SAMPLER2D = 0x8B5E, // GL_SAMPLER_2D

} sm_type_e;

#define SM_TYPE_TO_STR(TYPE)                   \
    ((TYPE) == SM_F32            ? "f32"       \
        : (TYPE) == SM_I32       ? "i32"       \
        : (TYPE) == SM_IVEC2     ? "ivec2"     \
        : (TYPE) == SM_IVEC3     ? "ivec3"     \
        : (TYPE) == SM_IVEC4     ? "ivec4"     \
        : (TYPE) == SM_VEC2      ? "vec2"      \
        : (TYPE) == SM_VEC3      ? "vec3"      \
        : (TYPE) == SM_VEC4      ? "vec4"      \
        : (TYPE) == SM_MAT2      ? "mat2"      \
        : (TYPE) == SM_MAT3      ? "mat3"      \
        : (TYPE) == SM_MAT4      ? "mat4"      \
        : (TYPE) == SM_SAMPLER2D ? "sampler2D" \
                                 : "unknown")

typedef enum
{

    SM_DEPTH_TEST = 0x0B71,
    SM_CULL_FACE = 0x0B44,
    SM_BLEND = 0x0BE2

} enable_flags_e;

typedef enum
{

    SM_DEPTH_BUFFER_BIT = 0x00000100,   /* GL_DEPTH_BUFFER_BIT   */
    SM_STENCIL_BUFFER_BIT = 0x00000400, /* GL_STENCIL_BUFFER_BIT */
    SM_COLOR_BUFFER_BIT = 0x00004000,   /* GL_COLOR_BUFFER_BIT */

} buffer_bit_e;

typedef void *(*sm_gl_loadproc_f)(const char *name);
b8 sm_gl_load_proc(sm_gl_loadproc_f load);

b8 sm_gl_shader_new(sm_resource_shader_s *shader);
void sm_gl_shader_bind(u32 program);
void sm_gl_shader_unbind(void);
i32 sm_gl_shader_uniform_get_location(u32 program, const char *uniform);
void sm_gl_shader_uniform_set(i32 uniform_loc, sm_type_e type, const void *value);
void sm_gl_shader_uniform_set_array(i32 uniform_loc, sm_type_e type, u32 size, void *value);
void sm_gl_shader_uniform_get_active(u32 program, u32 index, i32 buf_size, i32 *length, i32 *size, u32 *type, char *name);
i32 sm_gl_shader_uniform_get_active_count(u32 program);
i32 sm_gl_shader_attribute_get_location(u32 program, const char *attribute);
void sm_gl_shader_delete(u32 program);

void sm_gl_vertex_attribute_pointer(u32 index, i32 size, sm_type_e type, bool normalized, i32 stride, const void *pointer);
void sm_gl_vertex_attribute_enable(u32 index);
void sm_gl_vertex_attribute_disable(u32 index);

u32 sm_gl_vbo_new(const void *buf, u32 size, sm_buffer_usage_e usage);
void sm_gl_vbo_sub_data(u32 VBO, const void *data, u32 size, u32 offset);
void sm_gl_vbo_bind(u32 VBO);
void sm_gl_vbo_unbind(void);
void sm_gl_vbo_delete(u32 VBO);

u32 sm_gl_ebo_new(const void *buf, u32 size, sm_buffer_usage_e usage);
void sm_gl_ebo_sub_data(u32 EBO, const void *data, u32 size, u32 offset);
void sm_gl_ebo_bind(u32 EBO);
void sm_gl_ebo_unbind(void);

u32 sm_gl_vao_new(void);
void sm_gl_vao_bind(unsigned int VAO);
void sm_gl_vao_unbind(void);
void sm_gl_vao_delete(u32 VAO);

u32 sm_gl_texture_ctor(sm_resource_texture_s *texture);
void sm_gl_texture_dtor(u32 texture);
void sm_gl_texture_activate(u32 tex_index);
void sm_gl_texture_bind(u32 texture);
void sm_gl_texture_unbind(void);
u32 sm_gl_get_pixel_data_size(u32 width, u32 height, sm_pixel_format_e format);

void sm_gl_draw(sm_draw_mode_e mode, i32 offset, i32 count);
void sm_gl_draw_elements(sm_draw_mode_e mode, i32 count, const void *buffer);
void sm_gl_draw_instanced(i32 count, i32 instances);
void sm_gl_draw_elements_instanced(i32 count, const void *buffer, i32 instances);

void sm_gl_viewport(i32 x, i32 y, i32 width, i32 height);
void sm_gl_clear_color(sm_vec4 color);
void sm_gl_clear(u32 mask);
void sm_gl_enable(enable_flags_e flags);
void sm_gl_disable(enable_flags_e flags);
void sm_gl_clear_depth(f32 value);
void sm_gl_depth_func(sm_depth_func_e func);

void sm_gl_blend_func(sm_blend_factor_e src_fac, sm_blend_factor_e dst_fac);
void sm_gl_blend_equation(sm_blend_equation_e eq);

void sm_gl_set_line_width(f32 width);

void sm_gl_cull_face(sm_cull_mode_e width);
void sm_gl_front_face(sm_front_face_mode_e mode);

#endif /* SM_GL_H */
