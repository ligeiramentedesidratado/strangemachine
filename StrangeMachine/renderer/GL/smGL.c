#include "smpch.h"

#include "ecs/smComponents.h"
#include "renderer/GL/smGL.h"

#ifndef GL_LUMINANCE
    #define GL_LUMINANCE 0x1909
#endif
#ifndef GL_LUMINANCE_ALPHA
    #define GL_LUMINANCE_ALPHA 0x190A
#endif

#include "core/smCore.h"
#include "math/smMath.h"

#include "vendor/glad/glad.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "GL"

#ifdef SM_DEBUG

SM_PRIVATE
b8 sm__gl_log_call( );
SM_PRIVATE
void sm__gl_clear_error( );

    #define glCall(CALL)                            \
        do {                                        \
            sm__gl_clear_error( );                  \
            CALL;                                   \
            SM_ASSERT(sm__gl_log_call( ) && #CALL); \
        } while (0)

#else

    #define glCall(CALL) CALL

#endif /* SM_DEBUG */

b8 sm_gl_load_proc(sm_gl_loadproc_f load)
{
    return gladLoadGLLoader(load);
}

SM_PRIVATE
const char *sm__gl_error_to_string(GLenum error)
{
    switch (error)
    {
    case GL_NO_ERROR: return "GL_NO_ERROR";
    case GL_INVALID_ENUM: return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE: return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION";
    case GL_OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
    default: return "UNKNOWN";
    }
}

SM_PRIVATE
b8 sm__gl_log_call( )
{
    GLenum err;
    while ((err = glGetError( )))
    {
        SM_LOG_ERROR("[GL Error] (%d): %s", err, sm__gl_error_to_string(err));
        return false;
    }
    return true;
}

SM_PRIVATE
void sm__gl_clear_error( )
{
    while (glGetError( ) != GL_NO_ERROR)
    {
    }
}

SM_PRIVATE
GLuint sm__gl_shader_compile_vert(const SM_STRING vertex);
SM_PRIVATE
GLuint sm__gl_shader_compile_frag(const SM_STRING fragment);
SM_PRIVATE
bool sm__gl_shader_link(GLuint shader, GLuint vertex, GLuint fragment);

b8 sm_gl_shader_new(sm_resource_shader_s *shader)
{

    u32 program = 0;

    if (!sm_resource_shader_is_on_cpu(shader))
    {
        SM_LOG_ERROR("shader has no data");
        return false;
    }

    GLuint vert = sm__gl_shader_compile_vert(shader->vertex_data);
    GLuint frag = sm__gl_shader_compile_frag(shader->fragment_data);

    if (!vert || !frag) return false;

    glCall(program = glCreateProgram( ));

    if (!sm__gl_shader_link(program, vert, frag))
    {
        SM_LOG_ERROR("failed to link shader program");
        glCall(glDeleteProgram(program));
        return false;
    }

    shader->program = program;

    return true;
}

void sm_gl_shader_bind(u32 program)
{

    SM_ASSERT(program > 0);

    glCall(glUseProgram(program));
}

void sm_gl_shader_unbind(void)
{

    glCall(glUseProgram(0));
}

i32 sm_gl_shader_uniform_get_location(u32 program, const char *uniform)
{

    i32 loc = -1;

    glCall(loc = glGetUniformLocation(program, uniform));
    if (loc == -1) SM_LOG_WARN("failed to find shader uniform: '%s'", uniform);

    return loc;
}

void sm_gl_shader_uniform_set(i32 uniform_loc, sm_type_e type, const void *value)
{

    SM_ASSERT(uniform_loc >= 0);
    SM_ASSERT(value);

    switch (type)
    {

    case SM_SAMPLER2D: /* fallthrough */
    case SM_I32: glCall(glUniform1i(uniform_loc, *(int *)value)); break;
    case SM_F32: glCall(glUniform1f(uniform_loc, *(f32 *)value)); break;
    case SM_VEC2: glCall(glUniform2fv(uniform_loc, 1, (f32 *)value)); break;
    case SM_VEC3: glCall(glUniform3fv(uniform_loc, 1, (f32 *)value)); break;
    case SM_VEC4: glCall(glUniform4fv(uniform_loc, 1, (f32 *)value)); break;
    case SM_MAT4: glCall(glUniformMatrix4fv(uniform_loc, 1, GL_FALSE, (f32 *)value)); break;
    default: SM_LOG_ERROR("unsupported type (%d) %s", type, SM_TYPE_TO_STR(type)); break;
    }
}

void sm_gl_shader_uniform_set_array(i32 uniform_loc, sm_type_e type, u32 size, void *value)
{

    SM_ASSERT(uniform_loc > 0);
    SM_ASSERT(value);

    switch (type)
    {
    case SM_I32: glCall(glUniform1iv(uniform_loc, (i32)size, (int *)value)); break;
    case SM_F32: glCall(glUniform1fv(uniform_loc, (i32)size, (f32 *)value)); break;
    case SM_VEC2: glCall(glUniform2fv(uniform_loc, (i32)size, (f32 *)value)); break;
    case SM_VEC3: glCall(glUniform3fv(uniform_loc, (i32)size, (f32 *)value)); break;
    case SM_VEC4: glCall(glUniform4fv(uniform_loc, (i32)size, (f32 *)value)); break;
    case SM_MAT4: glCall(glUniformMatrix4fv(uniform_loc, (i32)size, GL_FALSE, (f32 *)value)); break;
    default: SM_LOG_WARN("unsupported type (%d) %s", type, SM_TYPE_TO_STR(type)); break;
    }
}

void sm_gl_shader_uniform_get_active(u32 program, u32 index, i32 buf_size, i32 *length, i32 *size, u32 *type, char *name)
{

    glCall(glGetActiveUniform(program, index, buf_size, length, size, type, name));
}

i32 sm_gl_shader_uniform_get_active_count(u32 program)
{

    i32 num_uniforms;
    glCall(glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &num_uniforms));

    return num_uniforms;
}

i32 sm_gl_shader_attribute_get_location(u32 program, const char *attribute)
{

    i32 loc = -1;
    glCall(loc = glGetAttribLocation(program, attribute));
    if (loc == -1) SM_LOG_WARN("failed to find shader attribute: '%s'", attribute);

    return loc;
}

void sm_gl_shader_delete(u32 program)
{

    glCall(glDeleteProgram(program));
}

void sm_gl_vertex_attribute_pointer(u32 index, i32 size, sm_type_e type, bool normalized, i32 stride, const void *pointer)
{

    glCall(glVertexAttribPointer(index, size, type, normalized, stride, pointer));
}

void sm_gl_vertex_attribute_enable(u32 index)
{

    glCall(glEnableVertexAttribArray(index));
}

void sm_gl_vertex_attribute_disable(u32 index)
{

    glCall(glDisableVertexAttribArray(index));
}

u32 sm_gl_vbo_new(const void *buf, u32 size, sm_buffer_usage_e usage)
{

    u32 VBO = 0;

    glCall(glGenBuffers(1, &VBO));
    glCall(glBindBuffer(GL_ARRAY_BUFFER, VBO));
    glCall(glBufferData(GL_ARRAY_BUFFER, size, buf, usage));

    return VBO;
}

void sm_gl_vbo_sub_data(u32 VBO, const void *data, u32 size, u32 offset)
{

    SM_ASSERT(VBO > 0);

    glCall(glBindBuffer(GL_ARRAY_BUFFER, VBO));
    glCall(glBufferSubData(GL_ARRAY_BUFFER, offset, size, data));
}

void sm_gl_vbo_bind(u32 VBO)
{

    SM_ASSERT(VBO > 0);

    glCall(glBindBuffer(GL_ARRAY_BUFFER, VBO));
}

void sm_gl_vbo_unbind(void)
{

    glCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

void sm_gl_vbo_delete(u32 VBO)
{
    if (VBO == 0) return;

    glCall(glDeleteBuffers(1, &VBO));
}

u32 sm_gl_ebo_new(const void *buf, u32 size, sm_buffer_usage_e usage)
{

    u32 EBO = 0;

    glCall(glGenBuffers(1, &EBO));
    glCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO));
    glCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, buf, usage));

    return EBO;
}

void sm_gl_ebo_sub_data(u32 EBO, const void *data, u32 size, u32 offset)
{

    SM_ASSERT(EBO > 0);

    glCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO));
    glCall(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, size, data));
}

void sm_gl_ebo_bind(u32 EBO)
{

    SM_ASSERT(EBO > 0);

    glCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO));
}

void sm_gl_ebo_unbind(void)
{

    glCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

u32 sm_gl_vao_new(void)
{

    u32 VAO = 0;

    glCall(glGenVertexArrays(1, &VAO));

    return VAO;
}

void sm_gl_vao_bind(u32 VAO)
{

    SM_ASSERT(VAO > 0);

    glCall(glBindVertexArray(VAO));
}

void sm_gl_vao_unbind(void)
{

    glCall(glBindVertexArray(0));
}

void sm_gl_vao_delete(u32 VAO)
{

    glCall(glDeleteVertexArrays(1, &VAO));
}

u32 sm_gl_get_pixel_data_size(u32 width, u32 height, sm_pixel_format_e format)
{
    u32 dataSize = 0; // Size in bytes
    u32 bpp = 0;      // Bits per pixel

    switch (format)
    {
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE: bpp = 8; break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA:
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R5G6B5:
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R5G5B5A1:
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R4G4B4A4: bpp = 16; break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8: bpp = 32; break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8: bpp = 24; break;
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGB:
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGBA: bpp = 4; break;
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT3_RGBA:
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT5_RGBA: bpp = 8; break;
    default: break;
    }

    dataSize = width * height * bpp / 8; // Total data size in bytes

    /* Most compressed formats works on 4x4 blocks, */
    /* if texture is smaller, minimum dataSize is 8 or 16 */
    if ((width < 4) && (height < 4))
    {
        if ((format >= SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGB) && (format < SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT3_RGBA)) dataSize = 8;
        else if ((format >= SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT3_RGBA)) dataSize = 16;
    }

    return dataSize;
}

void sm_gl_get_GL_texture_formats(sm_pixel_format_e format, u32 *glInternalFormat, u32 *glFormat, u32 *glType)
{
    *glInternalFormat = 0;
    *glFormat = 0;
    *glType = 0;

    switch (format)
    {
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE:
        *glInternalFormat = GL_LUMINANCE;
        *glFormat = GL_LUMINANCE;
        *glType = GL_UNSIGNED_BYTE;
        break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA:
        *glInternalFormat = GL_LUMINANCE_ALPHA;
        *glFormat = GL_LUMINANCE_ALPHA;
        *glType = GL_UNSIGNED_BYTE;
        break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R5G6B5:
        *glInternalFormat = GL_RGB;
        *glFormat = GL_RGB;
        *glType = GL_UNSIGNED_SHORT_5_6_5;
        break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8:
        *glInternalFormat = GL_RGB;
        *glFormat = GL_RGB;
        *glType = GL_UNSIGNED_BYTE;
        break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R5G5B5A1:
        *glInternalFormat = GL_RGBA;
        *glFormat = GL_RGBA;
        *glType = GL_UNSIGNED_SHORT_5_5_5_1;
        break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R4G4B4A4:
        *glInternalFormat = GL_RGBA;
        *glFormat = GL_RGBA;
        *glType = GL_UNSIGNED_SHORT_4_4_4_4;
        break;
    case SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8:
        *glInternalFormat = GL_RGBA;
        *glFormat = GL_RGBA;
        *glType = GL_UNSIGNED_BYTE;
        break;
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGB: *glInternalFormat = GL_COMPRESSED_RGB_S3TC_DXT1_EXT; break;
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGBA: *glInternalFormat = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT; break;
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT3_RGBA: *glInternalFormat = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; break;
    case SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT5_RGBA: *glInternalFormat = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; break;
    default: SM_LOG_WARN("texture format not supported (%i)", format); break;
    }
}

u32 sm_gl_texture_ctor(sm_resource_texture_s *texture)
{

    SM_ASSERT(texture);

    u32 handle = 0xFFFFFFFFu;

    SM_ASSERT(sm_resource_texture_is_on_cpu(texture));

    glCall(glGenTextures(1, &handle));

    glCall(glBindTexture(GL_TEXTURE_2D, handle));

    u32 size = sm_gl_get_pixel_data_size(texture->width, texture->height, texture->format);

    u32 gl_internal_format, gl_format, gl_type;
    sm_gl_get_GL_texture_formats(texture->format, &gl_internal_format, &gl_format, &gl_type);

    if (texture->format < SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGB)
        glCall(
            glTexImage2D(GL_TEXTURE_2D, 0, (i32)gl_internal_format, (i32)texture->width, (i32)texture->height, 0, gl_format, gl_type, texture->raw_data));
    else glCall(glCompressedTexImage2D(GL_TEXTURE_2D, 0, gl_internal_format, (i32)texture->width, (i32)texture->height, 0, (i32)size, texture->raw_data));

    if (texture->format == SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE)
    {
        GLint swizzleMask[] = {GL_RED, GL_RED, GL_RED, GL_ONE};
        glCall(glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask));
    }
    else if (texture->format == SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA)
    {
        GLint swizzleMask[] = {GL_RED, GL_RED, GL_RED, GL_ALPHA};
        glCall(glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask));
    }

    glCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
    glCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));

    glCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    glCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

    glCall(glBindTexture(GL_TEXTURE_2D, 0));

    return handle;
}

void sm_gl_texture_dtor(u32 texture)
{

    SM_ASSERT(texture > 0);

    glCall(glDeleteTextures(1, &texture));
}

void sm_gl_texture_activate(u32 tex_index)
{

    glCall(glActiveTexture(GL_TEXTURE0 + tex_index));
}

void sm_gl_texture_bind(u32 texture)
{

    SM_ASSERT(texture > 0);

    glCall(glBindTexture(GL_TEXTURE_2D, texture));
}

void sm_gl_texture_unbind(void)
{

    glCall(glBindTexture(GL_TEXTURE_2D, 0));
}

void sm_gl_draw(sm_draw_mode_e mode, i32 offset, i32 count)
{
    glCall(glDrawArrays(mode, offset, count));
}

void sm_gl_draw_elements(sm_draw_mode_e mode, i32 count, const void *buffer)
{
    glCall(glDrawElements(mode, count, GL_UNSIGNED_INT, buffer));
}

void sm_gl_draw_instanced(i32 count, i32 instances)
{

    glCall(glDrawArraysInstancedARB(GL_TRIANGLES, 0, count, instances));
}

void sm_gl_draw_elements_instanced(i32 count, const void *buffer, i32 instances)
{

    glCall(glDrawElementsInstancedARB(GL_TRIANGLES, count, GL_UNSIGNED_INT, buffer, instances));
}

void sm_gl_viewport(i32 x, i32 y, i32 width, i32 height)
{

    glViewport(x, y, width, height);
}

void sm_gl_clear_color(sm_vec4 color)
{

    glCall(glClearColor(color.x, color.y, color.z, color.w));
}

void sm_gl_clear(u32 mask)
{

    glCall(glClear(mask));
}

void sm_gl_enable(enable_flags_e flags)
{

    glCall(glEnable(flags));
}

void sm_gl_disable(enable_flags_e flags)
{

    glCall(glDisable(flags));
}

void sm_gl_clear_depth(f32 value)
{

    glCall(glClearDepth(value));
}

void sm_gl_depth_func(sm_depth_func_e func)
{

    glCall(glDepthFunc(func));
}

void sm_gl_blend_func(sm_blend_factor_e src_fac, sm_blend_factor_e dst_fac)
{

    glCall(glBlendFunc(src_fac, dst_fac));
}

void sm_gl_blend_equation(sm_blend_equation_e eq)
{

    glCall(glBlendEquation(eq));
}

void sm_gl_set_line_width(f32 width)
{

    glCall(glLineWidth(width));
}

void sm_gl_cull_face(sm_cull_mode_e mode)
{

    glCall(glCullFace(mode));
}

void sm_gl_front_face(sm_front_face_mode_e mode)
{

    glCall(glFrontFace(mode));
}

static GLuint sm__gl_shader_compile_vert(const SM_STRING vertex)
{

    GLuint v = glCreateShader(GL_VERTEX_SHADER);
    const char *v_source = vertex;
    glCall(glShaderSource(v, 1, &v_source, NULL));
    glCall(glCompileShader(v));

    GLint success = 0;
    glCall(glGetShaderiv(v, GL_COMPILE_STATUS, &success));
    if (success != GL_TRUE)
    {
        char info_log[2 * 512];
        glGetShaderInfoLog(v, 2 * 512, NULL, info_log);
        SM_LOG_ERROR("vertex compilation failed.\n\t%s", info_log);
        glCall(glDeleteShader(v));
        return 0;
    }

    return v;
}

static GLuint sm__gl_shader_compile_frag(const SM_STRING fragment)
{

    GLuint f = glCreateShader(GL_FRAGMENT_SHADER);
    const char *f_source = fragment;
    glCall(glShaderSource(f, 1, &f_source, NULL));
    glCall(glCompileShader(f));

    GLint success = 0;
    glCall(glGetShaderiv(f, GL_COMPILE_STATUS, &success));
    if (!success)
    {
        char info_log[2 * 512];
        glGetShaderInfoLog(f, 2 * 512, NULL, info_log);
        SM_LOG_ERROR("fragment compilation failed.\n\t%s", info_log);
        glCall(glDeleteShader(f));
        return 0;
    }

    return f;
}

static bool sm__gl_shader_link(GLuint shader, GLuint vertex, GLuint fragment)
{

    glAttachShader(shader, vertex);
    glAttachShader(shader, fragment);
    glLinkProgram(shader);

    GLint success = 0;
    glGetProgramiv(shader, GL_LINK_STATUS, &success);
    if (!success)
    {
        char info_log[2 * 512];
        glGetShaderInfoLog(shader, 2 * 512, NULL, info_log);
        SM_LOG_ERROR("shader linking failed.\n\t%s", info_log);
        glCall(glDeleteShader(vertex));
        glCall(glDeleteShader(fragment));

        return false;
    }

    SM_LOG_INFO("compiled and linked shaders successfully");

    glCall(glDeleteShader(vertex));
    glCall(glDeleteShader(fragment));

    return true;
}

#undef SM_MODULE_NAME
