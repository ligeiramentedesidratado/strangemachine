#include "ecs/smComponents.h"
#include "smpch.h"

#include "math/smMath.h"
#include "smShapes.h"

// helper
sm_capsule_s sm_shape_capsule_new(sm_sphere_s s, f32 height)
{

  sm_capsule_s c;

  glm_vec3_copy((vec3){s.center.x, s.center.y - s.radius, s.center.z}, c.base.data);
  glm_vec3_copy((vec3){s.center.x, (s.center.y - s.radius) + height, s.center.z}, c.tip.data);

  c.radius = s.radius;

  return c;
}

sm_bounding_box_s sm_shape_get_aabb_sphere(sm_sphere_s s)
{
  sm_bounding_box_s result;

  sm_vec3 smin;
  smin.x = s.center.x - s.radius;
  smin.y = s.center.y - s.radius;
  smin.z = s.center.z - s.radius;

  sm_vec3 smax;
  smax.x = s.center.x + s.radius;
  smax.y = s.center.y + s.radius;
  smax.z = s.center.z + s.radius;

  glm_vec3_copy(smin.data, result.min.data);
  glm_vec3_copy(smax.data, result.max.data);

  return result;
}

sm_bounding_box_s sm_shape_get_aabb_capsule(sm_capsule_s c)
{

  sm_bounding_box_s result;

  sm_vec3 bmin;
  bmin.x = c.base.x - c.radius;
  bmin.y = c.base.y - c.radius;
  bmin.z = c.base.z - c.radius;

  sm_vec3 bmax;
  bmax.x = c.base.x + c.radius;
  bmax.y = c.base.y + c.radius;
  bmax.z = c.base.z + c.radius;

  sm_vec3 tmin;
  tmin.x = c.tip.x - c.radius;
  tmin.y = c.tip.y - c.radius;
  tmin.z = c.tip.z - c.radius;

  sm_vec3 tmax;
  tmax.x = c.tip.x + c.radius;
  tmax.y = c.tip.y + c.radius;
  tmax.z = c.tip.z + c.radius;

  sm_vec3 cmin, cmax;
  glm_vec3_minv(bmin.data, tmin.data, cmin.data);
  glm_vec3_maxv(bmax.data, tmax.data, cmax.data);

  glm_vec3_copy(cmin.data, result.min.data);
  glm_vec3_copy(cmax.data, result.max.data);

  return result;
}

sm_bounding_box_s sm_shape_get_aabb_triangle(sm_triangle_s t)
{

  sm_bounding_box_s result;

  sm_vec3 tmin;
  glm_vec3_minv(t.p2.data, t.p1.data, tmin.data);
  glm_vec3_minv(t.p0.data, tmin.data, tmin.data);

  sm_vec3 tmax;
  glm_vec3_maxv(t.p2.data, t.p1.data, tmax.data);
  glm_vec3_maxv(t.p0.data, tmax.data, tmax.data);

  glm_vec3_copy(tmin.data, result.min.data);
  glm_vec3_copy(tmax.data, result.max.data);

  return result;
}

sm_bounding_box_s sm_shape_get_positions_aabb(SM_ARRAY(sm_vec3) positions)
{

  sm_vec3 min_vert;
  sm_vec3 max_vert;

  if (positions != NULL) {
    glm_vec3_copy(positions[0].data, min_vert.data);
    glm_vec3_copy(positions[0].data, max_vert.data);

    for (size_t i = 1; i < SM_ARRAY_LEN(positions); ++i) {
      glm_vec3_minv(min_vert.data, positions[i].data, min_vert.data);
      glm_vec3_maxv(max_vert.data, positions[i].data, max_vert.data);
    }
  }

  // Create the bounding box
  sm_bounding_box_s aabb;
  glm_vec3_copy(min_vert.data, aabb.min.data);
  glm_vec3_copy(max_vert.data, aabb.max.data);

  return aabb;
}
