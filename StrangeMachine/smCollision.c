#include "smpch.h"

#include "math/smMath.h"

#include "core/smCore.h"
#include "smCollision.h"
#include "smShapes.h"
#include "util/colors.h"

#define SM_FLT_EPSILON 1.192092896e-07

void sm__colision_closest_point_on_line_segment(vec3 a, vec3 b, vec3 point, vec3 out)
{

    sm_vec3 ab;
    glm_vec3_sub(b, a, ab.data);

    sm_vec3 ap;
    glm_vec3_sub(point, a, ap.data);

    f32 t = glm_vec3_dot(ap.data, ab.data) / glm_vec3_dot(ab.data, ab.data);

    t = fminf(fmaxf(t, 0.0f), 1.0f);

    vec3 closest_point;
    glm_vec3_scale(ab.data, t, closest_point);
    glm_vec3_add(a, closest_point, out);
}

void sm_collision_capsules(sm_capsule_s a, sm_capsule_s b, sm_intersect_result_s *result)
{
    // capsule A:
    vec3 a_norm;
    glm_vec3_sub(a.tip.data, a.base.data, a_norm);
    glm_vec3_normalize(a_norm);

    vec3 a_line_end_offset;
    glm_vec3_scale(a_norm, a.radius, a_line_end_offset);

    vec3 a_a, a_b;
    glm_vec3_add(a.base.data, a_line_end_offset, a_a);
    glm_vec3_sub(a.tip.data, a_line_end_offset, a_b);

    // capsule B:
    vec3 b_norm;
    glm_vec3_sub(b.tip.data, b.base.data, b_norm);
    glm_vec3_normalize(b_norm);

    vec3 b_line_end_offset;
    glm_vec3_scale(b_norm, b.radius, b_line_end_offset);

    vec3 b_a, b_b;
    glm_vec3_add(b.base.data, b_line_end_offset, b_a);
    glm_vec3_sub(b.tip.data, b_line_end_offset, b_b);

    // vectors between line endpoints:
    vec3 v0, v1, v2, v3;
    glm_vec3_sub(b_a, a_a, v0);
    glm_vec3_sub(b_b, a_a, v1);
    glm_vec3_sub(b_a, a_b, v2);
    glm_vec3_sub(b_b, a_b, v3);

    // squared distances
    f32 d0 = glm_vec3_norm2(v0);
    f32 d1 = glm_vec3_norm2(v1);
    f32 d2 = glm_vec3_norm2(v2);
    f32 d3 = glm_vec3_norm2(v3);

    // select best potential endpoint on capsule A:
    vec3 a_best_point;
    if (d2 < d0 || d2 < d1 || d3 < d0 || d3 < d1)
    {
        glm_vec3_copy(a_b, a_best_point);
    }
    else
    {
        glm_vec3_copy(a_a, a_best_point);
    }

    // select point on capsule B line segment nearest to best potential endpoint
    // on A capsule:
    vec3 b_best_point;
    sm__colision_closest_point_on_line_segment(b_a, b_b, a_best_point, b_best_point);

    // now do the same for capsule A segment:
    sm__colision_closest_point_on_line_segment(a_a, a_b, b_best_point, a_best_point);

    // Finally, sphere collision:
    vec3 v;
    glm_vec3_sub(a_best_point, b_best_point, v);
    f32 d = glm_vec3_norm(v); // vector length

    /* vec3 N = vec3_sub(bestA, bestB); */
    /* f32 len = vec3_len(N); */

    glm_vec3_divs(v, d, v);
    vec3 position;
    glm_vec3_scale(v, a.radius, position);
    glm_vec3_sub(a_best_point, position, position);

    f32 depth = a.radius + b.radius - d;

    glm_vec3_copy(v, result->normal.data);
    glm_vec3_copy(position, result->position.data);

    result->valid = depth > 0.0f;
    result->depth = depth;
}

void sm_collision_sphere_triangle(sm_sphere_s s, sm_triangle_s t, sm_world_s *world, sm_intersect_result_s *result)
{

    /* vec3 center = s.center; */
    f32 radius = s.radius;

    vec3 v1, v2;
    glm_vec3_sub(t.p1.data, t.p0.data, v1);
    glm_vec3_sub(t.p2.data, t.p0.data, v2);

    vec3 n;
    glm_vec3_cross(v1, v2, n);
    glm_vec3_normalize(n);

    /* assert that the triangle is not degenerate. */
    SM_ASSERT(glm_vec3_norm2(n) > 0.0f);

    /* Find the nearest feature on the triangle to the sphere. */
    vec3 sub;
    glm_vec3_sub(s.center.data, t.p0.data, sub);
    f32 d = glm_vec3_dot(sub, n);

    /* If the center of the sphere is farther from the plane of the triangle */
    /* than the radius of the sphere, then there cannot be an intersection. */
    bool no_intersection = (d < -radius || d > radius);

    /* Project the center of the sphere onto the plane of the triangle. */
    vec3 point0;
    glm_vec3_scale(n, d, point0);
    glm_vec3_sub(s.center.data, point0, point0); /* projected sphere center on triangle plane */

    /* Compute the cross products of the vector from the base of each edge to */
    /* the point with each edge vector. */
    vec3 edge0, edge1, edge2;
    glm_vec3_sub(t.p1.data, t.p0.data, edge0);
    glm_vec3_sub(t.p2.data, t.p1.data, edge1);
    glm_vec3_sub(t.p0.data, t.p2.data, edge2);

    vec3 p0, p1, p2;
    glm_vec3_sub(point0, t.p0.data, p0);
    glm_vec3_sub(point0, t.p1.data, p1);
    glm_vec3_sub(point0, t.p2.data, p2);

    vec3 c0, c1, c2;
    glm_vec3_cross(p0, edge0, c0);
    glm_vec3_cross(p1, edge1, c1);
    glm_vec3_cross(p2, edge2, c2);

    // If the cross product points in the same direction as the normal the the
    // point is inside the edge (it is zero if is on the edge).
    bool intersection = ((glm_vec3_dot(c0, n) <= 0.0f) && (glm_vec3_dot(c1, n) <= 0.0f)) && (glm_vec3_dot(c2, n) <= 0.0f);

    bool inside = intersection && !no_intersection;

    f32 radiussq = radius * radius; // sphere radius squared

    // Find the nearest point on each edge.

    // Edge 0,1
    vec3 point1;
    sm__colision_closest_point_on_line_segment(t.p0.data, t.p1.data, s.center.data, point1);

    // If the distance to the center of the sphere to the point is less than
    // the radius of the sphere then it must intersect.
    vec3 res1;
    glm_vec3_sub(s.center.data, point1, res1);
    f32 distsq = glm_vec3_norm2(res1);
    intersection |= distsq <= radiussq;

    // Edge 1,2
    vec3 point2;
    sm__colision_closest_point_on_line_segment(t.p1.data, t.p2.data, s.center.data, point2);

    // If the distance to the center of the sphere to the point is less than
    // the radius of the sphere then it must intersect.
    glm_vec3_sub(s.center.data, point2, res1);
    distsq = glm_vec3_norm2(res1);
    intersection |= distsq <= radiussq;

    // Edge 2,0
    vec3 point3;
    sm__colision_closest_point_on_line_segment(t.p2.data, t.p0.data, s.center.data, point3);

    // If the distance to the center of the sphere to the point is less than
    // the radius of the sphere then it must intersect.
    glm_vec3_sub(s.center.data, point3, res1);
    distsq = glm_vec3_norm2(res1);
    intersection |= distsq <= radiussq;

    b8 intersects = intersection && !no_intersection;

    if (intersects)
    {
        vec3 best_point;
        glm_vec3_copy(point0, best_point);

        if (!inside)
        {
            // If the sphere center's projection on the triangle plane is not
            // within the triangle,
            // determine the closest point on triangle to the sphere center
            glm_vec3_sub(point1, s.center.data, res1);
            f32 best_dist = glm_vec3_norm2(res1);
            glm_vec3_copy(point1, best_point);

            glm_vec3_sub(point2, s.center.data, res1);
            f32 dist = glm_vec3_norm2(res1);

            if (dist < best_dist)
            {
                glm_vec3_copy(point2, best_point);
                best_dist = dist;
            }

            glm_vec3_sub(point3, s.center.data, res1);
            dist = glm_vec3_norm2(res1);
            if (dist < best_dist)
            {
                glm_vec3_copy(point3, best_point);
                best_dist = dist;
            }
        }

        vec3 intersection_vector;
        glm_vec3_sub(s.center.data, best_point, intersection_vector);
        f32 intersection_length = glm_vec3_norm(intersection_vector);

        result->valid = true;
        result->depth = radius - intersection_length;
        glm_vec3_copy(best_point, result->position.data);
        glm_vec3_divs(intersection_vector, intersection_length, result->normal.data);

        sm_mat4 inv;
        glm_mat4_inv(world->matrix.data, inv.data);

        sm_vec3 vel;
        glm_mat4_mulv3(inv.data, best_point, 1.0f, vel.data);
        glm_mat4_mulv3(world->prev_matrix.data, vel.data, 1.0f, vel.data);
        glm_vec3_sub(best_point, vel.data, vel.data);

        glm_vec3_copy(vel.data, result->velocity.data);
    }
}

void sm_collision_capsule_triangle(sm_capsule_s c, sm_triangle_s t, sm_world_s *world, sm_intersect_result_s *result)
{

    vec3 base, tip;
    glm_vec3_copy(c.base.data, base);
    glm_vec3_copy(c.tip.data, tip);
    f32 radius = c.radius;

    vec3 line_end_offset;
    glm_vec3_sub(tip, base, line_end_offset);
    glm_vec3_normalize(line_end_offset);
    glm_vec3_scale(line_end_offset, radius, line_end_offset);

    vec3 a, b;
    glm_vec3_add(base, line_end_offset, a);
    glm_vec3_sub(tip, line_end_offset, b);

    // Compute the plane of the triangle (has to be normalized).
    vec3 n, v1, v2;
    glm_vec3_sub(t.p1.data, t.p0.data, v1);
    glm_vec3_sub(t.p2.data, t.p0.data, v2);
    glm_vec3_cross(v1, v2, n);
    glm_vec3_normalize(n);

    // assert that the triangle is not degenerate.
    SM_ASSERT(glm_vec3_norm2(n) > 0.0f);

    vec3 reference_point;
    vec3 capsule_normal;
    glm_vec3_sub(b, a, capsule_normal);
    glm_vec3_normalize(capsule_normal);

    if (fabsf(glm_vec3_dot(n, capsule_normal)) < SM_FLT_EPSILON)
    {
        // Capsule line cannot be intersected with triangle plane (they are  parallel)
        // In this case, just take a point from triangle
        glm_vec3_copy(t.p0.data, reference_point);
    }
    else
    {
        /* Intersect capsule line with triangle plane: */
        vec3 left_top;
        glm_vec3_sub(base, t.p0.data, left_top);
        f32 right_top = fabsf(glm_vec3_dot(n, capsule_normal));
        glm_vec3_divs(left_top, right_top, left_top);
        f32 r = glm_vec3_dot(n, left_top);

        vec3 line_plane_intersection;
        glm_vec3_scale(capsule_normal, r, line_plane_intersection);
        glm_vec3_add(line_plane_intersection, base, line_plane_intersection);

        // Compute the cross products of the vector from the base of each edge
        // to the point with each edge vector.
        vec3 e0, e1, e2;
        glm_vec3_sub(t.p1.data, t.p0.data, e0);
        glm_vec3_sub(t.p2.data, t.p1.data, e1);
        glm_vec3_sub(t.p0.data, t.p2.data, e2);

        vec3 p0, p1, p2;
        glm_vec3_sub(line_plane_intersection, t.p0.data, p0);
        glm_vec3_sub(line_plane_intersection, t.p1.data, p1);
        glm_vec3_sub(line_plane_intersection, t.p2.data, p2);

        vec3 c0, c1, c2;
        glm_vec3_cross(p0, e0, c0);
        glm_vec3_cross(p1, e1, c1);
        glm_vec3_cross(p2, e2, c2);

        // If the cross product points in the same direction as the normal the
        // the point is inside the edge (it is zero if is on the edge).
        bool inside = glm_vec3_dot(c0, n) <= 0 && glm_vec3_dot(c1, n) <= 0 && glm_vec3_dot(c2, n) <= 0;

        if (inside)
        {
            glm_vec3_copy(line_plane_intersection, reference_point);
        }
        else
        {
            vec3 point1, point2, point3;
            // Edge 1:
            sm__colision_closest_point_on_line_segment(t.p0.data, t.p1.data, line_plane_intersection, point1);

            // Edge 2:
            sm__colision_closest_point_on_line_segment(t.p1.data, t.p2.data, line_plane_intersection, point2);

            // Edge 3:
            sm__colision_closest_point_on_line_segment(t.p2.data, t.p0.data, line_plane_intersection, point3);

            glm_vec3_copy(point1, reference_point);

            vec3 best_dist_vec;
            glm_vec3_sub(point1, line_plane_intersection, best_dist_vec);
            f32 best_dist = glm_vec3_norm2(best_dist_vec);

            vec3 dist_vec;
            f32 dist;

            glm_vec3_sub(point2, line_plane_intersection, dist_vec);
            dist = fabsf(glm_vec3_norm2(dist_vec));

            if (dist < best_dist)
            {
                best_dist = dist;
                glm_vec3_copy(point2, reference_point);
            }

            glm_vec3_sub(point3, line_plane_intersection, dist_vec);
            dist = fabsf(glm_vec3_norm2(dist_vec));

            if (dist < best_dist)
            {
                best_dist = dist;
                glm_vec3_copy(point3, reference_point);
            }
        }
    }

    // Place a sphere on closest point on line segment to intersection:
    sm_vec3 center;
    sm__colision_closest_point_on_line_segment(a, b, reference_point, center.data);

    sm_sphere_s sph = {.center = center, .radius = radius};

    sm_collision_sphere_triangle(sph, t, world, result);
}

void sm_collision_sphere_mesh(sm_sphere_s s, sm_resource_mesh_s *mesh, sm_intersect_result_s *result)
{

    sm_triangle_s t;

    sm_bounding_box_s s_aabb = sm_shape_get_aabb_sphere(s);

    /* sm_bounding_box_s mesh_aabb = sm_mesh_component_get_aabb(mesh); */
    /* if (!sm_collision_aabbs(s_aabb, mesh_aabb)) return; */

    for (size_t i = 0; i < SM_ARRAY_LEN(mesh->positions); i += 3)
    {

        glm_vec3_copy(mesh->positions[i + 0].data, t.p0.data);
        glm_vec3_copy(mesh->positions[i + 1].data, t.p1.data);
        glm_vec3_copy(mesh->positions[i + 2].data, t.p2.data);

        sm_bounding_box_s t_aabb = sm_shape_get_aabb_triangle(t);

        if (!sm_collision_aabbs(s_aabb, t_aabb)) continue;

        sm_collision_sphere_triangle(s, t, NULL, result);

        if (result->valid)
        {
            return;
        }
    }
}

// collision_check_spheres - Check if two spheres are colliding.
void sm_collision_spheres(sm_sphere_s s1, sm_sphere_s s2, sm_intersect_result_s *result)
{

    vec3 d;
    glm_vec3_sub(s1.center.data, s2.center.data, d);

    f32 dist = glm_vec3_norm(d);

    if (dist < s1.radius + s2.radius)
    {
        // calculate the depth of the collision
        // and the normal of the collision
        // (the normal is the direction of the collision)
        // (the depth is the distance from the center of the sphere to the collision)
        f32 depth = s1.radius + s2.radius - dist;
        glm_vec3_normalize(d);

        result->valid = true;
        result->depth = depth;
        glm_vec3_copy(d, result->normal.data);
    }
}

void sm_collision_sphere_cube(sm_sphere_s s, sm_cube_s c, sm_intersect_result_s *result)
{

    sm_vec3 d;
    glm_vec3_sub(s.center.data, c.center.data, d.data);

    // check if the sphere is inside the cube
    if (fabsf(d.x) <= c.size.x / 2.0f && fabsf(d.y) <= c.size.y / 2.0f && fabsf(d.z) <= c.size.z / 2.0f)
    {
        // calculate the depth of the collision
        // and the normal of the collision
        // (the normal is the direction of the collision)
        // (the depth is the distance from the center of the sphere to the collision)
        f32 depth = c.size.x / 2.0f - fabsf(d.x);
        sm_vec3 normal;
        normal.x = d.x > 0.0f ? -1.0f : 1.0f;
        normal.y = 0.0f;
        normal.z = 0.0f;

        result->valid = true;
        result->depth = depth;
        glm_vec3_copy(normal.data, result->normal.data);
    }
}

b8 sm_collision_aabbs(sm_bounding_box_s bb1, sm_bounding_box_s bb2)
{
    b8 collision = true;

    if ((bb1.max.x >= bb2.min.x) && (bb1.min.x <= bb2.max.x))
    {
        if ((bb1.max.y < bb2.min.y) || (bb1.min.y > bb2.max.y)) collision = false;
        if ((bb1.max.z < bb2.min.z) || (bb1.min.z > bb2.max.z)) collision = false;
    }
    else collision = false;

    return collision;
}
