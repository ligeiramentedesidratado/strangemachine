#ifndef SM_COLLISION_H
#define SM_COLLISION_H

#include "smpch.h"

#include "ecs/smComponents.h"
#include "resource/smResource.h"
#include "smShapes.h"

typedef struct sm__intersect_result_s
{
    b8 valid;
    sm_vec3 position, normal, velocity;
    f32 depth;

} sm_intersect_result_s;

void sm_collision_capsules(sm_capsule_s a, sm_capsule_s b, sm_intersect_result_s *result);
void sm_collision_sphere_triangle(sm_sphere_s s, sm_triangle_s t, sm_world_s *world, sm_intersect_result_s *result);
/* void sm_collision_capsule_triangle(sm_capsule_s c, sm_triangle_s t, sm_intersect_result_s *result); */
void sm_collision_capsule_triangle(sm_capsule_s c, sm_triangle_s t, sm_world_s *world, sm_intersect_result_s *result);
void sm_collision_spheres(sm_sphere_s a, sm_sphere_s b, sm_intersect_result_s *result);
void sm_collision_capsule_mesh(sm_capsule_s c, const sm_resource_mesh_s *mesh, sm_intersect_result_s *result);
void sm_collision_sphere_mesh(sm_sphere_s s, sm_resource_mesh_s *mesh, sm_intersect_result_s *result);
b8 sm_collision_aabbs(sm_bounding_box_s bb1, sm_bounding_box_s bb2);

#endif // SM_COLLISION_H
