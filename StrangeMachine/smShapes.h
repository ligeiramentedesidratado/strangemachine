#ifndef SM_SHAPES_H
#define SM_SHAPES_H

#include "smpch.h"

#include "math/smMath.h"
#include "core/smCore.h"

typedef struct sm__sphere_s {
  sm_vec3 center;
  f32 radius;

} sm_sphere_s;

typedef struct sm__triangle_s {
  sm_vec3 p0;
  sm_vec3 p1;
  sm_vec3 p2;

} sm_triangle_s;

typedef struct sm__capsule_s {
  sm_vec3 base;
  sm_vec3 tip;
  f32 radius;

} sm_capsule_s;

typedef struct sm__bounding_box_s {
  sm_vec3 min;
  sm_vec3 max;

} sm_bounding_box_s;

// cube
typedef struct sm__cube_s {
  sm_vec3 center;
  sm_vec3 size;

} sm_cube_s;

typedef union sm__shape_u {

  sm_cube_s cube;
  sm_capsule_s capsule;
  sm_sphere_s sphere;
  sm_triangle_s triangle;
  void *mesh_ref;

} sm_shape_u;

sm_capsule_s sm_shape_capsule_new(sm_sphere_s s, f32 height);
sm_bounding_box_s sm_shape_get_aabb_sphere(sm_sphere_s s);
sm_bounding_box_s sm_shape_get_aabb_capsule(sm_capsule_s c);
sm_bounding_box_s sm_shape_get_aabb_triangle(sm_triangle_s t);
sm_bounding_box_s sm_shape_get_positions_aabb(SM_ARRAY(sm_vec3) positions);

#endif // SM_SHAPES_H
