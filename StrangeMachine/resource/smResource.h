#ifndef SM_RESOURCE_H
#define SM_RESOURCE_H

#include "smpch.h"

#include "ecs/smComponents.h"
#include "ecs/smSystem.h"
#include "math/smMath.h"
#include "smShapes.h"

/* #include "scene/smScene.h" */
#include "core/smCore.h"

typedef enum
{
    SM_RESOURCE_TYPE_NONE = 0,
    SM_RESOURCE_TYPE_TEXTURE = 1 << 0,
    SM_RESOURCE_TYPE_MATERIAL = 1 << 1,
    SM_RESOURCE_TYPE_MESH = 1 << 2,
    SM_RESOURCE_TYPE_SCENE = 1 << 3,
    SM_RESOURCE_TYPE_SHADER = 1 << 4,
    SM_RESOURCE_TYPE_AUDIO = 1 << 5,

    SM_RESOURCE_TYPE_MASK_ALL = SM_RESOURCE_TYPE_TEXTURE | SM_RESOURCE_TYPE_AUDIO | SM_RESOURCE_TYPE_SHADER | SM_RESOURCE_TYPE_SCENE |
                                SM_RESOURCE_TYPE_MATERIAL | SM_RESOURCE_TYPE_MESH

} sm_resource_manager_type_e;

#define SM_RESOURCE_HEADER_NAME_SIZE 64

typedef struct sm__resource_header_s
{
    u32 signature;
    u32 version;
    sm_resource_manager_type_e resouce_type;
    u32 uuid;
    char name[SM_RESOURCE_HEADER_NAME_SIZE];

} sm_resource_header_s;

/* magic number 'SMR' */
#define SM_RESOURCE_MAGIC_NUMBER   4290632
#define SM_RESOURCE_VERSION_NUMBER 0x01u

sm_resource_header_s sm_resource_header_new(sm_resource_manager_type_e resource_type, u32 uuid, const SM_STRING name);

typedef struct sm__resource_entity_s
{

    u32 index;
    sm_resource_manager_type_e type;

} sm_resource_entity_s;

#define SM_RESOURCE_INVALID_HANDLE (0xFFFFFFFFu)

#define SM_RESOURCE_INVALID_ENTITY ((sm_resource_entity_s){.index = SM_RESOURCE_INVALID_HANDLE, .type = SM_RESOURCE_TYPE_NONE})

#define SM_RESOURCE_IS_VALID(RESOURCE_ENTITY) ((RESOURCE_ENTITY.index != SM_RESOURCE_INVALID_HANDLE) && (RESOURCE_ENTITY.type != SM_RESOURCE_TYPE_NONE))

typedef enum
{
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE = 1, // 8 bit per pixel (no alpha)
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA,    // 8*2 bpp (2 channels)
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R5G6B5,        // 16 bpp
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8,        // 24 bpp
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R5G5B5A1,      // 16 bpp (1 bit alpha)
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R4G4B4A4,      // 16 bpp (4 bit alpha)
    SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8,      // 32 bpp
    SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGB,        // 4 bpp (no alpha)
    SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT1_RGBA,       // 4 bpp (1 bit alpha)
    SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT3_RGBA,       // 8 bpp
    SM_TEXTURE_PIXELFORMAT_COMPRESSED_DXT5_RGBA,       // 8 bpp
    SM__TEXTURE_PIXELFORMAT_ENUM_SIZE = 0x7fffffff     /* force 32-bit size enum */
} sm_pixel_format_e;

typedef struct sm_resource_texture_s
{

    sm_resource_header_s header;

    u32 width, height;
    sm_pixel_format_e format;

    u32 handle; /* OpenGL handle */
    void *raw_data;

    sm_rc_s ref_count;

    enum sm_texture_component_flags_e
    {
        SM_TEXTURE_FLAG_NONE = 0,
        SM_TEXTURE_FLAG_DIRTY = 1 << 0,
        SM_TEXTURE_FLAG_ON_GPU = 1 << 1,
        SM_TEXTURE_FLAG_ON_CPU = 1 << 2,
        SM__TEXTURE_FLAG_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } flags;

} sm_resource_texture_s;

b8 sm_resource_texture_write(sm_resource_texture_s *texture);
b8 sm_resource_texture_read(sm_resource_texture_s *texture);
void sm_resource_texture_do(sm_resource_texture_s *texture);

sm_resource_entity_s sm_resource_texture_new_from_mem(sm_resource_texture_s *texture);
void sm_resource_texture_load_gpu(sm_resource_texture_s *texture);
void sm_resource_texture_unload_cpu(sm_resource_texture_s *texture);
void sm_resource_texture_unload_gpu(sm_resource_texture_s *texture);

void sm_resource_texture_dtor(sm_resource_texture_s *texture);
sm_resource_entity_s sm_resource_texture_make_reference(sm_resource_entity_s texture);

void sm_resource_texture_bind(sm_resource_texture_s *texture, u32 tex_index);
void sm_resource_texture_unbind(void);

void sm_resource_texture_set_dirty(sm_resource_texture_s *texture, b8 dirty);
b8 sm_resource_texture_is_dirty(const sm_resource_texture_s *texture);
void sm_resource_texture_set_on_cpu(sm_resource_texture_s *texture, b8 on_cpu);
b8 sm_resource_texture_is_on_cpu(const sm_resource_texture_s *texture);
void sm_resource_texture_set_on_gpu(sm_resource_texture_s *texture, b8 on_gpu);
b8 sm_resource_texture_is_on_gpu(const sm_resource_texture_s *texture);

typedef struct sm__resource_material_s
{

    sm_resource_header_s header;

    /* sm_shader_handler_s shader; */
    u32 diffuse_map;
    sm_vec4 diffuse_color;

    sm_rc_s ref_count;

    enum sm_material_component_flags_e
    {
        SM_MATERIAL_FLAG_NONE = 0,
        SM_MATERIAL_FLAG_DIRTY = 1 << 0,
        SM_MATERIAL_FLAG_ON_CPU = 1 << 1,
        SM_MATERIAL_FLAG_TWOSIDED = 1 << 2,
        SM__MATERIAL_FLAG_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } flags;

} sm_resource_material_s;

b8 sm_resource_material_write(sm_resource_material_s *material);
b8 sm_resource_material_read(sm_resource_material_s *material);
void sm_resource_material_do(sm_resource_material_s *material);

void sm_resource_material_dtor(sm_resource_material_s *material);
sm_resource_entity_s sm_resource_material_make_reference(sm_resource_entity_s material);

void sm_resource_material_set_dirty(sm_resource_material_s *material, b8 dirty);
b8 sm_resource_material_is_dirty(const sm_resource_material_s *material);
void sm_resource_material_set_twosided(sm_resource_material_s *material, b8 twosided);
b8 sm_resource_material_is_twosided(const sm_resource_material_s *material);
void sm_resource_material_set_on_cpu(sm_resource_material_s *material, b8 on_cpu);
b8 sm_resource_material_is_on_cpu(const sm_resource_material_s *material);

// sm__resource_mesh_s should maintain a copy of the mesh data on
// the CPU as well as on the GPU. Store arrays for the position, normal, tex
// coordinates, weights, and influences that define each vertex. Include an
// optional vector for indices
typedef struct sm__resource_mesh_s
{

    sm_resource_header_s header;

    SM_ARRAY(sm_vec3) positions;
    SM_ARRAY(sm_vec2) uvs;
    SM_ARRAY(sm_vec4) colors;
    SM_ARRAY(sm_vec3) normals;
    SM_ARRAY(u32) indices;

    struct
    {
        SM_ARRAY(sm_vec4) weights;
        SM_ARRAY(sm_ivec4) influences;
        u32 vbos[2]; /* openGL vertex buffer objects */

        // add an additional copy of the pose and normal data, as well as a matrix
        // palette to use for CPU skinning.
        mat4 *pose_palette;
    } skin_data;

    u32 vao;     /* openGL vertex array object */
    u32 vbos[4]; /* openGL vertex buffer objects */
    u32 ebo;     /* openGL vertex buffer object */

    u32 material;

    sm_bounding_box_s aabb;

    sm_rc_s ref_count;

    enum sm_mesh_component_flags_e
    {
        SM_MESH_FLAG_NONE = 0,
        SM_MESH_FLAG_DIRTY = 1 << 0,
        SM_MESH_FLAG_RENDERABLE = 1 << 1,
        SM_MESH_FLAG_ON_CPU = 1 << 2,
        SM_MESH_FLAG_ON_GPU = 1 << 3,
        SM_MESH_FLAG_SKINNED = 1 << 4,
        SM_MESH_FLAG_DRAW_AABB = 1 << 5,
        SM__MESH_FLAG_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } flags;

} sm_resource_mesh_s;

b8 sm_resource_mesh_write(sm_resource_mesh_s *mesh);
b8 sm_resource_mesh_read(sm_resource_mesh_s *mesh);
void sm_resource_mesh_do(sm_resource_mesh_s *mesh);

sm_resource_entity_s sm_resource_scene_make_reference(sm_resource_entity_s scene);
void sm_resource_mesh_load_gpu(sm_resource_mesh_s *mesh);
void sm_resource_mesh_unload_cpu(sm_resource_mesh_s *mesh);
void sm_resource_mesh_unload_gpu(sm_resource_mesh_s *mesh);

void sm_resource_mesh_dtor(sm_resource_mesh_s *mesh);
sm_resource_entity_s sm_resource_mesh_make_reference(sm_resource_entity_s mesh);

void sm_resource_mesh_calculate_aabb(sm_resource_mesh_s *mesh);
sm_bounding_box_s sm_resource_mesh_get_aabb(sm_resource_mesh_s *mesh);

void sm_resource_mesh_set_renderable(sm_resource_mesh_s *mesh, bool renderable);
b8 sm_resource_mesh_is_renderable(const sm_resource_mesh_s *mesh);
void sm_resource_mesh_set_dirty(sm_resource_mesh_s *mesh, bool dirty);
b8 sm_resource_mesh_is_dirty(const sm_resource_mesh_s *mesh);
void sm_resource_mesh_set_twosided(sm_resource_mesh_s *mesh, b8 twosided);
b8 sm_resource_mesh_is_twosided(const sm_resource_mesh_s *mesh);
void sm_resource_mesh_set_on_gpu(sm_resource_mesh_s *mesh, b8 on_gpu);
b8 sm_resource_mesh_is_on_gpu(const sm_resource_mesh_s *mesh);
void sm_resource_mesh_set_on_cpu(sm_resource_mesh_s *mesh, b8 on_cpu);
b8 sm_resource_mesh_is_on_cpu(const sm_resource_mesh_s *mesh);
void sm_resource_mesh_set_skinned(sm_resource_mesh_s *mesh, b8 skinned);
b8 sm_resource_mesh_is_skinned(const sm_resource_mesh_s *mesh);
void sm_resource_mesh_set_draw_aabb(sm_resource_mesh_s *mesh, b8 draw_aabb);
b8 sm_resource_mesh_is_draw_aabb(const sm_resource_mesh_s *mesh);

#define SM_SCENE_ROOT_NODE      0u          /* 0 */
#define SM_SCENE_INVALID_NODE   0xFFFFFFFFu /* -1 */
#define SM_SCENE_NO_PARENT_NODE 0xFFFFFFFEu /* -2 */

typedef struct sm__node_s
{

    SM_STRING name;
    sm_entity_s entity;

    u32 parent;
    SM_ARRAY(u32) children;

} sm_node_s;

static inline sm_node_s sm_node_new_invalid(void)
{

    sm_node_s node = {
        .name = NULL,
        .entity = SM_ENTITY_INVALID( ),
        .parent = SM_SCENE_INVALID_NODE,
        .children = NULL,
    };

    return node;
}

typedef struct sm__resource_scene_s
{

    sm_resource_header_s header;
    sm_entity_s current_camera;
    SM_ARRAY(sm_node_s) nodes;

    sm_rc_s ref_count;
    sm_ecs_world_s *ecs;

    enum sm_resource_scene_flags_e
    {
        SM_SCENE_FLAG_NONE = 0,
        SM_SCENE_FLAG_DIRTY = 1 << 1,
        SM_SCENE_FLAG_ON_CPU = 1 << 2,
        SM__SCENE_FLAG_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } flags;

} sm_resource_scene_s;

b8 sm_resource_scene_write(sm_resource_scene_s *scene);
b8 sm_resource_scene_read(sm_resource_scene_s *scene);
void sm_resource_scene_dtor(sm_resource_scene_s *scene);
void sm_resource_scene_do(sm_resource_scene_s *scene, f32 dt);

void sm_resource_scene_set_parent(sm_resource_scene_s *scene, u32 node_index, u32 node_parent);
u32 sm_resource_scene_get_parent(sm_resource_scene_s *scene, u32 node_index);
void sm_resource_scene_set_name(sm_resource_scene_s *scene, u32 node_index, const SM_STRING name);
const SM_STRING sm_resource_scene_get_name(sm_resource_scene_s *scene, u32 node_index);
void sm_resource_scene_set_current_camera(sm_resource_scene_s *scene, sm_entity_s handle);
sm_entity_s sm_resource_scene_get_current_camera(sm_resource_scene_s *scene);
b8 sm_resource_scene_has_parent(sm_resource_scene_s *scene, u32 node_index);
void sm_resource_scene_remove_child(sm_resource_scene_s *scene, u32 node_index, u32 node_child);
u32 sm_resource_scene_add_child(sm_resource_scene_s *scene, u32 node_index, u32 node_child);
u32 sm_resource_scene_new_node(sm_resource_scene_s *scene);
b8 sm_resource_scene_set_entity(sm_resource_scene_s *scene, u32 node_index, sm_entity_s entity);
u32 sm_resource_scene_get_by_name(sm_resource_scene_s *scene, const SM_STRING name);
u32 sm_resource_scene_get_by_entity(sm_resource_scene_s *scene, sm_entity_s entity);
u32 sm_resource_scene_get_root(sm_resource_scene_s *scene);
sm_entity_s sm_resource_scene_get_entity(sm_resource_scene_s *scene, u32 node_index);
void sm_resource_scene_for_each(
    sm_resource_scene_s *scene, u32 node_index, void (*callback)(sm_resource_scene_s *scene, u32 node_index, void *user_data), void *user_data);

void sm_resource_scene_set_dirty(sm_resource_scene_s *scene, b8 dirty);
b8 sm_resource_scene_is_dirty(const sm_resource_scene_s *scene);
void sm_resource_scene_set_on_cpu(sm_resource_scene_s *scene, b8 on_cpu);
b8 sm_resource_scene_is_on_cpu(const sm_resource_scene_s *scene);

typedef struct sm__resource_shader_s
{

    u32 program; /* GL handle */

    SM_STRING vertex_data;
    SM_STRING fragment_data;

    sm_rc_s ref_count;

    enum sm_resource_shader_flags_e
    {
        SM_SHADER_FLAG_NONE = 0,
        SM_SHADER_FLAG_DIRTY = 1 << 1,
        SM_SHADER_FLAG_ON_GPU = 1 << 2,
        SM_SHADER_FLAG_ON_CPU = 1 << 3,
        SM__SHADERFLAG_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } flags;

} sm_resource_shader_s;

b8 sm_resource_shader_write(sm_resource_shader_s *shader);
b8 sm_resource_shader_read(const SM_STRING file_path, sm_resource_shader_s *shader);
void sm_resource_shader_dtor(sm_resource_shader_s *shader);
void sm_resource_shader_do(sm_resource_shader_s *shader);

sm_resource_entity_s sm_resource_shader_make_reference(sm_resource_entity_s entity);
void sm_resource_shader_load_gpu(sm_resource_shader_s *shader);

void sm_resource_shader_bind(sm_resource_entity_s entity);
void sm_resource_shader_ubind(void);

void sm_resource_shader_set_on_cpu(sm_resource_shader_s *shader, b8 on_cpu);
b8 sm_resource_shader_is_on_cpu(const sm_resource_shader_s *shader);
void sm_resource_shader_set_on_gpu(sm_resource_shader_s *shader, b8 on_cpu);
b8 sm_resource_shader_is_on_gpu(const sm_resource_shader_s *shader);

typedef struct sm__resource_s
{
    sm_resource_entity_s entity;
    void *data;

} sm_resource_s;

b8 sm_resource_manager_init(const char *folder);
void sm_resource_manager_teardown(void);
void sm_resource_manager_do(f32 dt);

SM_STRING sm_resource_manager_construct_path(const SM_STRING name, sm_resource_manager_type_e type);

u32 sm_resource_manager_get_uuid(sm_resource_entity_s entity);
void *sm_resource_manager_get_data(sm_resource_entity_s entity);
sm_resource_entity_s sm_resource_manager_get_by_name(const SM_STRING file, sm_resource_manager_type_e type);
sm_resource_entity_s sm_resource_manager_get_by_uuid(u32 uuid);

sm_resource_entity_s sm___resource_mock_push(const void *data, sm_resource_manager_type_e type);
b8 sm___resource_mock_write(void);
sm_resource_texture_s *sm___resource_get_texture_pointer(void);
b8 sm___resource_mock_init(const SM_STRING root_folder);
void sm___resource_mock_teardown(void);

#endif /* SM_RESOURCE_H */
