#include "smCollision.h"
#include "smpch.h"

#include "core/smCore.h"

#include "resource/smResource.h"

#include "renderer/GL/smGL.h"
#include "renderer/smRenderer.h"

#include "ecs/smComponents.h"
#include "ecs/smECS.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "RESOURCE MANAGER"

typedef struct
{

    SM_STRING root_folder;

    SM_ARRAY(sm_resource_texture_s) textures;
    SM_ARRAY(sm_resource_material_s) materials;
    SM_ARRAY(sm_resource_mesh_s) meshes;
    SM_ARRAY(sm_resource_scene_s) scenes;
    SM_ARRAY(sm_resource_shader_s) shaders;

    // you can either use uuid or string to query resources
    sm_hashmap_str_m *strmap;
    sm_hashmap_u32_m *u32map;

} resource_manager_s;

resource_manager_s *RESOURCE_MANAGER = NULL;

char *expected_ext = "png;mp3;ogg;smshader;smscene;smmesh;smmaterial;smtexture"; /* supported resource
                                                                          types */

#define SM_TEXTURE_EXT  ".smtexture"
#define SM_MATERIAL_EXT ".smmaterial"
#define SM_MESH_EXT     ".smmesh"
#define SM_SCENE_EXT    ".smscene"
#define SM_SHADER_EXT   ".smshader"

#define SM_TEXTURE_FOLDER  "textures/"
#define SM_MATERIAL_FOLDER "materials/"
#define SM_MESH_FOLDER     "meshes/"
#define SM_SCENE_FOLDER    "scenes/"
#define SM_SHADER_FOLDER   "shaders/"

/* private functions */
sm_resource_manager_type_e sm__resource_manager_get_file_type(const SM_STRING file);
void sm__resource_manager_dir_read(const char *folder);

sm_resource_header_s sm_resource_header_new(sm_resource_manager_type_e resource_type, u32 uuid, const SM_STRING name)
{

    struct sm__resource_header_s header = {
        .signature = SM_RESOURCE_MAGIC_NUMBER,
        .version = SM_RESOURCE_VERSION_NUMBER,
        .resouce_type = resource_type,
        .uuid = uuid,
        .name = {'\0'},
    };
    strncpy(header.name, name, SM_RESOURCE_HEADER_NAME_SIZE);

    return header;
}

b8 sm_resource_header_is_valid(sm_resource_header_s *header)
{
    return header->signature == SM_RESOURCE_MAGIC_NUMBER && header->version == SM_RESOURCE_VERSION_NUMBER &&
           header->resouce_type != SM_RESOURCE_TYPE_NONE && header->uuid != 0u && strlen(header->name) != 0;
}

b8 sm_resource_header_write(sm_resource_header_s *header, sm_file_handle_s *file)
{

    SM_ASSERT(file);

    if (!sm_resource_header_is_valid(header))
    {
        SM_LOG_ERROR("signature: 0x%x", header->signature);
        SM_LOG_ERROR("version: 0x%x", header->version);
        SM_LOG_ERROR("resource type: 0x%x", header->resouce_type);
        SM_LOG_ERROR("uuid: %u", header->uuid);
        SM_LOG_ERROR("name: %s", header->name);
        return false;
    }

    if (!sm_filesystem_write_bytes(file, &header->signature, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to write header signatue");
        return false;
    }

    if (!sm_filesystem_write_bytes(file, &header->version, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to write header version");
        return false;
    }

    if (!sm_filesystem_write_bytes(file, &header->resouce_type, sizeof(sm_resource_manager_type_e)))
    {
        SM_LOG_ERROR("failed to write header resouce_type");
        return false;
    }

    if (!sm_filesystem_write_bytes(file, &header->uuid, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to write header uuid");
        return false;
    }

    // Writing the name
    if (!sm_filesystem_write_bytes(file, header->name, SM_RESOURCE_HEADER_NAME_SIZE))
    {
        SM_LOG_ERROR("failed to write header name");
        return false;
    }

    return true;
}

b8 sm_resource_header_read(sm_resource_header_s *header, sm_file_handle_s *file)
{

    SM_ASSERT(file);

    if (sm_filesystem_size(file) < sizeof(sm_resource_header_s))
    {
        SM_LOG_ERROR("file is too small");
        return false;
    }

    if (!sm_filesystem_read_bytes(file, &header->signature, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to read header signature");
        return false;
    }

    if (!sm_filesystem_read_bytes(file, &header->version, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to read header version");
        return false;
    }

    if (!sm_filesystem_read_bytes(file, &header->resouce_type, sizeof(sm_resource_manager_type_e)))
    {
        SM_LOG_ERROR("failed to read header resouce_type");
        return false;
    }

    if (!sm_filesystem_read_bytes(file, &header->uuid, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to read header uuid");
        return false;
    }

    if (!sm_filesystem_read_bytes(file, header->name, SM_RESOURCE_HEADER_NAME_SIZE))
    {
        SM_LOG_ERROR(" failed to read header name");
        return false;
    }

    if (!sm_resource_header_is_valid(header))
    {
        SM_LOG_ERROR("signature: 0x%x", header->signature);
        SM_LOG_ERROR("version: 0x%x", header->version);
        SM_LOG_ERROR("resource type: 0x%x", header->resouce_type);
        SM_LOG_ERROR("uuid: %u", header->uuid);
        SM_LOG_ERROR("name: %s", header->name);
        return false;
    }

    return true;
}

b8 sm_resource_header_step_over(sm_file_handle_s *file)
{
    SM_ASSERT(file);

    // step over the header
    if (fseek(file->handle, sizeof(sm_resource_header_s), SEEK_SET))
    {
        SM_LOG_ERROR("fseek() failed");
        return false;
    }

    SM_ASSERT(ftell(file->handle) == sizeof(sm_resource_header_s));

    return true;
}

b8 sm_resource_manager_init(const SM_STRING root_folder)
{

    SM_ASSERT(RESOURCE_MANAGER == NULL && "resource already initialized");
    SM_ASSERT(root_folder);

    srand((u32)time(NULL));

    RESOURCE_MANAGER = SM_CALLOC(1, sizeof(resource_manager_s));
    SM_ASSERT(RESOURCE_MANAGER);

    RESOURCE_MANAGER->root_folder = SM_STRING_DUP(root_folder);

    RESOURCE_MANAGER->strmap = sm_hashmap_new_str( );
    if (!sm_hashmap_ctor_str(RESOURCE_MANAGER->strmap, 16, NULL, NULL))
    {
        SM_LOG_ERROR("failed to create hashmap");
        return false;
    }

    RESOURCE_MANAGER->u32map = sm_hashmap_new_u32( );
    if (!sm_hashmap_ctor_u32(RESOURCE_MANAGER->u32map, 16, NULL, NULL))
    {
        SM_LOG_ERROR("failed to create hashmap");
        return false;
    }

    sm__resource_manager_dir_read(RESOURCE_MANAGER->root_folder);

    return true;
}

b8 sm__reource_manager_dtor_cb(const SM_STRING key, void *value, void *user_data)
{

    SM_UNUSED(user_data);

    sm_resource_s *res = (sm_resource_s *)value;

    SM_FREE(res);
    SM_STRING_FREE((SM_STRING)key);

    return true;
}

void sm_resource_manager_do(f32 dt)
{

    SM_ASSERT(RESOURCE_MANAGER);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->shaders); ++i)
    {
        sm_resource_shader_s *shader = &RESOURCE_MANAGER->shaders[i];

        sm_resource_shader_do(shader);
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->scenes); ++i)
    {
        sm_resource_scene_s *scene = &RESOURCE_MANAGER->scenes[i];
        sm_resource_scene_do(scene, dt);
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->meshes); ++i)
    {
        sm_resource_mesh_s *mesh = &RESOURCE_MANAGER->meshes[i];
        sm_resource_mesh_do(mesh);
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->materials); ++i)
    {
        sm_resource_material_s *material = &RESOURCE_MANAGER->materials[i];
        sm_resource_material_do(material);
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->textures); ++i)
    {
        sm_resource_texture_s *texture = &RESOURCE_MANAGER->textures[i];
        sm_resource_texture_do(texture);
    }
}

void sm_resource_manager_teardown(void)
{

    SM_ASSERT(RESOURCE_MANAGER);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->shaders); ++i)
    {
        sm_resource_shader_s *shader = &RESOURCE_MANAGER->shaders[i];
        sm_resource_shader_dtor(shader);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->shaders);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->scenes); ++i)
    {
        sm_resource_scene_s *scene = &RESOURCE_MANAGER->scenes[i];
        sm_resource_scene_dtor(scene);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->scenes);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->meshes); ++i)
    {
        sm_resource_mesh_s *mesh = &RESOURCE_MANAGER->meshes[i];
        sm_resource_mesh_dtor(mesh);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->meshes);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->materials); ++i)
    {
        sm_resource_material_s *material = &RESOURCE_MANAGER->materials[i];
        sm_resource_material_dtor(material);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->materials);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->textures); ++i)
    {
        sm_resource_texture_s *texture = &RESOURCE_MANAGER->textures[i];
        sm_resource_texture_dtor(texture);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->textures);

    sm_hashmap_for_each_str(RESOURCE_MANAGER->strmap, sm__reource_manager_dtor_cb, NULL);

    sm_hashmap_dtor_str(RESOURCE_MANAGER->strmap);
    sm_hashmap_dtor_u32(RESOURCE_MANAGER->u32map);

    SM_STRING_FREE(RESOURCE_MANAGER->root_folder);

    SM_FREE(RESOURCE_MANAGER);
    RESOURCE_MANAGER = NULL;
}

SM_STRING sm_resource_manager_construct_path(const SM_STRING name, sm_resource_manager_type_e type)
{

    SM_ASSERT(RESOURCE_MANAGER);
    SM_ASSERT(name);

    SM_STRING file_path = NULL;

    switch (type)
    {

    case SM_RESOURCE_TYPE_TEXTURE:
        {

            file_path = SM_STRING_MALLOC(strlen(RESOURCE_MANAGER->root_folder) + strlen(SM_TEXTURE_FOLDER) + strlen(name) + strlen(SM_TEXTURE_EXT) + 1);
            strcpy(file_path, RESOURCE_MANAGER->root_folder);
            strcat(file_path, SM_TEXTURE_FOLDER);
            strcat(file_path, name);
            strcat(file_path, SM_TEXTURE_EXT);
            break;
        }

    case SM_RESOURCE_TYPE_MATERIAL:
        {

            file_path = SM_STRING_MALLOC(strlen(RESOURCE_MANAGER->root_folder) + strlen(SM_MATERIAL_FOLDER) + strlen(name) + strlen(SM_MATERIAL_EXT) + 1);
            strcpy(file_path, RESOURCE_MANAGER->root_folder);
            strcat(file_path, SM_MATERIAL_FOLDER);
            strcat(file_path, name);
            strcat(file_path, SM_MATERIAL_EXT);
            break;
        }

    case SM_RESOURCE_TYPE_MESH:
        {

            file_path = SM_STRING_MALLOC(strlen(RESOURCE_MANAGER->root_folder) + strlen(SM_MESH_FOLDER) + strlen(name) + strlen(SM_MESH_EXT) + 1);
            strcpy(file_path, RESOURCE_MANAGER->root_folder);
            strcat(file_path, SM_MESH_FOLDER);
            strcat(file_path, name);
            strcat(file_path, SM_MESH_EXT);
            break;
        }
    case SM_RESOURCE_TYPE_SCENE:
        {

            file_path = SM_STRING_MALLOC(strlen(RESOURCE_MANAGER->root_folder) + strlen(SM_SCENE_FOLDER) + strlen(name) + strlen(SM_SCENE_EXT) + 1);
            strcpy(file_path, RESOURCE_MANAGER->root_folder);
            strcat(file_path, SM_SCENE_FOLDER);
            strcat(file_path, name);
            strcat(file_path, SM_SCENE_EXT);
            break;
        }
    case SM_RESOURCE_TYPE_SHADER:
        {

            file_path = SM_STRING_MALLOC(strlen(RESOURCE_MANAGER->root_folder) + strlen(SM_SHADER_FOLDER) + strlen(name) + strlen(SM_SHADER_EXT) + 1);
            strcpy(file_path, RESOURCE_MANAGER->root_folder);
            strcat(file_path, SM_SHADER_FOLDER);
            strcat(file_path, name);
            strcat(file_path, SM_SHADER_EXT);
            break;
        }
    default:
        {
            SM_LOG_ERROR("invalid type %d", type);
        }
    }

    SM_ASSERT(file_path);

    return file_path;
}

u32 sm_resource_manager_get_uuid(sm_resource_entity_s entity)
{

    if (!SM_RESOURCE_IS_VALID(entity)) return 0;

    switch (entity.type)
    {
    case SM_RESOURCE_TYPE_TEXTURE: return RESOURCE_MANAGER->textures[entity.index].header.uuid; break;
    case SM_RESOURCE_TYPE_MATERIAL: return RESOURCE_MANAGER->materials[entity.index].header.uuid; break;
    case SM_RESOURCE_TYPE_MESH: return RESOURCE_MANAGER->meshes[entity.index].header.uuid; break;
    case SM_RESOURCE_TYPE_SCENE: return RESOURCE_MANAGER->scenes[entity.index].header.uuid; break;
    // case SM_RESOURCE_TYPE_SHADER: return RESOURCE_MANAGER->shaders[entity.index].header.uuid; break;
    default:
        {
            SM_LOG_ERROR("invalid resource type (%d)", entity.type);
            return 0;
        }
    }
}

sm_resource_entity_s sm_resource_manager_get_by_uuid(u32 uuid)
{

    if (!uuid) return SM_RESOURCE_INVALID_ENTITY;

    // sm_hashmap_lock_u32(RESOURCE_MANAGER->u32map);
    sm_resource_s *resource = sm_hashmap_get_u32(RESOURCE_MANAGER->u32map, uuid);
    // sm_hashmap_unlock_u32(RESOURCE_MANAGER->u32map);

    if (resource == NULL)
    {
        SM_LOG_WARN("resource not found by uuid (%u)", uuid);
        return SM_RESOURCE_INVALID_ENTITY;
    }

    return resource->entity;
}

sm_resource_entity_s sm_resource_manager_get_by_name(const SM_STRING file, sm_resource_manager_type_e type)
{

    SM_ASSERT(file);

    SM_STRING key = sm_resource_manager_construct_path(file, type);
    if (!key) return SM_RESOURCE_INVALID_ENTITY;

    sm_resource_s *resource = NULL;
    resource = sm_hashmap_get_str(RESOURCE_MANAGER->strmap, key);

    if (resource == NULL)
    {
        SM_LOG_WARN("[%s] resource not found", key);
        return SM_RESOURCE_INVALID_ENTITY;
    }

    SM_STRING_FREE(key);

    return resource->entity;
}

sm_resource_manager_type_e sm__resource_manager_get_file_type(const SM_STRING filename)
{

    SM_ASSERT(filename);

    if (sm_filesystem_has_ext(filename, "png;smtexture")) return SM_RESOURCE_TYPE_TEXTURE;
    if (sm_filesystem_has_ext(filename, "mp3;ogg")) return SM_RESOURCE_TYPE_AUDIO;
    if (sm_filesystem_has_ext(filename, "smshader")) return SM_RESOURCE_TYPE_SHADER;
    if (sm_filesystem_has_ext(filename, "smscene")) return SM_RESOURCE_TYPE_SCENE;
    if (sm_filesystem_has_ext(filename, "smmaterial")) return SM_RESOURCE_TYPE_MATERIAL;
    if (sm_filesystem_has_ext(filename, "smmesh")) return SM_RESOURCE_TYPE_MESH;

    return SM_RESOURCE_TYPE_NONE;
}

void *sm_resource_manager_get_data(sm_resource_entity_s entity)
{

    switch (entity.type)
    {
    case SM_RESOURCE_TYPE_TEXTURE: return &RESOURCE_MANAGER->textures[entity.index]; break;
    case SM_RESOURCE_TYPE_MATERIAL: return &RESOURCE_MANAGER->materials[entity.index]; break;
    case SM_RESOURCE_TYPE_MESH: return &RESOURCE_MANAGER->meshes[entity.index]; break;
    case SM_RESOURCE_TYPE_SCENE: return &RESOURCE_MANAGER->scenes[entity.index]; break;
    case SM_RESOURCE_TYPE_SHADER: return &RESOURCE_MANAGER->shaders[entity.index]; break;
    default:
        {
            SM_LOG_ERROR("invalid resource type (%d)", entity.type);
            return NULL;
        }
    }
}

void sm__resource_manager_load_header(const SM_STRING file_path, sm_resource_manager_type_e resouce_type)
{

    switch (resouce_type)
    {

    case SM_RESOURCE_TYPE_TEXTURE:
        {

            sm_resource_texture_s texture = {0};
            sm_file_handle_s file;
            if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
            {
                SM_LOG_ERROR("[%s] failed to open file", file_path);
                return;
            }

            if (!sm_resource_header_read(&texture.header, &file))
            {
                SM_LOG_ERROR("[%s] failed to read header", file_path);
                return;
            }

            SM_ASSERT(sm_resource_header_is_valid(&texture.header));

            if (!sm_filesystem_close(&file))
            {
                SM_LOG_ERROR("[%s] failed to close file", file_path);
                return;
            }

            SM_ARRAY_PUSH(RESOURCE_MANAGER->textures, texture);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->textures) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));

            // it is safe to store the texture pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = texture.header.resouce_type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->textures[index];

            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, texture.header.uuid, resource));

            break;
        }
    case SM_RESOURCE_TYPE_MATERIAL:
        {

            sm_resource_material_s material = {0};
            sm_file_handle_s file;
            if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
            {
                SM_LOG_ERROR("[%s] failed to open file", file_path);
                return;
            }

            if (!sm_resource_header_read(&material.header, &file))
            {
                SM_LOG_ERROR("[%s] failed to read header", file_path);
                return;
            }

            SM_ASSERT(sm_resource_header_is_valid(&material.header));

            if (!sm_filesystem_close(&file))
            {
                SM_LOG_ERROR("[%s] failed to close file", file_path);
                return;
            }

            SM_ARRAY_PUSH(RESOURCE_MANAGER->materials, material);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->materials) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the material pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = material.header.resouce_type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->materials[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, material.header.uuid, resource));

            break;
        }
    case SM_RESOURCE_TYPE_MESH:
        {

            sm_resource_mesh_s mesh = {0};
            sm_file_handle_s file;
            if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
            {
                SM_LOG_ERROR("[%s] failed to open file", file_path);
                return;
            }

            if (!sm_resource_header_read(&mesh.header, &file))
            {
                SM_LOG_ERROR("[%s] failed to read header", file_path);
                return;
            }

            SM_ASSERT(sm_resource_header_is_valid(&mesh.header));

            if (!sm_filesystem_close(&file))
            {
                SM_LOG_ERROR("[%s] failed to close file", file_path);
                return;
            }

            SM_ARRAY_PUSH(RESOURCE_MANAGER->meshes, mesh);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->meshes) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the mesh pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = mesh.header.resouce_type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->meshes[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, mesh.header.uuid, resource));

            break;
        }
    case SM_RESOURCE_TYPE_SCENE:
        {

            sm_resource_scene_s scene = {0};

            sm_file_handle_s file;
            if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
            {
                SM_LOG_ERROR("[%s] failed to open file", file_path);
                return;
            }

            if (!sm_resource_header_read(&scene.header, &file))
            {
                SM_LOG_ERROR("[%s] failed to read header", file_path);
                return;
            }

            SM_ASSERT(sm_resource_header_is_valid(&scene.header));

            if (!sm_filesystem_close(&file))
            {
                SM_LOG_ERROR("[%s] failed to close file", file_path);
                return;
            }

            SM_ARRAY_PUSH(RESOURCE_MANAGER->scenes, scene);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->scenes) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the mesh pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = scene.header.resouce_type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->scenes[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, scene.header.uuid, resource));

            break;
        }
    case SM_RESOURCE_TYPE_SHADER:
        {

            sm_resource_shader_s shader = {0};
            if (!sm_resource_shader_read(file_path, &shader))
            {
                SM_LOG_ERROR("[%s] error reading shader", file_path);
                return;
            }
            SM_ARRAY_PUSH(RESOURCE_MANAGER->shaders, shader);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->shaders) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the material pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = resouce_type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->shaders[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            /* SM_ASSERT(sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, material.uuid,
             * resource)); */

            break;
        }

    default: return;
    }
}

void sm__resource_manager_dir_read(const char *folder)
{

    char lookup_folders[5][64];

    // example: assets/material/
    strcpy(lookup_folders[0], folder);
    strcat(lookup_folders[0], SM_TEXTURE_FOLDER);

    strcpy(lookup_folders[1], folder);
    strcat(lookup_folders[1], SM_MATERIAL_FOLDER);

    strcpy(lookup_folders[2], folder);
    strcat(lookup_folders[2], SM_MESH_FOLDER);

    strcpy(lookup_folders[3], folder);
    strcat(lookup_folders[3], SM_SCENE_FOLDER);

    strcpy(lookup_folders[4], folder);
    strcat(lookup_folders[4], SM_SHADER_FOLDER);

    char buf[256];

    for (size_t i = 0; i < sizeof(lookup_folders) / sizeof(lookup_folders[0]); ++i)
    {

        DIR *dir = opendir(lookup_folders[i]);
        if (!dir) continue;

        struct dirent *ent;
        while ((ent = readdir(dir)) != NULL)
        {

            /* check if it is a regular file */
            if (SM_MASK_CHK(ent->d_type, DT_REG))
            {

                /* check if it is a supported file type */
                if (sm_filesystem_has_ext(ent->d_name, expected_ext))
                {

                    /* create the full path */
                    strcpy(buf, lookup_folders[i]);
                    strcat(buf, ent->d_name);

                    if (!sm_filesystem_exists(buf))
                    {
                        SM_LOG_WARN("[%s] resource does not exist", buf);
                        return;
                    }

                    SM_LOG_TRACE("[%s] resource found", buf);

                    sm_resource_manager_type_e ft = sm__resource_manager_get_file_type(buf);
                    if (ft == SM_RESOURCE_TYPE_NONE)
                    {
                        SM_LOG_WARN("[%s] resource not supported", buf);
                        return;
                    }

                    sm__resource_manager_load_header(buf, ft);
                }
            }
        }

        closedir(dir);
    }
}

b8 sm_resource_texture_write(sm_resource_texture_s *texture)
{

    SM_ASSERT(texture);

    SM_STRING file_path = sm_resource_manager_construct_path(texture->header.name, SM_RESOURCE_TYPE_TEXTURE);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_WRITE, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return false;
    }

    SM_LOG_TRACE("Writing texture...");

    if (!sm_resource_header_write(&texture->header, &file))
    {
        SM_LOG_ERROR("[%s] failed to write header", file_path);
        return false;
    }

    // Writing width height
    if (!sm_filesystem_write_bytes(&file, &texture->width, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to write texture widthj");
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, &texture->height, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to write texture height");
        return false;
    }

    // Writing pixel format
    if (!sm_filesystem_write_bytes(&file, &texture->format, sizeof(sm_pixel_format_e)))
    {
        SM_LOG_ERROR("failed to write texture format");
        return false;
    }

    // Writing raw data
    size_t len = sm_gl_get_pixel_data_size(texture->width, texture->height, texture->format);
    if (!sm_filesystem_write_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("failed to write texture len raw data");
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, texture->raw_data, len))
    {
        SM_LOG_ERROR("failed to write texture raw data");
        return false;
    }

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] failed to close file", file_path);
        return false;
    }

    SM_STRING_FREE(file_path);

    return true;
}

void sm_resource_texture_do(sm_resource_texture_s *texture)
{

    SM_ASSERT(texture);

    if (sm_rc_load(&texture->ref_count) > 0)
    {
        if (!sm_resource_texture_is_on_cpu(texture))
        {
            if (!sm_resource_texture_read(texture))
            {
                SM_LOG_ERROR("[%s] error reading texture", texture->header.name);
                return;
            }
        }

        if (!sm_resource_texture_is_on_gpu(texture))
        {
            sm_resource_texture_load_gpu(texture);
        }
    }

    if (sm_resource_texture_is_dirty(texture))
    {
        // do dirty
        sm_resource_texture_set_dirty(texture, false);
    }
}

b8 sm_resource_texture_read(sm_resource_texture_s *texture)
{

    SM_ASSERT(texture);

    SM_LOG_TRACE("[%s] reading texture", texture->header.name);

    SM_STRING file_path = sm_resource_manager_construct_path(texture->header.name, SM_RESOURCE_TYPE_TEXTURE);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return false;
    }

    if (!sm_resource_header_step_over(&file))
    {
        SM_LOG_ERROR("error stepping over the header file");
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &texture->width, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to read texture width");
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &texture->height, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to read texture height");
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &texture->format, sizeof(sm_pixel_format_e)))
    {
        SM_LOG_ERROR("failed to read texture format");
        return false;
    }

    size_t len = 0;
    if (!sm_filesystem_read_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("failed to read texture len raw data");
        return false;
    }

    texture->raw_data = SM_MALLOC(len * sizeof(i8));

    if (!sm_filesystem_read_bytes(&file, texture->raw_data, len))
    {
        SM_LOG_ERROR("failed to read texture raw data");
        return false;
    }

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] failed to close file", file_path);
        return false;
    }

    sm_resource_texture_set_on_cpu(texture, true);

    SM_STRING_FREE(file_path);

    return true;
}

sm_resource_entity_s sm_resource_texture_new_from_mem(sm_resource_texture_s *texture)
{

    SM_ASSERT(texture);
    SM_ASSERT(sm_resource_texture_is_on_cpu(texture));

    SM_STRING file_path = sm_resource_manager_construct_path(texture->header.name, SM_RESOURCE_TYPE_TEXTURE);

    SM_ARRAY_PUSH(RESOURCE_MANAGER->textures, *texture);
    u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->textures) - 1;

    sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));

    resource->entity.type = SM_RESOURCE_TYPE_TEXTURE;
    resource->entity.index = index;
    resource->data = &RESOURCE_MANAGER->textures[index];

    SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
    SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, texture->header.uuid, resource));

    SM_STRING_FREE(file_path);

    return resource->entity;
}

void sm_resource_texture_load_gpu(sm_resource_texture_s *texture)
{

    SM_ASSERT(texture);

    if (!sm_resource_texture_is_on_gpu(texture))
    {

        texture->handle = sm_gl_texture_ctor(texture);
        SM_ASSERT(texture->handle != 0);
        sm_resource_texture_set_on_gpu(texture, true);
        sm_resource_texture_set_dirty(texture, false);
        SM_LOG_TRACE("[%s] texture uploaded successfully to VRAM", texture->header.name);
    }
}

void sm_resource_texture_unload_cpu(sm_resource_texture_s *texture)
{

    if (sm_resource_texture_is_on_cpu(texture))
    {
        SM_FREE(texture->raw_data);
        texture->raw_data = NULL;
        sm_resource_texture_set_on_cpu(texture, false);
    }
}

void sm_resource_texture_unload_gpu(sm_resource_texture_s *texture)
{

    if (sm_resource_texture_is_on_gpu(texture))
    {
        sm_gl_texture_dtor(texture->handle);
        texture->handle = 0;
        sm_resource_texture_set_on_gpu(texture, false);
    }
}

void sm_resource_texture_dtor(sm_resource_texture_s *texture)
{
    sm_rc_dec(&texture->ref_count);
    if (sm_rc_load(&texture->ref_count) > 0) return;

    SM_LOG_INFO("[%s] destructing texture", texture->header.name);
    sm_resource_texture_unload_cpu(texture);
    sm_resource_texture_unload_gpu(texture);
}

sm_resource_entity_s sm_resource_texture_make_reference(sm_resource_entity_s texture)
{
    SM_ASSERT(SM_RESOURCE_IS_VALID(texture));
    SM_ASSERT(texture.type == SM_RESOURCE_TYPE_TEXTURE);

    sm_rc_inc(&RESOURCE_MANAGER->textures[texture.index].ref_count);

    if (sm_rc_load(&RESOURCE_MANAGER->textures[texture.index].ref_count) == 1)
    {
        sm_resource_texture_do(&RESOURCE_MANAGER->textures[texture.index]);
    }

    return texture;
}

void sm_resource_texture_bind(sm_resource_texture_s *texture, u32 tex_index)
{

    if (!sm_resource_texture_is_on_gpu(texture)) SM_LOG_WARN("texture [%s] has no gpu data", texture->header.name);

    sm_gl_texture_activate(tex_index);
    sm_gl_texture_bind(texture->handle);
}

void sm_resource_texture_unbind(void)
{

    sm_gl_texture_unbind( );
}

void sm_resource_texture_set_dirty(sm_resource_texture_s *texture, b8 dirty)
{
    if (dirty)
    {
        texture->flags |= SM_TEXTURE_FLAG_DIRTY;
    }
    else
    {
        texture->flags &= ~(u32)SM_TEXTURE_FLAG_DIRTY;
    }
}

b8 sm_resource_texture_is_dirty(const sm_resource_texture_s *texture)
{
    return texture->flags & SM_TEXTURE_FLAG_DIRTY;
}

void sm_resource_texture_set_on_cpu(sm_resource_texture_s *texture, b8 on_cpu)
{
    if (on_cpu) texture->flags |= SM_TEXTURE_FLAG_ON_CPU;
    else texture->flags &= ~(u32)SM_TEXTURE_FLAG_ON_CPU;
}

b8 sm_resource_texture_is_on_cpu(const sm_resource_texture_s *texture)
{
    return texture->flags & SM_TEXTURE_FLAG_ON_CPU;
}

void sm_resource_texture_set_on_gpu(sm_resource_texture_s *texture, b8 on_gpu)
{
    if (on_gpu) texture->flags |= SM_TEXTURE_FLAG_ON_GPU;
    else texture->flags &= ~(u32)SM_TEXTURE_FLAG_ON_GPU;
}

b8 sm_resource_texture_is_on_gpu(const sm_resource_texture_s *texture)
{
    return texture->flags & SM_TEXTURE_FLAG_ON_GPU;
}

// Material
b8 sm_resource_material_write(sm_resource_material_s *material)
{

    SM_ASSERT(material);

    SM_LOG_TRACE("Writing material...");

    SM_STRING file_path = sm_resource_manager_construct_path(material->header.name, SM_RESOURCE_TYPE_MATERIAL);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_WRITE, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open/create file", file_path);
        return false;
    }

    if (!sm_resource_header_write(&material->header, &file))
    {
        SM_LOG_ERROR("[%s] failed to write header", file_path);
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, &material->diffuse_map, sizeof(u32)))
    {
        SM_LOG_ERROR("[s] failed to write the diffuse_map uuid");
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, &material->diffuse_color, sizeof(sm_vec4)))
    {
        SM_LOG_ERROR("[s] failed to write the diffuse_color");
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, &material->flags, sizeof(enum sm_material_component_flags_e)))
    {
        SM_LOG_ERROR("[s] failed to write the flags");
        return false;
    }

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] failed to close file", file_path);
        return false;
    }
    SM_STRING_FREE(file_path);

    return true;
}

void sm_resource_material_do(sm_resource_material_s *material)
{

    SM_ASSERT(material);

    if (sm_rc_load(&material->ref_count) > 0)
    {
        if (!sm_resource_material_is_on_cpu(material))
        {
            if (!sm_resource_material_read(material))
            {
                SM_LOG_ERROR("[%s] error reading material", material->header.name);
                return;
            }
            sm_resource_material_set_on_cpu(material, true);
        }
    }
}

b8 sm_resource_material_read(sm_resource_material_s *material)
{

    SM_ASSERT(material);

    SM_LOG_TRACE("[%s] reading material", material->header.name);

    SM_STRING file_path = sm_resource_manager_construct_path(material->header.name, SM_RESOURCE_TYPE_MATERIAL);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return false;
    }

    if (!sm_resource_header_step_over(&file))
    {
        SM_LOG_ERROR("[%s] error stepping over the header file", file_path);
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &material->diffuse_map, sizeof(u32)))
    {
        SM_LOG_ERROR("[%s] failed to read the diffuse_map uuid", file_path);
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &material->diffuse_color, sizeof(sm_vec4)))
    {
        SM_LOG_ERROR("[%s] failed to read the diffuse_color", file_path);
        return false;
    }

    if (material->diffuse_map)
    {
        sm_resource_entity_s diffuse_map_ett = sm_resource_manager_get_by_uuid(material->diffuse_map);
        sm_resource_texture_make_reference(diffuse_map_ett);
    }

    if (!sm_filesystem_read_bytes(&file, &material->flags, sizeof(enum sm_material_component_flags_e)))
    {
        SM_LOG_ERROR("[%s] failed to read the flags", file_path);
        return false;
    }

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] error closing file", file_path);
        return false;
    }

    SM_STRING_FREE(file_path);

    return true;
}

void sm_resource_material_dtor(sm_resource_material_s *material)
{

    sm_rc_dec(&material->ref_count);
    if (sm_rc_load(&material->ref_count) > 0) return;

    SM_LOG_INFO("[%s] destructing material", material->header.name);

    if (material->diffuse_map)
    {
        sm_resource_entity_s texture_ett = sm_resource_manager_get_by_uuid(material->diffuse_map);
        sm_resource_texture_s *texture = sm_resource_manager_get_data(texture_ett);
        sm_resource_texture_dtor(texture);
    }
}

sm_resource_entity_s sm_resource_material_make_reference(sm_resource_entity_s material)
{
    SM_ASSERT(SM_RESOURCE_IS_VALID(material));
    SM_ASSERT(material.type == SM_RESOURCE_TYPE_MATERIAL);

    sm_rc_inc(&RESOURCE_MANAGER->materials[material.index].ref_count);

    if (sm_rc_load(&RESOURCE_MANAGER->materials[material.index].ref_count) == 1)
    {
        sm_resource_material_do(&RESOURCE_MANAGER->materials[material.index]);
    }

    return material;
}

void sm_resource_material_set_dirty(sm_resource_material_s *material, b8 dirty)
{
    if (dirty) material->flags |= SM_MATERIAL_FLAG_DIRTY;
    else material->flags &= ~(u32)SM_MATERIAL_FLAG_DIRTY;
}

b8 sm_resource_material_is_dirty(const sm_resource_material_s *material)
{
    return material->flags & SM_MATERIAL_FLAG_DIRTY;
}

void sm_resource_material_set_on_cpu(sm_resource_material_s *material, b8 on_cpu)
{
    if (on_cpu) material->flags |= SM_MATERIAL_FLAG_ON_CPU;
    else material->flags &= ~(u32)SM_MATERIAL_FLAG_ON_CPU;
}

b8 sm_resource_material_is_on_cpu(const sm_resource_material_s *material)
{
    return material->flags & SM_MATERIAL_FLAG_ON_CPU;
}

void sm_resource_material_set_twosided(sm_resource_material_s *material, b8 twosided)
{
    if (twosided) material->flags |= SM_MATERIAL_FLAG_TWOSIDED;
    else material->flags &= ~(u32)SM_MATERIAL_FLAG_TWOSIDED;
}

b8 sm_resource_material_is_twosided(const sm_resource_material_s *material)
{
    return material->flags & SM_MATERIAL_FLAG_TWOSIDED;
}

// Mesh
b8 sm_resource_mesh_write(sm_resource_mesh_s *mesh)
{

    SM_ASSERT(mesh);

    SM_STRING file_path = sm_resource_manager_construct_path(mesh->header.name, SM_RESOURCE_TYPE_MESH);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_WRITE, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return false;
    }
    SM_LOG_TRACE("Writing mesh...");

    if (!sm_resource_header_write(&mesh->header, &file))
    {
        SM_LOG_ERROR("[%s] failed to write header", file_path);
        return false;
    }

    size_t len = 0, size = 0;

    /* Write positions */
    len = SM_ARRAY_LEN(mesh->positions);
    size = sizeof(*mesh->positions);
    SM_LOG_TRACE("positions: len: %lu, size: %lu", len, size);
    if (!sm_filesystem_write_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the length of positions");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the size of positions");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, mesh->positions, len * size))
    {
        SM_LOG_ERROR("[s] failed to write positions");
        return false;
    }

    /* Write UVs */
    len = SM_ARRAY_LEN(mesh->uvs);
    size = sizeof(*mesh->uvs);
    SM_LOG_TRACE("uvs: len: %lu, size: %lu", len, size);
    if (!sm_filesystem_write_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the length of uvs");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the size of uvs");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, mesh->uvs, len * size))
    {
        SM_LOG_ERROR("[s] failed to write uvs");
        return false;
    }

    /* Write colors */
    len = SM_ARRAY_LEN(mesh->colors);
    size = sizeof(*mesh->colors);
    SM_LOG_TRACE("colors: len: %lu, size: %lu", len, size);
    if (!sm_filesystem_write_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the length of colors");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the size of colors");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, mesh->colors, len * size))
    {
        SM_LOG_ERROR("[s] failed to write colors");
        return false;
    }

    /* Write normals */
    len = SM_ARRAY_LEN(mesh->normals);
    size = sizeof(*mesh->normals);
    SM_LOG_TRACE("normals: len: %lu, size: %lu", len, size);
    if (!sm_filesystem_write_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the length of normals");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the size of normals");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, mesh->normals, len * size))
    {
        SM_LOG_ERROR("[s] failed to write normals");
        return false;
    }

    /* Write indices */
    len = SM_ARRAY_LEN(mesh->indices);
    size = sizeof(*mesh->indices);
    SM_LOG_TRACE("indices: len: %lu, size: %lu", len, size);
    if (!sm_filesystem_write_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the length of indices");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[s] failed to write the size of indices");
        return false;
    }
    if (!sm_filesystem_write_bytes(&file, mesh->indices, len * size))
    {
        SM_LOG_ERROR("[s] failed to write indices");
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, &mesh->flags, sizeof(mesh->flags)))
    {
        SM_LOG_ERROR("[s] failed to write flags");
        return false;
    }

    if (!sm_filesystem_write_bytes(&file, &mesh->material, sizeof(u32)))
    {
        SM_LOG_ERROR("[s] failed to write material uuid");
        return false;
    }

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] failed to close file", file_path);
        return false;
    }
    SM_STRING_FREE(file_path);

    return true;
}

b8 sm_resource_mesh_read(sm_resource_mesh_s *mesh)
{

    SM_ASSERT(mesh);

    SM_LOG_TRACE("[%s] reading meseh", mesh->header.name);

    SM_STRING file_path = sm_resource_manager_construct_path(mesh->header.name, SM_RESOURCE_TYPE_MESH);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return false;
    }

    if (!sm_resource_header_step_over(&file))
    {
        SM_LOG_ERROR("[%s] error stepping over the header file", file_path);
        return false;
    }

    size_t len = 0, size = 0;

    /* Read positions */
    if (!sm_filesystem_read_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the length of positions", file_path);
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the size of positions", file_path);
        return false;
    }

    SM_ARRAY_SET_LEN(mesh->positions, len);
    if (!sm_filesystem_read_bytes(&file, mesh->positions, len * size))
    {
        SM_LOG_ERROR("[%s] failed to read vertices", file_path);
        return false;
    }

    /* Read UVs */
    if (!sm_filesystem_read_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the length of uvs", file_path);
        return false;
    }
    if (!sm_filesystem_read_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the size of uvs", file_path);
        return false;
    }
    SM_ARRAY_SET_LEN(mesh->uvs, len);
    if (!sm_filesystem_read_bytes(&file, mesh->uvs, len * size))
    {
        SM_LOG_ERROR("[%s] failed to read uvs", file_path);
        return false;
    }

    /* Read colors */
    if (!sm_filesystem_read_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the length of colors", file_path);
        return false;
    }
    if (!sm_filesystem_read_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the size of colors", file_path);
        return false;
    }
    SM_ALIGNED_ARRAY_NEW(mesh->colors, 16, len);
    if (!sm_filesystem_read_bytes(&file, mesh->colors, len * size))
    {
        SM_LOG_ERROR("[%s] failed to read colors", file_path);
        return false;
    }

    /* Read normals */
    if (!sm_filesystem_read_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the length of normals", file_path);
        return false;
    }
    if (!sm_filesystem_read_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the size of normals", file_path);
        return false;
    }
    SM_ARRAY_SET_LEN(mesh->normals, len);
    if (!sm_filesystem_read_bytes(&file, mesh->normals, len * size))
    {
        SM_LOG_ERROR("[%s] failed to read normals", file_path);
        return false;
    }

    /* Read indices */
    if (!sm_filesystem_read_bytes(&file, &len, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the length of indices", file_path);
        return false;
    }
    if (!sm_filesystem_read_bytes(&file, &size, sizeof(size_t)))
    {
        SM_LOG_ERROR("[%s] failed to read the size of indices", file_path);
        return false;
    }
    SM_ARRAY_SET_LEN(mesh->indices, len);
    if (!sm_filesystem_read_bytes(&file, mesh->indices, len * size))
    {
        SM_LOG_ERROR("[%s] failed to read indices", file_path);
        return false;
    }

    if (!sm_filesystem_read_bytes(&file, &mesh->flags, sizeof(mesh->flags)))
    {
        SM_LOG_ERROR("[%s] failed to read flags", file_path);
        return false;
    }

    u32 material_uuid = 0;
    if (!sm_filesystem_read_bytes(&file, &material_uuid, sizeof(u32)))
    {
        SM_LOG_ERROR("[%s] failed to read uuid", file_path);
        return false;
    }

    sm_resource_entity_s material_entt = sm_resource_manager_get_by_uuid(material_uuid);
    sm_resource_material_make_reference(material_entt);

    mesh->material = material_uuid;

    sm_resource_mesh_set_dirty(mesh, true);
    sm_resource_mesh_set_on_cpu(mesh, true);
    sm_resource_mesh_set_on_gpu(mesh, false);
    sm_resource_mesh_set_draw_aabb(mesh, false);

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] failed to close file", file_path);
        return false;
    }
    SM_STRING_FREE(file_path);

    return true;
}

void sm_resource_mesh_do(sm_resource_mesh_s *mesh)
{

    if (sm_rc_load(&mesh->ref_count) > 0)
    {
        if (!sm_resource_mesh_is_on_cpu(mesh))
        {
            if (!sm_resource_mesh_read(mesh))
            {
                SM_LOG_ERROR("[%s] error reading mesh", mesh->header.name);
                return;
            }
        }

        if (!sm_resource_mesh_is_on_gpu(mesh))
        {
            sm_resource_mesh_load_gpu(mesh);
        }
    }

    if (sm_resource_mesh_is_dirty(mesh))
    {
        sm_resource_mesh_calculate_aabb(mesh);
        sm_resource_mesh_set_dirty(mesh, false);
    }
}

void sm_resource_mesh_load_gpu(sm_resource_mesh_s *mesh)
{

    SM_ASSERT(mesh);

    if (!sm_resource_mesh_is_on_gpu(mesh))
    {

        SM_ASSERT(mesh->vao == 0);

        mesh->vao = sm_gl_vao_new( );
        sm_gl_vao_bind(mesh->vao);

        sm_buffer_usage_e usage = SM_STATIC_DRAW;
        if (sm_resource_mesh_is_skinned(mesh))
        {
            usage = SM_STREAM_DRAW;
            // mesh->skin_data.vbos[0] = sm_gl_vbo_new(mesh->skin_data.influences, (u32)(SM_ARRAY_LEN(mesh->skin_data.influences) * sizeof(sm_ivec4)),
            // usage); sm_gl_vertex_attribute_pointer((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_POSITION], 3, SM_F32, false, 0, 0);
            // sm_gl_vertex_attribute_enable((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_POSITION]);
        }

        // generate positions buffer
        mesh->vbos[0] = sm_gl_vbo_new(mesh->positions, (u32)(SM_ARRAY_LEN(mesh->positions) * sizeof(sm_vec3)), usage);
        sm_gl_vertex_attribute_pointer((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_POSITION], 3, SM_F32, false, 0, 0);
        sm_gl_vertex_attribute_enable((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_POSITION]);

        // generate uvs buffer
        mesh->vbos[1] = sm_gl_vbo_new(mesh->uvs, (u32)SM_ARRAY_LEN(mesh->uvs) * sizeof(sm_vec2), usage);
        sm_gl_vertex_attribute_enable((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_UV]);
        sm_gl_vertex_attribute_pointer((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_UV], 2, SM_F32, false, 0, NULL);

        // generate colors buffer
        mesh->vbos[2] = sm_gl_vbo_new(mesh->colors, (u32)(SM_ARRAY_LEN(mesh->colors) * sizeof(sm_vec4)), usage);
        sm_gl_vertex_attribute_enable((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_COLOR]);
        sm_gl_vertex_attribute_pointer((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_COLOR], 4, SM_F32, false, 0, 0);

        // generate normals buffer
        mesh->vbos[3] = sm_gl_vbo_new(mesh->normals, (u32)(SM_ARRAY_LEN(mesh->normals) * sizeof(sm_vec3)), usage);
        sm_gl_vertex_attribute_enable((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_NORMAL]);
        sm_gl_vertex_attribute_pointer((u32)renderer_defaults.shader_locs[SM_SHADER_LOC_VERTEX_NORMAL], 3, SM_F32, false, 0, 0);

        // generate indices buffer
        mesh->ebo = sm_gl_ebo_new(mesh->indices, (u32)(sizeof(u32) * SM_ARRAY_LEN(mesh->indices)), usage);

        sm_gl_vao_unbind( );

        // just in case
        sm_gl_vbo_unbind( );
        sm_gl_ebo_unbind( );

        if (mesh->vao > 0)
        {
            sm_resource_mesh_set_on_gpu(mesh, true);
            SM_LOG_TRACE("[%s] mesh uploaded successfully to VRAM", mesh->header.name);
        }
    }
}

void sm_resource_mesh_unload_cpu(sm_resource_mesh_s *mesh)
{

    SM_ASSERT(mesh);

    if (sm_resource_mesh_is_on_cpu(mesh))
    {

        SM_ARRAY_DTOR(mesh->positions);
        SM_ARRAY_DTOR(mesh->uvs);
        SM_ALIGNED_ARRAY_DTOR(mesh->colors);
        SM_ARRAY_DTOR(mesh->normals);
        SM_ARRAY_DTOR(mesh->indices);

        if (sm_resource_mesh_is_skinned(mesh))
        {
            SM_ARRAY_DTOR(mesh->skin_data.influences);
            SM_ARRAY_DTOR(mesh->skin_data.weights);
            SM_ARRAY_DTOR(mesh->skin_data.pose_palette);
        }

        sm_resource_mesh_set_on_cpu(mesh, false);
    }
}

void sm_resource_mesh_unload_gpu(sm_resource_mesh_s *mesh)
{

    if (sm_resource_mesh_is_on_gpu(mesh))
    {

        for (u8 i = 0; i < 4; ++i)
        {
            sm_gl_vbo_delete(mesh->vbos[i]);
        }
        sm_gl_vbo_delete(mesh->ebo);

        if (sm_resource_mesh_is_skinned(mesh))
        {
            sm_gl_vbo_delete(mesh->skin_data.vbos[0]);
            sm_gl_vbo_delete(mesh->skin_data.vbos[1]);
        }

        sm_gl_vao_delete(mesh->vao);

        sm_resource_mesh_set_on_gpu(mesh, false);
    }
}

void sm_resource_mesh_dtor(sm_resource_mesh_s *mesh)
{
    sm_rc_dec(&mesh->ref_count);
    if (sm_rc_load(&mesh->ref_count) > 0) return;

    SM_LOG_INFO("[%s] destructing mesh", mesh->header.name);

    sm_resource_mesh_unload_cpu(mesh);
    sm_resource_mesh_unload_gpu(mesh);

    if (mesh->material)
    {
        sm_resource_entity_s material_entt = sm_resource_manager_get_by_uuid(mesh->material);
        sm_resource_material_s *material = sm_resource_manager_get_data(material_entt);
        sm_resource_material_dtor(material);
    }
}

sm_resource_entity_s sm_resource_mesh_make_reference(sm_resource_entity_s mesh)
{

    SM_ASSERT(SM_RESOURCE_IS_VALID(mesh));
    SM_ASSERT(mesh.type == SM_RESOURCE_TYPE_MESH);

    sm_rc_inc(&RESOURCE_MANAGER->meshes[mesh.index].ref_count);

    if (sm_rc_load(&RESOURCE_MANAGER->meshes[mesh.index].ref_count) == 1)
    {
        sm_resource_mesh_do(&RESOURCE_MANAGER->meshes[mesh.index]);
    }

    return mesh;
}

void sm_resource_mesh_set_renderable(sm_resource_mesh_s *mesh, bool renderable)
{
    if (renderable) SM_MASK_SET(mesh->flags, SM_MESH_FLAG_RENDERABLE);
    else SM_MASK_CLR(mesh->flags, (u32)SM_MESH_FLAG_RENDERABLE);
}

b8 sm_resource_mesh_is_renderable(const sm_resource_mesh_s *mesh)
{
    return SM_MASK_CHK(mesh->flags, SM_MESH_FLAG_RENDERABLE);
}

void sm_resource_mesh_set_dirty(sm_resource_mesh_s *mesh, bool dirty)
{

    if (dirty) SM_MASK_SET(mesh->flags, SM_MESH_FLAG_DIRTY);
    else SM_MASK_CLR(mesh->flags, (u32)SM_MESH_FLAG_DIRTY);
}

b8 sm_resource_mesh_is_dirty(const sm_resource_mesh_s *mesh)
{
    return SM_MASK_CHK(mesh->flags, SM_MESH_FLAG_DIRTY);
}

void sm_resource_mesh_set_on_gpu(sm_resource_mesh_s *mesh, b8 on_gpu)
{
    if (on_gpu) SM_MASK_SET(mesh->flags, SM_MESH_FLAG_ON_GPU);
    else SM_MASK_CLR(mesh->flags, (u32)SM_MESH_FLAG_ON_GPU);
}

b8 sm_resource_mesh_is_on_gpu(const sm_resource_mesh_s *mesh)
{
    return SM_MASK_CHK(mesh->flags, SM_MESH_FLAG_ON_GPU);
}

void sm_resource_mesh_set_on_cpu(sm_resource_mesh_s *mesh, b8 on_cpu)
{
    if (on_cpu) SM_MASK_SET(mesh->flags, SM_MESH_FLAG_ON_CPU);
    else SM_MASK_CLR(mesh->flags, (u32)SM_MESH_FLAG_ON_CPU);
}

b8 sm_resource_mesh_is_on_cpu(const sm_resource_mesh_s *mesh)
{
    return SM_MASK_CHK(mesh->flags, SM_MESH_FLAG_ON_CPU);
}

void sm_resource_mesh_set_skinned(sm_resource_mesh_s *mesh, b8 skinned)
{
    if (skinned) SM_MASK_SET(mesh->flags, SM_MESH_FLAG_SKINNED);
    else SM_MASK_CLR(mesh->flags, (u32)SM_MESH_FLAG_SKINNED);
}

b8 sm_resource_mesh_is_skinned(const sm_resource_mesh_s *mesh)
{
    return SM_MASK_CHK(mesh->flags, SM_MESH_FLAG_SKINNED);
}

void sm_resource_mesh_set_draw_aabb(sm_resource_mesh_s *mesh, b8 draw_aabb)
{
    if (draw_aabb) SM_MASK_SET(mesh->flags, SM_MESH_FLAG_DRAW_AABB);
    else SM_MASK_CLR(mesh->flags, (u32)SM_MESH_FLAG_DRAW_AABB);
}

b8 sm_resource_mesh_is_draw_aabb(const sm_resource_mesh_s *mesh)
{
    return SM_MASK_CHK(mesh->flags, SM_MESH_FLAG_DRAW_AABB);
}

struct sm__save_data
{
    sm_file_handle_s *file;
    SM_STRING file_path;
};

void sm__scene_node_write_cb(sm_resource_scene_s *scene, u32 node_index, void *user_data)
{

    struct sm__save_data *data = (struct sm__save_data *)user_data;
    sm_entity_s entity = sm_resource_scene_get_entity(scene, node_index);

    const SM_STRING name = sm_resource_scene_get_name(scene, node_index);
    SM_LOG_TRACE("Writing node %s", name);

    /* Writing index */
    if (!sm_filesystem_write_bytes(data->file, &node_index, sizeof(node_index)))
    {
        SM_LOG_ERROR("failed to write index");
        return;
    }

    /* Writing parent */
    u32 parent = sm_resource_scene_get_parent(scene, node_index);
    if (!sm_filesystem_write_bytes(data->file, &parent, sizeof(u32)))
    {
        SM_LOG_ERROR("failed to write parent");
        return;
    }

    /* Writing the name length */
    size_t name_len = strlen(name);
    if (!sm_filesystem_write_bytes(data->file, &name_len, sizeof(size_t)))
    {
        SM_LOG_ERROR("failed to write name len");
        return;
    }

    /* Writing the name */
    if (!sm_filesystem_write_bytes(data->file, name, name_len))
    {
        SM_LOG_ERROR("failed to write name");
        return;
    }

    sm_component_t archetype = 0u;
    if (sm_ecs_entity_is_valid(scene->ecs, entity))
    {
        archetype = sm_ecs_entity_get_archetype(scene->ecs, entity);
    }
    else
    {
        SM_LOG_WARN("no entity for '%s' node", name);
    }

    /* Writing archtype */
    if (!sm_filesystem_write_bytes(data->file, &archetype, sizeof(sm_component_t)))
    {
        SM_LOG_ERROR("failed to write archetype index");
        return;
    }

    sm_chunk_s *chunk = sm_hashmap_get_u64(scene->ecs->map_components, archetype);

    if (chunk)
    {

        SM_ASSERT(chunk->pool);
        sm_handle_t handle = sm_ecs_entity_get_handle(scene->ecs, entity);
        u32 handle_idx = sm_handle_index(handle);
        SM_ASSERT(handle_idx < chunk->length);

        for (u32 v = 0; v < SM_ARRAY_LEN(chunk->view); ++v)
        {

            sm_component_view_s *view = &chunk->view[v];
            void *chdata = (u8 *)chunk->data + (handle_idx * chunk->size) + view->offset;
            if (view->write)
            {
                SM_ASSERT(view->write(data->file, chdata, NULL));
            }
        }
    }
    else
    {
        SM_LOG_WARN("no chunk for archetype %lu", archetype);
    }
}

b8 sm_resource_scene_write(sm_resource_scene_s *scene)
{

    SM_STRING file_path = sm_resource_manager_construct_path(scene->header.name, SM_RESOURCE_TYPE_SCENE);

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_WRITE, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open/create file", file_path);
        return false;
    }

    if (!sm_resource_header_write(&scene->header, &file))
    {
        SM_LOG_ERROR("[%s] failed to write header", file_path);
        return false;
    }

    size_t nodes_len = SM_ARRAY_LEN(scene->nodes);
    if (!sm_filesystem_write_bytes(&file, &nodes_len, sizeof(size_t)))
    {
        SM_LOG_ERROR("failed to write node length");
        return false;
    }

    struct sm__save_data data = {.file = &file, .file_path = file_path};
    sm_resource_scene_for_each(scene, 0, sm__scene_node_write_cb, &data);

    if (!sm_filesystem_close(&file))
    {
        SM_LOG_ERROR("[%s] failed to close file", file_path);
        return true;
    }

    SM_STRING_FREE(file_path);

    return true;
}

b8 sm_resource_scene_read(sm_resource_scene_s *scene)
{

    SM_STRING file_path = sm_resource_manager_construct_path(scene->header.name, SM_RESOURCE_TYPE_SCENE);

    SM_LOG_TRACE("[%s] reading scene", scene->header.name);

    if (scene->ecs == NULL)
    {
        sm_ecs_world_s *ecs = sm_ecs_world_new( );
        if (!sm_ecs_world_ctor(ecs, SM_ALL_COMP))
        {
            SM_LOG_ERROR("[%s] Failed to create ECS", file_path);
            return false;
        }

        scene->ecs = ecs;
    }

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return NULL;
    }

    if (!sm_resource_header_step_over(&file))
    {
        SM_LOG_ERROR("error stepping over the header file");
        return false;
    }

    size_t nodes_len = 0;
    if (!sm_filesystem_read_bytes(&file, &nodes_len, sizeof(size_t)))
    {
        SM_LOG_ERROR("failed to read nodes len");
        return NULL;
    }

    SM_ARRAY_SET_LEN(scene->nodes, nodes_len);

    for (size_t i = 0; i < nodes_len; ++i)
    {

        scene->nodes[i] = sm_node_new_invalid( );

        u32 index = 0;
        if (!sm_filesystem_read_bytes(&file, &index, sizeof(u32)))
        {
            SM_LOG_ERROR("failed to read index");
            return NULL;
        }

        u32 parent = 0;
        if (!sm_filesystem_read_bytes(&file, &parent, sizeof(u32)))
        {
            SM_LOG_ERROR("failed to read parent");
            return NULL;
        }

        if (parent != SM_SCENE_NO_PARENT_NODE) sm_resource_scene_add_child(scene, parent, index);
        else sm_resource_scene_set_parent(scene, index, SM_SCENE_NO_PARENT_NODE);

        size_t name_len = 0;
        if (!sm_filesystem_read_bytes(&file, &name_len, sizeof(size_t)))
        {
            SM_LOG_ERROR("failed to read name len");
            return NULL;
        }

        char buf_node[name_len + 1];
        if (!sm_filesystem_read_bytes(&file, buf_node, name_len))
        {
            SM_LOG_ERROR(" failed to read the string");
            return NULL;
        }
        buf_node[name_len] = '\0';
        sm_resource_scene_set_name(scene, index, buf_node);

        sm_component_t archetype = 0u;
        if (!sm_filesystem_read_bytes(&file, &archetype, sizeof(sm_component_t)))
        {
            SM_LOG_ERROR("failed to read archetype index");
            return NULL;
        }

        sm_entity_s entity = {0};
        if (archetype != 0u)
        {
            entity = sm_ecs_entity_new(scene->ecs, archetype);
            sm_handle_t handle = sm_ecs_entity_get_handle(scene->ecs, entity);
            if (handle == SM_INVALID_HANDLE)
            {
                SM_LOG_ERROR("failed to create entity");
                return NULL;
            }

            sm_chunk_s *chunk = sm_hashmap_get_u64(scene->ecs->map_components, archetype);
            if (chunk == NULL)
            {
                SM_LOG_WARN("no chunk for archetype %lu", archetype);
                return NULL;
            }

            u32 handle_idx = sm_handle_index(handle);

            for (u32 k = 0; k < SM_ARRAY_LEN(chunk->view); ++k)
            {
                sm_component_view_s *view = &chunk->view[k];
                void *chdata = (u8 *)chunk->data + (handle_idx * chunk->size) + view->offset;

                if (view->read)
                {
                    SM_ASSERT(view->read(&file, chdata, NULL));
                }

                if (SM_MASK_CHK_EQ(view->id, SM_RESOURCE_COMP))
                {

                    sm_res_s *res = chdata;
                    sm_resource_entity_s entt = sm_resource_manager_get_by_uuid(res->uuid);
                    switch (entt.type)
                    {
                    case SM_RESOURCE_TYPE_MESH:
                        {

                            sm_resource_mesh_make_reference(entt);

                            break;
                        }
                    default:
                        {
                            SM_LOG_ERROR("not handled resource (%d)", entt.type);
                            SM_UNREACHABLE( );
                        }
                    }

                    continue;
                }
            }
            sm_resource_scene_set_entity(scene, index, entity);
        }
    }

    SM_STRING_FREE(file_path);

    sm_filesystem_close(&file);

    return scene;
}

void sm_resource_scene_dtor(sm_resource_scene_s *scene)
{
    sm_rc_dec(&scene->ref_count);
    if (sm_rc_load(&scene->ref_count) > 0) return;

    SM_LOG_INFO("[%s] destructing scene", scene->header.name);

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {

        sm_entity_s ett = scene->nodes[i].entity;
        if (sm_ecs_entity_is_valid(scene->ecs, ett) && sm_ecs_entity_has_component(scene->ecs, ett, SM_RESOURCE_COMP))
        {

            const sm_res_s *res = sm_ecs_component_get(scene->ecs, ett, SM_RESOURCE_COMP);
            sm_resource_entity_s resource_ett = sm_resource_manager_get_by_uuid(res->uuid);
            SM_ASSERT(resource_ett.type == SM_RESOURCE_TYPE_MESH);

            sm_resource_mesh_s *mesh = sm_resource_manager_get_data(resource_ett);
            sm_resource_mesh_dtor(mesh);
        }

        if (scene->nodes[i].name) SM_STRING_FREE(scene->nodes[i].name);
        SM_ARRAY_DTOR(scene->nodes[i].children);
    }
    sm_ecs_world_dtor(scene->ecs);
    SM_ARRAY_DTOR(scene->nodes);
}

sm_resource_entity_s sm_resource_scene_make_reference(sm_resource_entity_s scene)
{
    SM_ASSERT(SM_RESOURCE_IS_VALID(scene));
    SM_ASSERT(scene.type == SM_RESOURCE_TYPE_SCENE);

    sm_rc_inc(&RESOURCE_MANAGER->scenes[scene.index].ref_count);

    if (sm_rc_load(&RESOURCE_MANAGER->scenes[scene.index].ref_count) == 1)
    {
        sm_resource_scene_s *sc = &RESOURCE_MANAGER->scenes[scene.index];
        if (!sm_resource_scene_is_on_cpu(sc))
        {
            if (!sm_resource_scene_read(sc))
            {
                SM_LOG_ERROR("[%s] error reading scene", sc->header.name);
                return scene;
            }

            sm_resource_scene_set_on_cpu(sc, true);
        }
    }

    return scene;
}

void sm_resource_scene_do(sm_resource_scene_s *scene, f32 dt)
{

    if (sm_rc_load(&scene->ref_count) == 0)
    {
        return;
    }

    if (!sm_resource_scene_is_on_cpu(scene))
    {
        if (!sm_resource_scene_read(scene))
        {
            SM_LOG_ERROR("[%s] error reading scene", scene->header.name);
            return;
        }

        sm_resource_scene_set_on_cpu(scene, true);
    }

#if 0

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {

        sm_entity_s ett = scene->nodes[i].entity;

        if (!sm_ecs_entity_is_valid(scene->ecs, ett) || !sm_ecs_entity_has_component(scene->ecs, ett, SM_TRANSFORM_COMP | SM_WORLD_COMP))
        {
            continue;
        }

        // local space
        sm_transform_s accumulator = *(sm_transform_s *)sm_ecs_component_get_mut(scene->ecs, ett, SM_TRANSFORM_COMP);

        u32 parent = scene->nodes[i].parent;
        while (parent != SM_SCENE_NO_PARENT_NODE)
        {

            sm_entity_s parent_ett = scene->nodes[parent].entity;

            if (sm_ecs_entity_is_valid(scene->ecs, parent_ett) && sm_ecs_entity_has_component(scene->ecs, parent_ett, SM_TRANSFORM_COMP))
            {
                const sm_transform_s *parent_transform = sm_ecs_component_get(scene->ecs, parent_ett, SM_TRANSFORM_COMP);
                accumulator = sm_transform_component_combine(parent_transform, &accumulator);
            }

            parent = scene->nodes[parent].parent;
        }

        sm_world_s *current_world = sm_ecs_component_get_mut(scene->ecs, ett, SM_WORLD_COMP);

        sm_mat4 world_matrix;
        sm_transform_component_to_mat4(&accumulator, world_matrix.data);
        sm_world_component_store_matrix(current_world, &world_matrix);
    }

#else

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {

        sm_entity_s ett = scene->nodes[i].entity;
        if (!sm_ecs_entity_is_valid(scene->ecs, ett))
        {
            continue;
        }

        u32 parent = scene->nodes[i].parent;
        if (sm_ecs_entity_has_component(scene->ecs, ett, SM_TRANSFORM_COMP | SM_WORLD_COMP))
        {

            // local space
            sm_transform_s *current_transform = sm_ecs_component_get_mut(scene->ecs, ett, SM_TRANSFORM_COMP);

            b8 is_dirty = sm_transform_component_is_dirty(current_transform);

            while (parent != SM_SCENE_NO_PARENT_NODE && !is_dirty)
            {

                sm_entity_s parent_ett = scene->nodes[parent].entity;

                if (sm_ecs_entity_is_valid(scene->ecs, parent_ett) && sm_ecs_entity_has_component(scene->ecs, parent_ett, SM_TRANSFORM_COMP))
                {
                    const sm_transform_s *parent_transform = sm_ecs_component_get(scene->ecs, parent_ett, SM_TRANSFORM_COMP);
                    is_dirty = sm_transform_component_is_dirty(parent_transform);
                }
                parent = scene->nodes[parent].parent;
            }

            if (is_dirty)
            {

                parent = scene->nodes[i].parent;

                sm_transform_s accumulator = *current_transform;
                while (parent != SM_SCENE_NO_PARENT_NODE)
                {
                    sm_entity_s parent_ett = scene->nodes[parent].entity;
                    if (sm_ecs_entity_is_valid(scene->ecs, parent_ett) && sm_ecs_entity_has_component(scene->ecs, parent_ett, SM_TRANSFORM_COMP))
                    {
                        sm_transform_s *parent_transform = sm_ecs_component_get_mut(scene->ecs, parent_ett, SM_TRANSFORM_COMP);
                        accumulator = sm_transform_component_combine(parent_transform, &accumulator);
                    }

                    parent = scene->nodes[parent].parent;
                }

                sm_world_s *current_world = sm_ecs_component_get_mut(scene->ecs, ett, SM_WORLD_COMP);

                sm_mat4 world_matrix;
                sm_transform_component_to_mat4(&accumulator, world_matrix.data);
                sm_world_component_store_matrix(current_world, &world_matrix);
            }
        }
    }

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {

        sm_entity_s ett = scene->nodes[i].entity;
        if (!sm_ecs_entity_is_valid(scene->ecs, ett))
        {
            continue;
        }

        if (sm_ecs_entity_has_component(scene->ecs, ett, SM_TRANSFORM_COMP | SM_WORLD_COMP))
        {
            sm_transform_s *transform = sm_ecs_component_get_mut(scene->ecs, ett, SM_TRANSFORM_COMP);
            sm_transform_component_set_dirty(transform, false);
        }
    }
#endif

    sm_ecs_do(scene->ecs, dt);
}

void sm_resource_mesh_calculate_aabb(sm_resource_mesh_s *mesh)
{

    // get min and max vertex to construct bounds (AABB)
    sm_vec3 min_vert;
    sm_vec3 max_vert;

    if (mesh->positions != NULL)
    {
        glm_vec3_copy(mesh->positions[0].data, min_vert.data);
        glm_vec3_copy(mesh->positions[0].data, max_vert.data);

        for (size_t i = 1; i < SM_ARRAY_LEN(mesh->positions); ++i)
        {
            glm_vec3_minv(min_vert.data, mesh->positions[i].data, min_vert.data);
            glm_vec3_maxv(max_vert.data, mesh->positions[i].data, max_vert.data);
        }
    }

    // create the bounding box
    sm_bounding_box_s aabb;
    glm_vec3_copy(min_vert.data, aabb.min.data);
    glm_vec3_copy(max_vert.data, aabb.max.data);

    mesh->aabb = aabb;
}

sm_bounding_box_s sm_resource_mesh_get_aabb(sm_resource_mesh_s *mesh)
{
    if (sm_resource_mesh_is_dirty(mesh))
    {
        sm_resource_mesh_calculate_aabb(mesh);
    }

    return mesh->aabb;
}

sm_intersect_result_s sm_resource_scene_intersects_capsule(sm_resource_scene_s *scene, sm_capsule_s capsule)
{

    SM_ASSERT(scene);

    sm_bounding_box_s capsule_aabb = sm_shape_get_aabb_capsule(capsule);

    u8 alloca_index = 0;
#define MAX_RESULTS 128
    sm_intersect_result_s *results = alloca(MAX_RESULTS * sizeof(sm_intersect_result_s));

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {

        if (strcmp(scene->nodes[i].name, "Player") == 0) continue;
        sm_entity_s ett = scene->nodes[i].entity;

        if (sm_ecs_entity_is_valid(scene->ecs, ett) && sm_ecs_entity_has_component(scene->ecs, ett, SM_WORLD_COMP | SM_RESOURCE_COMP))
        {

            sm_world_s *world = sm_ecs_component_get_mut(scene->ecs, ett, SM_WORLD_COMP);
            const sm_res_s *resource = sm_ecs_component_get(scene->ecs, ett, SM_RESOURCE_COMP);

            sm_resource_entity_s entt = sm_resource_manager_get_by_uuid(resource->uuid);
            if (entt.type != SM_RESOURCE_TYPE_MESH) continue;

            sm_resource_mesh_s *mesh = sm_resource_manager_get_data(entt);
            sm_bounding_box_s bbmesh = sm_resource_mesh_get_aabb(mesh);

            glm_mat4_mulv3(world->matrix.data, bbmesh.max.data, 1.0f, bbmesh.max.data);
            glm_mat4_mulv3(world->matrix.data, bbmesh.min.data, 1.0f, bbmesh.min.data);

            if (!sm_collision_aabbs(capsule_aabb, bbmesh))
            {
                sm_resource_mesh_set_draw_aabb(mesh, false);
                continue;
            }
            sm_resource_mesh_set_draw_aabb(mesh, true);

            for (size_t index = 0; index < SM_ARRAY_LEN(mesh->indices); index += 3)
            {

                sm_triangle_s triangle;
                sm_intersect_result_s result = {0};

                // TODO: support for vertex array without indices
                glm_vec3_copy(mesh->positions[mesh->indices[index + 0]].data, triangle.p0.data);
                glm_vec3_copy(mesh->positions[mesh->indices[index + 1]].data, triangle.p1.data);
                glm_vec3_copy(mesh->positions[mesh->indices[index + 2]].data, triangle.p2.data);

                glm_mat4_mulv3(world->matrix.data, triangle.p0.data, 1.0f, triangle.p0.data);
                glm_mat4_mulv3(world->matrix.data, triangle.p1.data, 1.0f, triangle.p1.data);
                glm_mat4_mulv3(world->matrix.data, triangle.p2.data, 1.0f, triangle.p2.data);

                sm_bounding_box_s triangle_aabb = sm_shape_get_aabb_triangle(triangle);
                if (!sm_collision_aabbs(capsule_aabb, triangle_aabb)) continue;

                sm_collision_capsule_triangle(capsule, triangle, world, &result);

                if (result.valid)
                {

                    SM_ASSERT(alloca_index < MAX_RESULTS);
                    results[alloca_index++] = result;
                }
            }
        }
    }

#undef MAX_RESULTS

    sm_intersect_result_s best_result = {0};
    if (alloca_index)
    {

        best_result = results[0];

        for (u8 i = 1; i < alloca_index; ++i)
        {
            if (results[i].depth > best_result.depth) best_result = results[i];
        }
    }

    return best_result;
}

void sm_resource_scene_set_parent(sm_resource_scene_s *scene, u32 node_index, u32 node_parent)
{
    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    scene->nodes[node_index].parent = node_parent;
}

u32 sm_resource_scene_get_parent(sm_resource_scene_s *scene, u32 node_index)
{
    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    return scene->nodes[node_index].parent;
}

void sm_resource_scene_set_name(sm_resource_scene_s *scene, u32 node_index, const SM_STRING name)
{
    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    if (scene->nodes[node_index].name)
    {
        SM_STRING_FREE(scene->nodes[node_index].name);
    }

    scene->nodes[node_index].name = SM_STRING_DUP(name);
}

const SM_STRING sm_resource_scene_get_name(sm_resource_scene_s *scene, u32 node_index)
{
    SM_ASSERT(scene);

    if (node_index >= SM_ARRAY_LEN(scene->nodes))
    {
        SM_LOG_ERROR("index %d out of bounds", node_index);
        return NULL;
    }

    return scene->nodes[node_index].name;
}

void sm_resource_scene_set_current_camera(sm_resource_scene_s *scene, sm_entity_s entity)
{

    SM_ASSERT(scene);

    if (!sm_ecs_entity_has_component(scene->ecs, entity, SM_CAMERA_COMP))
    {
        SM_LOG_ERROR("entity has no SM_CAMERA_COMP component");
        SM_UNREACHABLE( );
    }

    scene->current_camera = entity;
}

sm_entity_s sm_resource_scene_get_current_camera(sm_resource_scene_s *scene)
{

    SM_ASSERT(scene);

    if (!sm_ecs_entity_is_valid(scene->ecs, scene->current_camera))
    {
        SM_LOG_ERROR("scene has no camera set");
        SM_UNREACHABLE( );
    }

    return scene->current_camera;
}

b8 sm_resource_scene_has_parent(sm_resource_scene_s *scene, u32 node_index)
{
    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    return scene->nodes[node_index].parent != SM_SCENE_INVALID_NODE;
}

void sm_resource_scene_remove_child(sm_resource_scene_s *scene, u32 node_index, u32 node_child)
{
    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes[node_index].children); ++i)
    {
        if (scene->nodes[node_index].children[i] == node_child)
        {
            SM_ARRAY_DEL(scene->nodes[node_index].children, i, 1);
            return;
        }
    }
}

u32 sm_resource_scene_add_child(sm_resource_scene_s *scene, u32 node_index, u32 node_child)
{

    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    if (node_child == SM_SCENE_ROOT_NODE)
    {
        SM_LOG_ERROR("cannot add root as child");
        return SM_SCENE_INVALID_NODE;
    }

    if (node_child == node_index)
    {
        SM_LOG_ERROR("cannot add self as child");
        return SM_SCENE_INVALID_NODE;
    }

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes[node_child].children); ++i)
    {
        if (scene->nodes[node_child].children[i] == node_index)
        {
            SM_LOG_ERROR("cyclic dependency of %d and %d", node_index, node_child);
            return SM_SCENE_INVALID_NODE;
        }
    }

    u32 parent = scene->nodes[node_index].parent;
    while (parent != SM_SCENE_ROOT_NODE && parent != SM_SCENE_NO_PARENT_NODE && parent != SM_SCENE_INVALID_NODE)
    {
        if (node_child == parent)
        {
            SM_LOG_ERROR("cyclic dependency of %d and %d", node_index, node_child);
            return SM_SCENE_INVALID_NODE;
        }
        parent = scene->nodes[parent].parent;
    }

    if (scene->nodes[node_index].parent == SM_SCENE_INVALID_NODE)
    {
        SM_LOG_WARN("node %d has no parent", node_index);
        sm_resource_scene_add_child(scene, SM_SCENE_ROOT_NODE, node_index);
    }

    if (sm_resource_scene_has_parent(scene, node_child))
    {
        sm_resource_scene_remove_child(scene, scene->nodes[node_child].parent, node_child);
    }

    SM_ARRAY_PUSH(scene->nodes[node_index].children, node_child);
    sm_resource_scene_set_parent(scene, node_child, node_index);

    return node_child;
}

u32 sm_resource_scene_new_node(sm_resource_scene_s *scene)
{
    SM_ASSERT(scene);

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {
        if (scene->nodes[i].parent == SM_SCENE_INVALID_NODE)
        {
            return (u32)i;
        }
    }

    /* if we get here, there are no invalid nodes */
    sm_node_s n = sm_node_new_invalid( );
    SM_ARRAY_PUSH(scene->nodes, n);
    return (u32)SM_ARRAY_LEN(scene->nodes) - 1;
}

b8 sm_resource_scene_set_entity(sm_resource_scene_s *scene, u32 node_index, sm_entity_s entity)
{
    SM_ASSERT(scene);

    if (node_index >= SM_ARRAY_LEN(scene->nodes))
    {
        SM_LOG_ERROR("index %d out of bounds", node_index);
        return false;
    }

    if (scene->nodes[node_index].parent == SM_SCENE_INVALID_NODE)
    {
        SM_LOG_ERROR("node %d has no parent", node_index);
        return false;
    }

    scene->nodes[node_index].entity = entity;

    return true;
}

u32 sm_resource_scene_get_by_name(sm_resource_scene_s *scene, const SM_STRING name)
{

    SM_ASSERT(scene);
    if (!name) return SM_SCENE_INVALID_NODE;

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {
        if (scene->nodes[i].name && strcmp(scene->nodes[i].name, name) == 0) return (u32)i;
    }

    return SM_SCENE_INVALID_NODE;
}

u32 sm_resource_scene_get_by_entity(sm_resource_scene_s *scene, sm_entity_s entity)
{
    SM_ASSERT(scene);

    for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i)
    {
        sm_entity_s entt = scene->nodes[i].entity;
        if (entt.idx == entity.idx) return (u32)i;
    }

    return SM_SCENE_INVALID_NODE;
}

u32 sm_resource_scene_get_root(sm_resource_scene_s *scene)
{

    SM_ASSERT(scene);

    if (!SM_ARRAY_LEN(scene->nodes))
    {

        SM_ARRAY_SET_LEN(scene->nodes, 1);
        scene->nodes[0] = sm_node_new_invalid( );
        /* set the first node as the root node */
        scene->nodes[0].parent = SM_SCENE_NO_PARENT_NODE;
        scene->nodes[0].name = SM_STRING_DUP("SM_SCENE_ROOT_NODE");
    }

    return SM_SCENE_ROOT_NODE;
}

sm_entity_s sm_resource_scene_get_entity(sm_resource_scene_s *scene, u32 node_index)
{
    SM_ASSERT(scene);

    if (node_index >= SM_ARRAY_LEN(scene->nodes))
    {
        SM_LOG_ERROR("index %d out of bounds", node_index);
        return SM_ENTITY_INVALID( );
    }

    return scene->nodes[node_index].entity;
}

void sm_resource_scene_for_each(
    sm_resource_scene_s *scene, u32 node_index, void (*callback)(sm_resource_scene_s *scene, u32 node_index, void *user_data), void *user_data)
{

    SM_ASSERT(scene);
    SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

    // static int depth = -1;
    // depth++;

    // for (int i = 0; i < depth; ++i)
    // {
    //     printf("\t.");
    // }
    callback(scene, node_index, user_data);

    for (u32 i = 0; i < SM_ARRAY_LEN(scene->nodes[node_index].children); ++i)
    {
        sm_resource_scene_for_each(scene, scene->nodes[node_index].children[i], callback, user_data);
    }
    // depth--;
}

void sm_resource_scene_set_dirty(sm_resource_scene_s *scene, b8 dirty)
{
    if (dirty) SM_MASK_SET(scene->flags, SM_SCENE_FLAG_DIRTY);
    else SM_MASK_CLR(scene->flags, (u32)SM_SCENE_FLAG_DIRTY);
}

b8 sm_resource_scene_is_dirty(const sm_resource_scene_s *scene)
{
    return SM_MASK_CHK(scene->flags, SM_SCENE_FLAG_DIRTY);
}

void sm_resource_scene_set_on_cpu(sm_resource_scene_s *scene, b8 on_cpu)
{
    if (on_cpu) SM_MASK_SET(scene->flags, SM_SCENE_FLAG_ON_CPU);
    else SM_MASK_CLR(scene->flags, (u32)SM_SCENE_FLAG_ON_CPU);
}

b8 sm_resource_scene_is_on_cpu(const sm_resource_scene_s *scene)
{
    return SM_MASK_CHK(scene->flags, SM_SCENE_FLAG_ON_CPU);
}

b8 sm_resource_shader_read(const SM_STRING file_path, sm_resource_shader_s *shader)
{

    SM_ASSERT(file_path);
    SM_ASSERT(shader);

    SM_LOG_TRACE("Reading shader...");

    sm_file_handle_s file;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, false, &file))
    {
        SM_LOG_ERROR("[%s] failed to open file", file_path);
        return false;
    }

    // shader->name = SM_STRING_DUP(basename((i8 *)file_path));

    i8 line_buf[256];
    size_t file_size = sm_filesystem_size(&file);

    i8 *file_buf = SM_MALLOC(file_size);
    file_buf[0] = '\0';
    while (sm_filesystem_read_line(&file, line_buf, 256) && (strcmp(line_buf, "// SM FRAGMENT\n") != 0)) file_buf = strcat(file_buf, line_buf);

    shader->vertex_data = SM_STRING_DUP(file_buf);
    memset(file_buf, 0x0, sizeof(i8) * file_size);

    while (sm_filesystem_read_line(&file, line_buf, 256)) file_buf = strcat(file_buf, line_buf);

    shader->fragment_data = SM_STRING_DUP(file_buf);

    SM_FREE(file_buf);

    sm_resource_shader_set_on_cpu(shader, true);
    sm_resource_shader_set_on_gpu(shader, false);

    return true;
}

void sm_resource_shader_do(sm_resource_shader_s *shader)
{
    SM_ASSERT(shader);

    if (sm_rc_load(&shader->ref_count) > 0)
    {
        // TODO: Fix this
        SM_ASSERT(sm_resource_shader_is_on_cpu(shader));
    }
}

sm_resource_entity_s sm_resource_shader_make_reference(sm_resource_entity_s entity)
{
    SM_ASSERT(SM_RESOURCE_IS_VALID(entity));
    SM_ASSERT(entity.type == SM_RESOURCE_TYPE_SHADER);

    sm_rc_inc(&RESOURCE_MANAGER->shaders[entity.index].ref_count);

    if (sm_rc_load(&RESOURCE_MANAGER->shaders[entity.index].ref_count) == 1)
    {
        sm_resource_shader_do(&RESOURCE_MANAGER->shaders[entity.index]);
    }

    return entity;
}

void sm_resource_shader_load_gpu(sm_resource_shader_s *shader)
{

    SM_ASSERT(shader);

    if (!sm_resource_shader_is_on_gpu(shader))
    {
        if (!sm_gl_shader_new(shader))
        {
            SM_LOG_ERROR("failed to create shader");
            return;
        }

        SM_ASSERT(shader->program != 0);
        sm_resource_shader_set_on_gpu(shader, true);
    }
}

void sm_resource_shader_unload_cpu(sm_resource_shader_s *shader)
{

    SM_ASSERT(shader);

    if (shader->fragment_data) SM_STRING_FREE(shader->fragment_data);
    if (shader->vertex_data) SM_STRING_FREE(shader->vertex_data);
    sm_resource_shader_set_on_cpu(shader, false);
}

void sm_resource_shader_unload_gpu(sm_resource_shader_s *shader)
{

    SM_ASSERT(shader);

    if (shader->program > 0)
    {
        sm_gl_shader_delete(shader->program);
        shader->program = 0;
        sm_resource_shader_set_on_gpu(shader, false);
    }
}

void sm_resource_shader_dtor(sm_resource_shader_s *shader)
{

    SM_ASSERT(shader);

    sm_rc_dec(&shader->ref_count);
    if (sm_rc_load(&shader->ref_count) > 0) return;

    SM_LOG_INFO("destructing shader");
    sm_resource_shader_unload_cpu(shader);
    sm_resource_shader_unload_gpu(shader);
}

void sm_resource_shader_bind(sm_resource_entity_s entity)
{

    SM_ASSERT(entity.type == SM_RESOURCE_TYPE_SHADER);

    sm_resource_shader_s *shader = &RESOURCE_MANAGER->shaders[entity.index];
    sm_gl_shader_bind(shader->program);
}

void sm_resource_shader_ubind(void)
{
    sm_gl_shader_unbind( );
}

void sm_resource_shader_set_on_cpu(sm_resource_shader_s *shader, b8 on_cpu)
{
    if (on_cpu) shader->flags |= SM_SHADER_FLAG_ON_CPU;
    else shader->flags &= ~(u32)SM_SHADER_FLAG_ON_CPU;
}

b8 sm_resource_shader_is_on_cpu(const sm_resource_shader_s *shader)
{
    return shader->flags & SM_SHADER_FLAG_ON_CPU;
}

void sm_resource_shader_set_on_gpu(sm_resource_shader_s *shader, b8 on_cpu)
{
    if (on_cpu) shader->flags |= SM_SHADER_FLAG_ON_GPU;
    else shader->flags &= ~(u32)SM_SHADER_FLAG_ON_GPU;
}

b8 sm_resource_shader_is_on_gpu(const sm_resource_shader_s *shader)
{
    return shader->flags & SM_SHADER_FLAG_ON_GPU;
}

b8 sm___resource_mock_init(const SM_STRING root_folder)
{

    SM_ASSERT(RESOURCE_MANAGER == NULL && "resource already initialized");
    SM_ASSERT(root_folder);

    srand((u32)time(NULL));

    RESOURCE_MANAGER = SM_CALLOC(1, sizeof(resource_manager_s));
    SM_ASSERT(RESOURCE_MANAGER);

    RESOURCE_MANAGER->root_folder = SM_STRING_DUP(root_folder);

    RESOURCE_MANAGER->strmap = sm_hashmap_new_str( );
    if (!sm_hashmap_ctor_str(RESOURCE_MANAGER->strmap, 16, NULL, NULL))
    {
        SM_LOG_ERROR("failed to create hashmap");
        return false;
    }

    RESOURCE_MANAGER->u32map = sm_hashmap_new_u32( );
    if (!sm_hashmap_ctor_u32(RESOURCE_MANAGER->u32map, 16, NULL, NULL))
    {
        SM_LOG_ERROR("failed to create hashmap");
        return false;
    }

    return true;
}

void sm___resource_mock_teardown(void)
{

    SM_ASSERT(RESOURCE_MANAGER);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->shaders); ++i)
    {
        sm_resource_shader_s *shader = &RESOURCE_MANAGER->shaders[i];
        sm_resource_shader_dtor(shader);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->shaders);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->scenes); ++i)
    {
        sm_resource_scene_s *scene = &RESOURCE_MANAGER->scenes[i];
        sm_resource_scene_dtor(scene);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->scenes);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->meshes); ++i)
    {
        sm_resource_mesh_s *mesh = &RESOURCE_MANAGER->meshes[i];
        sm_resource_mesh_dtor(mesh);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->meshes);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->materials); ++i)
    {
        sm_resource_material_s *material = &RESOURCE_MANAGER->materials[i];
        sm_resource_material_dtor(material);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->materials);

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->textures); ++i)
    {
        sm_resource_texture_s *texture = &RESOURCE_MANAGER->textures[i];
        sm_resource_texture_dtor(texture);
    }
    SM_ARRAY_DTOR(RESOURCE_MANAGER->textures);

    sm_hashmap_for_each_str(RESOURCE_MANAGER->strmap, sm__reource_manager_dtor_cb, NULL);

    sm_hashmap_dtor_str(RESOURCE_MANAGER->strmap);
    sm_hashmap_dtor_u32(RESOURCE_MANAGER->u32map);

    SM_STRING_FREE(RESOURCE_MANAGER->root_folder);

    SM_FREE(RESOURCE_MANAGER);
    RESOURCE_MANAGER = NULL;
}

sm_resource_entity_s sm___resource_mock_push(const void *data, sm_resource_manager_type_e type)
{
    SM_ASSERT(RESOURCE_MANAGER);
    SM_ASSERT(data);

    switch (type)
    {
    case SM_RESOURCE_TYPE_TEXTURE:
        {
            sm_resource_texture_s texture = *(sm_resource_texture_s *)data;
            SM_STRING file_path = sm_resource_manager_construct_path(texture.header.name, SM_RESOURCE_TYPE_TEXTURE);

            SM_ARRAY_PUSH(RESOURCE_MANAGER->textures, texture);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->textures) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));

            // it is safe to store the texture pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->textures[index];

            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, texture.header.uuid, resource));

            SM_STRING_FREE(file_path);

            return resource->entity;

            break;
        }

    case SM_RESOURCE_TYPE_MATERIAL:
        {

            sm_resource_material_s material = *(sm_resource_material_s *)data;
            SM_STRING file_path = sm_resource_manager_construct_path(material.header.name, SM_RESOURCE_TYPE_MATERIAL);
            SM_ASSERT(material.header.uuid != 0);

            SM_ARRAY_PUSH(RESOURCE_MANAGER->materials, material);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->materials) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the material pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.index = index;
            resource->entity.type = SM_RESOURCE_TYPE_MATERIAL;
            resource->data = &RESOURCE_MANAGER->materials[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, material.header.uuid, resource));

            SM_STRING_FREE(file_path);

            return resource->entity;
            break;
        }

    case SM_RESOURCE_TYPE_MESH:
        {

            sm_resource_mesh_s mesh = *(sm_resource_mesh_s *)data;
            SM_STRING file_path = sm_resource_manager_construct_path(mesh.header.name, SM_RESOURCE_TYPE_MESH);

            SM_ASSERT(mesh.header.uuid != 0);

            SM_ARRAY_PUSH(RESOURCE_MANAGER->meshes, mesh);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->meshes) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the mesh pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->meshes[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, mesh.header.uuid, resource));

            SM_STRING_FREE(file_path);

            return resource->entity;
        }
    case SM_RESOURCE_TYPE_SCENE:
        {

            sm_resource_scene_s scene = *(sm_resource_scene_s *)data;
            SM_STRING file_path = sm_resource_manager_construct_path(scene.header.name, SM_RESOURCE_TYPE_SCENE);

            SM_ASSERT(scene.header.uuid != 0);

            SM_ARRAY_PUSH(RESOURCE_MANAGER->scenes, scene);
            u32 index = (u32)SM_ARRAY_LEN(RESOURCE_MANAGER->scenes) - 1;

            sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
            // it is safe to store the mesh pointer in the hashmap as it will be
            // initialized on startup and will only be destroyed when the program is
            // finished
            resource->entity.type = type;
            resource->entity.index = index;
            resource->data = &RESOURCE_MANAGER->scenes[index];

            // both hashmaps will point to the same resource
            SM_ASSERT(!sm_hashmap_put_str(RESOURCE_MANAGER->strmap, SM_STRING_DUP(file_path), resource));
            SM_ASSERT(!sm_hashmap_put_u32(RESOURCE_MANAGER->u32map, scene.header.uuid, resource));

            SM_STRING_FREE(file_path);

            return resource->entity;
        }
    default:
        {
            SM_LOG_ERROR("mock not implemented yet");
            exit(1);
        }
    }

    return SM_RESOURCE_INVALID_ENTITY;
}

sm_resource_texture_s *sm___resource_get_texture_pointer(void)
{

    return RESOURCE_MANAGER->textures;
}

b8 sm___resource_mock_write(void)
{

    SM_LOG_TRACE("mock writing...");

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->textures); ++i)
    {
        sm_resource_texture_s *texture = &RESOURCE_MANAGER->textures[i];
        if (!sm_resource_texture_write(texture))
        {
            SM_LOG_ERROR("error writing texture");
            return false;
        }
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->materials); ++i)
    {
        sm_resource_material_s *material = &RESOURCE_MANAGER->materials[i];
        if (!sm_resource_material_write(material))
        {
            SM_LOG_ERROR("error writing material");
            return false;
        }
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->meshes); ++i)
    {
        sm_resource_mesh_s *meshes = &RESOURCE_MANAGER->meshes[i];
        if (!sm_resource_mesh_write(meshes))
        {
            SM_LOG_ERROR("error writing mesh");
            return false;
        }
    }

    for (u32 i = 0; i < SM_ARRAY_LEN(RESOURCE_MANAGER->scenes); ++i)
    {
        sm_resource_scene_s *scene = &RESOURCE_MANAGER->scenes[i];
        if (!sm_resource_scene_write(scene))
        {
            SM_LOG_ERROR("error writing scene");
            return false;
        }
    }

    SM_LOG_TRACE("mock writing finished...");

    return true;
}

#undef SM_MODULE_NAME
