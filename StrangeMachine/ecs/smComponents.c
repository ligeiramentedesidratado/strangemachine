#include "smpch.h"

#include "core/smCore.h"
#include "ecs/smECS.h"

#include "ecs/smComponents.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "COMPONENTS"

const sm_component_t SM_TRANSFORM_COMP = 1ULL << 0;
const sm_component_t SM_WORLD_COMP = 1ULL << 1;
const sm_component_t SM_RESOURCE_COMP = 1ULL << 2;
const sm_component_t SM_RIGID_BODY_COMP = 1ULL << 3;
const sm_component_t SM_CAMERA_COMP = 1ULL << 4;
const sm_component_t SM_ALL_COMP = SM_TRANSFORM_COMP | SM_WORLD_COMP | SM_RESOURCE_COMP | SM_RIGID_BODY_COMP | SM_CAMERA_COMP;

static int initialized = 0;
SM_ARRAY(sm_component_view_s) COMPONENTS = NULL;

void component_init(void)
{

    if (initialized++ > 0)
    {
        return;
    }

    SM_ASSERT(COMPONENTS == NULL && "Component system already initialized");

    sm_component_view_s transform_view = {
        .name = SM_STRING_DUP("transform"),
        .size = sizeof(sm_transform_s),
        .aligned = true,
        .id = SM_TRANSFORM_COMP,
        .write = sm_transform_component_write,
        .read = sm_transform_component_read,
    };

    sm_component_view_s world_view = {
        .name = SM_STRING_DUP("world"),
        .size = sizeof(sm_world_s),
        .aligned = true,
        .id = SM_WORLD_COMP,
        .write = NULL,
        .read = NULL,
    };

    sm_component_view_s resource_view = {
        .name = SM_STRING_DUP("resource"),
        .size = sizeof(sm_res_s),
        .aligned = false,
        .id = SM_RESOURCE_COMP,
        .write = sm_resource_component_write,
        .read = sm_resource_component_read,
    };

    sm_component_view_s rigid_body_view = {
        .name = SM_STRING_DUP("rigid_body"),
        .size = sizeof(sm_rigid_body_s),
        .aligned = false,
        .id = SM_RIGID_BODY_COMP,
        .write = NULL,
        .read = NULL,
    };

    sm_component_view_s camera_view = {
        .name = SM_STRING_DUP("camera"),
        .size = sizeof(sm_camera_s),
        .aligned = false,
        .id = SM_CAMERA_COMP,
        .write = sm_camera_component_write,
        .read = sm_resource_component_read,
    };

    SM_ARRAY_PUSH(COMPONENTS, transform_view);
    SM_ARRAY_PUSH(COMPONENTS, world_view);
    SM_ARRAY_PUSH(COMPONENTS, resource_view);
    SM_ARRAY_PUSH(COMPONENTS, rigid_body_view);
    SM_ARRAY_PUSH(COMPONENTS, camera_view);
}

void component_register_component(sm_component_view_s *desc)
{

    SM_ASSERT(COMPONENTS != NULL && "Component system not initialized");
    SM_ASSERT(desc != NULL);
    SM_ASSERT(desc->id != 0);

#ifdef SM_DEBUG
    for (size_t i = 0; i < SM_ARRAY_LEN(COMPONENTS); i++)
    {
        SM_ASSERT(COMPONENTS[i].id != desc->id && "Component already registered");
    }
#endif

    SM_ARRAY_PUSH(COMPONENTS, *desc);
}

const sm_component_view_s *component_get_desc(sm_component_t id)
{

    SM_ASSERT(COMPONENTS != NULL && "Component system not initialized");

    for (size_t i = 0; i < SM_ARRAY_LEN(COMPONENTS); ++i)
    {
        if (COMPONENTS[i].id == id)
        {
            return &COMPONENTS[i];
        }
    }

    return NULL;
}

void component_teardown(void)
{

    if (--initialized > 0)
    {
        return;
    }

    SM_LOG_TRACE("component system teardown");

    SM_ASSERT(COMPONENTS != NULL && "component system not initialized");

    for (size_t i = 0; i < SM_ARRAY_LEN(COMPONENTS); ++i)
    {
        if (COMPONENTS[i].name) SM_STRING_FREE(COMPONENTS[i].name);
    }

    SM_ARRAY_DTOR(COMPONENTS);

    COMPONENTS = NULL;
}

b8 sm_rigid_body_write(const sm_file_handle_s *handle, const void *ptr)
{

    SM_ASSERT(ptr);

    SM_LOG_TRACE("Writing rigid body...");
    sm_rigid_body_s *rigid_body = (sm_rigid_body_s *)ptr;
    sm_file_handle_s *fh = (sm_file_handle_s *)handle;

    SM_LOG_TRACE("type: %u\n", rigid_body->type);
    if (!sm_filesystem_write_bytes(fh, &rigid_body->type, sizeof(rigid_body->type)))
    {
        SM_LOG_ERROR("[s] failed to write rigid body type");
        return false;
    }

    if (!sm_filesystem_write_bytes(fh, &rigid_body->shape, sizeof(rigid_body->shape)))
    {
        SM_LOG_ERROR("[s] failed to write rigid body shape");
        return false;
    }

    return true;
}

b8 sm_rigid_body_read(const sm_file_handle_s *handle, const void *ptr)
{

    SM_ASSERT(ptr);

    SM_LOG_TRACE("Reading rigid body...");
    sm_rigid_body_s *rigid_body = (sm_rigid_body_s *)ptr;
    sm_file_handle_s *fh = (sm_file_handle_s *)handle;

    if (!sm_filesystem_read_bytes(fh, &rigid_body->type, sizeof(rigid_body->type)))
    {
        SM_LOG_ERROR("failed to read rigid body type");
        return false;
    }

    SM_LOG_TRACE("type: %u\n", rigid_body->type);
    if (!sm_filesystem_read_bytes(fh, &rigid_body->shape, sizeof(rigid_body->shape)))
    {
        SM_LOG_ERROR("failed to read rigid body shape");
        return false;
    }

    return true;
}

#undef SM_MODULE_NAME
