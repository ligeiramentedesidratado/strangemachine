#include "smpch.h"

#include "ecs/smComponents.h"

b8 sm_transform_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_transform_s *transform = (sm_transform_s *)ptr;

    SM_LOG_TRACE("Writing transform...");

    SM_LOG_TRACE("position: %f, %f, %f", transform->position.x, transform->position.y, transform->position.z);
    if (!sm_filesystem_write_bytes(fhandle, &transform->position, sizeof(transform->position)))
    {
        SM_LOG_ERROR("[s] failed to write transform position");
        return false;
    }
    SM_LOG_TRACE("rotation: %f, %f, %f", transform->rotation.x, transform->rotation.y, transform->rotation.z);
    if (!sm_filesystem_write_bytes(fhandle, &transform->rotation, sizeof(transform->rotation)))
    {
        SM_LOG_ERROR("[s] failed to write transform rotation");
        return false;
    }
    SM_LOG_TRACE("scale: %f, %f, %f", transform->scale.x, transform->scale.y, transform->scale.z);
    if (!sm_filesystem_write_bytes(fhandle, &transform->scale, sizeof(transform->scale)))
    {
        SM_LOG_ERROR("[s] failed to write transform scale");
        return false;
    }

    return true;
}

b8 sm_transform_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_transform_s *transform = (sm_transform_s *)ptr;
    SM_LOG_TRACE("Reading transform...");

    if (!sm_filesystem_read_bytes(fhandle, &transform->position, sizeof(transform->position)))
    {
        SM_LOG_ERROR("[s] failed to write transform position");
        return false;
    }
    SM_LOG_TRACE("position: %f, %f, %f", transform->position.x, transform->position.y, transform->position.z);

    if (!sm_filesystem_read_bytes(fhandle, &transform->rotation, sizeof(transform->rotation)))
    {
        SM_LOG_ERROR("[s] failed to write transform rotation");
        return false;
    }
    SM_LOG_TRACE("rotation: %f, %f, %f", transform->rotation.x, transform->rotation.y, transform->rotation.z);

    if (!sm_filesystem_read_bytes(fhandle, &transform->scale, sizeof(transform->scale)))
    {
        SM_LOG_ERROR("[s] failed to write transform scale");
        return false;
    }
    SM_LOG_TRACE("scale: %f, %f, %f", transform->scale.x, transform->scale.y, transform->scale.z);

    sm_transform_component_set_dirty(transform, true);

    return true;
}

sm_transform_s sm_transform_component_from_mat4(sm_mat4 m)
{
    sm_transform_s out = {0};
    glm_vec3_copy(m.vec3.position.data, out.position.data); // rotation = first column of mat
    glm_mat4_quat(m.data, out.rotation.data);

    /* get the rotate scale matrix, then estimate the scale from that */
    sm_mat4 rot_scale_mat;
    glm_mat4_identity(rot_scale_mat.data);

    rot_scale_mat = sm_mat4_new(m.float16[0], m.float16[1], m.float16[2], 0, m.float16[4], m.float16[5], m.float16[6], 0, m.float16[8], m.float16[9],
        m.float16[10], 0, 0, 0, 0, 1);

    versor q;
    glm_quat_inv(out.rotation.data, q);
    sm_mat4 inv_rot_mat;
    glm_quat_mat4(q, inv_rot_mat.data);
    sm_mat4 scale_mat;
    glm_mat4_mul(rot_scale_mat.data, inv_rot_mat.data, scale_mat.data);

    /* the diagonal of the scale matrix is the scale */

    out.scale.data[0] = scale_mat.c0r0;
    out.scale.data[1] = scale_mat.c1r1;
    out.scale.data[2] = scale_mat.c2r2;

    /* out.position.data[3] = 1.0f; */
    /* out.scale.data[3] = 1.0f; */

    return out;
}

void sm_transform_component_to_mat4(const sm_transform_s *transform, mat4 out)
{
    /* first, extract the rotation basis of the transform */
    sm_vec4 x, y, z;

    vec3 right = {1.0f, 0.0f, 0.0f};
    vec3 up = {0.0f, 1.0f, 0.0f};
    vec3 forward = {0.0f, 0.0f, 1.0f};

    versor q = {transform->rotation.x, transform->rotation.y, transform->rotation.z, transform->rotation.w};

    glm_quat_rotatev(q, right, x.data);
    glm_quat_rotatev(q, up, y.data);
    glm_quat_rotatev(q, forward, z.data);

    /* next, scale the basis vectors */
    glm_vec4_scale(x.data, transform->scale.x, x.data);
    glm_vec4_scale(y.data, transform->scale.y, y.data);
    glm_vec4_scale(z.data, transform->scale.z, z.data);

    sm_mat4 result = sm_mat4_new(x.x, x.y, x.z, 0.0f,                             // X basis (and scale)
        y.x, y.y, y.z, 0.0f,                                                      // Y basis (and scale)
        z.x, z.y, z.z, 0.0f,                                                      // Z basis (and scale)
        transform->position.x, transform->position.y, transform->position.z, 1.0f // Position
    );

    glm_mat4_copy(result.data, out);
}

void sm_transform_component_set_dirty(sm_transform_s *transform, bool dirty)
{

    if (dirty) transform->flags |= SM_TRANSFORM_FLAG_DIRTY;
    else transform->flags &= ~(u32)SM_TRANSFORM_FLAG_DIRTY;
}

b8 sm_transform_component_is_dirty(const sm_transform_s *transform)
{
    return transform->flags & SM_TRANSFORM_FLAG_DIRTY;
}

sm_transform_s sm_transform_component_combine(const sm_transform_s *a, const sm_transform_s *b)
{

    sm_transform_s *_a = (sm_transform_s *)a;
    sm_transform_s *_b = (sm_transform_s *)b;
    sm_transform_s out;

    glm_vec3_mul(_a->scale.data, _b->scale.data, out.scale.data);
    glm_quat_mul(_b->rotation.data, _a->rotation.data, out.rotation.data);

    sm_vec3 pos;
    glm_vec3_mul(_a->scale.data, _b->position.data, pos.data);
    glm_quat_rotatev(_a->rotation.data, pos.data, out.position.data);
    glm_vec3_add(_a->position.data, out.position.data, out.position.data);

    sm_transform_component_set_dirty(&out, true);

    return out;
}
