#ifndef SM_SCENE_COMPONENTS_H
#define SM_SCENE_COMPONENTS_H

// #include "resource/smResource.h"
#include "smpch.h"

#include "core/smCore.h"
#include "math/smMath.h"
#include "smShapes.h"

typedef u64 sm_component_t;

typedef struct sm__entity_s
{

    u32 idx;

} sm_entity_s;

#define SM_ENTITY_INVALID_INDEX           0xFFFFFFFF
#define SM_ENTITY_INVALID( )              ((sm_entity_s){.idx = SM_ENTITY_INVALID_INDEX})
#define SM_ENTITY_IS_VALID(ENTITY)        ((ENTITY).idx != SM_ENTITY_INVALID_INDEX)
#define SM_ENTITY_CMP_EQ(ENTITY, AGAINST) ((ENTITY).idx == (AGAINST).idx)

extern const sm_component_t SM_TRANSFORM_COMP;

typedef struct __attribute__((packed)) sm__transform_s
{

    sm_vec4 position; // local position x y z plus padding (relative to its parent)
    sm_vec4 rotation; // local rotation x y z w (relative to its parent)
    sm_vec3 scale;    // local scale x y z (relative to its parent)

    enum sm_transform_component_flags_e
    {
        SM_TRANSFORM_FLAG_NONE = 0,
        SM_TRANSFORM_FLAG_DIRTY = 1 << 1,
        SM__TRANSFORM_FLAG_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } flags;

    char a, b;

} sm_transform_s;

b8 sm_transform_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);
b8 sm_transform_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);

void sm_transform_component_to_mat4(const sm_transform_s *t, mat4 out);
sm_transform_s sm_transform_component_from_mat4(sm_mat4 m);
void sm_transform_component_store_prev(sm_transform_s *transform);
void sm_transform_component_set_dirty(sm_transform_s *transform, bool dirty);
b8 sm_transform_component_is_dirty(const sm_transform_s *transform);
sm_transform_s sm_transform_component_combine(const sm_transform_s *a, const sm_transform_s *b);

#define sm_transform_print(T)                                                                            \
    printf("%s:\n", #T);                                                                                 \
    printf("\tposit: %f, %f, %f\n", (T).position.x, (T).position.y, (T).position.z);                     \
    printf("\trotat: %f, %f, %f, %f\n", (T).rotation.x, (T).rotation.y, (T).rotation.z, (T).rotation.w); \
    printf("\tscale: %f, %f, %f\n", (T).scale.x, (T).scale.y, (T).scale.z);

extern const sm_component_t SM_WORLD_COMP;

typedef struct sm__world_s
{
    sm_mat4 matrix;
    sm_mat4 prev_matrix;

} sm_world_s;

b8 sm_world_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);
b8 sm_world_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);

void sm_world_component_store_matrix(sm_world_s *world, sm_mat4 *new_world);

extern const sm_component_t SM_RESOURCE_COMP;

typedef struct sm__res_s
{

    u32 uuid;

} sm_res_s;

b8 sm_resource_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);
b8 sm_resource_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);

extern const sm_component_t SM_RIGID_BODY_COMP;

typedef struct sm__rigid_body_s
{

    sm_vec3 position;
    sm_vec3 force;
    sm_vec3 velocity;
    sm_vec3 gravity;

    enum sm_shape_type_e
    {
        SM_RB_SHAPE_TYPE_EMPTY = (0 << 0),

        SM_RB_SHAPE_TYPE_SPHERE = (1 << 0),  /* dynamic rigid body */
        SM_RB_SHAPE_TYPE_CAPSULE = (1 << 1), /* dynamic rigid body */
        SM_RB_SHAPE_TYPE_MESH = (1 << 2),    /* static rigid body */

        SM_RB_SHAPE_TYPE_STATIC = SM_RB_SHAPE_TYPE_MESH,
        SM_RB_SHAPE_TYPE_DYNAMIC = SM_RB_SHAPE_TYPE_SPHERE | SM_RB_SHAPE_TYPE_CAPSULE,

        SM_RB_SHAPE_TYPE_ALL = (SM_RB_SHAPE_TYPE_EMPTY | SM_RB_SHAPE_TYPE_SPHERE | SM_RB_SHAPE_TYPE_CAPSULE | SM_RB_SHAPE_TYPE_MESH),

        SM__RB_SHAPE_TYPE_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } type;

    sm_shape_u shape;

} sm_rigid_body_s;

b8 sm_rigid_body_write(const sm_file_handle_s *handle, const void *ptr);
b8 sm_rigid_body_read(const sm_file_handle_s *handle, const void *ptr);

static inline void sm_rigid_body_component_set_shape(sm_rigid_body_s *rigid_body, sm_shape_u shape, enum sm_shape_type_e type)
{

    rigid_body->type = type;
    rigid_body->shape = shape;
}

static inline void sm_rigid_body_add_force(sm_rigid_body_s *rigid_body, sm_vec3 force)
{

    glm_vec3_add(rigid_body->force.data, force.data, rigid_body->force.data);
}

extern const sm_component_t SM_CAMERA_COMP;

typedef struct sm__camera_s
{

    sm_vec3 position; /* current camera position */
    sm_vec3 target, _next_target;
    f32 target_distance;
    sm_vec3 right;
    sm_vec3 up;

    sm_vec3 angle; /* pitch, yaw and roll angle in degrees */

    f32 fov;
    f32 aspect_ratio;

    f32 move_speed; /* units per second */
    f32 sensitive;  /* degrees per second */

    enum sm_camera_component_projection_e
    {
        SM_CAM_PROJ_PERSPECTIVE,
        SM_CAM_PROJ_ORTHOGONAL,
        SM__CAM_PROJ_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } projection;

    enum sm_camera_component_mode_e
    {
        SM_CAM_MODE_FREE,
        SM_CAM_MODE_THIRD_PERSON,
        SM_CAM_MODE_CUSTOM,
        SM__CAM_ENFORCE_ENUM_SIZE = 0x7fffffff /* force 32-bit size enum */
    } mode;

} sm_camera_s;

b8 sm_camera_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);
b8 sm_camera_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data);

void sm_camera_component_set_aspect_ratio(sm_camera_s *camera, f32 aspect_ratio);
void sm_camera_component_get_projection(const sm_camera_s *camera, mat4 out_projection);
void sm_camera_component_get_view(const sm_camera_s *camera, mat4 out_view);
void sm_camera_component_set_target(sm_camera_s *camera, vec3 target);

extern const sm_component_t SM_ALL_COMP;

typedef void (*sm_component_dtor_f)(void *ptr, void *user_data);
typedef b8 (*sm_component_write_f)(const sm_file_handle_s *handle, const void *ptr, const void *user_data);
typedef b8 (*sm_component_read_f)(const sm_file_handle_s *handle, const void *ptr, const void *user_data);

typedef struct sm__component_view_s
{

    sm_component_t id;
    SM_STRING name;

    /* size in bytes of the component */
    size_t size;
    size_t offset;

    b8 aligned;
    sm_component_write_f write;
    sm_component_read_f read;
    /* sm_component_dtor_f dtor; */

} sm_component_view_s;

void component_init(void);
void component_teardown(void);

void component_register_component(sm_component_view_s *desc);
const sm_component_view_s *component_get_desc(sm_component_t id);

#endif /* SM_SCENE_COMPONENTS_H */
