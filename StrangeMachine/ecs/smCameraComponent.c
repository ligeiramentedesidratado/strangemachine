#include "smpch.h"

#include "ecs/smComponents.h"

b8 sm_camera_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_camera_s *camera = (sm_camera_s *)ptr;

    SM_LOG_TRACE("Writing camera...");

    if (!sm_filesystem_write_bytes(fhandle, camera, sizeof(sm_camera_s)))
    {
        SM_LOG_ERROR("failed to write camera");
        return false;
    }

    return true;
}

b8 sm_camera_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_camera_s *camera = (sm_camera_s *)ptr;

    SM_LOG_TRACE("Reading camera...");

    if (!sm_filesystem_read_bytes(fhandle, camera, sizeof(sm_camera_s)))
    {
        SM_LOG_ERROR("failed to read camera");
        return false;
    }

    return true;
}

void sm_camera_component_set_aspect_ratio(sm_camera_s *camera, f32 aspect_ratio)
{

    if (aspect_ratio > 0.0f) camera->aspect_ratio = aspect_ratio;
}

void sm_camera_component_get_projection(const sm_camera_s *camera, mat4 out_projection)
{

    if (camera->projection == SM_CAM_PROJ_PERSPECTIVE) glm_perspective(glm_rad(camera->fov), camera->aspect_ratio, 0.01f, 100.0f, out_projection);

    else if (camera->projection == SM_CAM_PROJ_ORTHOGONAL)
    {

        // https://stackoverflow.com/a/55009832
        f32 ratio_size_per_depth = atanf(glm_rad(camera->fov / 2.0f)) * 2.0f;
        vec3 dist;
        glm_vec3_sub((f32 *)camera->target.data, (f32 *)camera->position.data, dist);
        f32 distance = glm_vec3_norm(dist);

        f32 size_y = ratio_size_per_depth * distance;
        f32 size_x = ratio_size_per_depth * distance * camera->aspect_ratio;

        glm_ortho(-size_x, size_x, -size_y, size_y, 0.01f, 100.0f, out_projection);
    }
    else SM_UNREACHABLE( );
}

void sm_camera_component_get_view(const sm_camera_s *camera, mat4 out_view)
{

    if (camera->mode == SM_CAM_MODE_THIRD_PERSON)
    {

        glm_lookat((f32 *)camera->position.data, (f32 *)camera->target.data, (f32 *)camera->up.data, out_view);
    }
    else if (camera->mode == SM_CAM_MODE_FREE)
    {

        vec3 dist;
        glm_vec3_sub((f32 *)camera->position.data, (f32 *)camera->target.data, dist);
        glm_lookat((f32 *)camera->position.data, dist, (f32 *)camera->up.data, out_view);
    }
}

void sm_camera_component_set_target(sm_camera_s *camera, vec3 target)
{

    glm_vec3_copy(target, camera->_next_target.data);
}
