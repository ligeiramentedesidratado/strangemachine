#include "cglm/vec3.h"
#include "smpch.h"

#include "ecs/smECS.h"
#include "smInput.h"

// void sm_system_mesh_default_update(sm_ecs_s *ecs, f32 dt)
// {
//
//   SM_ASSERT(ecs);
//
//   SM_UNUSED(dt);
//
//   sm_system_iterator_s iter = sm_ecs_iter_begin(ecs, SM_MESH_COMP);
//
//   while (sm_ecs_iter_next(ecs, &iter)) {
//
//     sm_mesh_s *mesh = sm_ecs_iter_get_component_mut(&iter, SM_MESH_COMP);
//
//     if (sm_mesh_component_is_dirty(mesh)) {
//
//       mesh->aabb = sm_shape_get_positions_aabb(mesh->positions);
//
//       if (sm_mesh_component_is_renderable(mesh)) {
//         sm_mesh_component_upload_gpu(mesh);
//       }
//     }
//   }
// }

void sm_system_camera_default_update(sm_ecs_world_s *ecs, f32 dt)
{

    sm_system_iterator_s iter = sm_ecs_iter_begin(ecs, SM_CAMERA_COMP);

    while (sm_ecs_iter_next(ecs, &iter))
    {

        sm_camera_s *camera = sm_ecs_iter_get_component_mut(&iter, SM_CAMERA_COMP);

        if (camera->mode == SM_CAM_MODE_THIRD_PERSON)
        {

            float offset_x = input_get_x_rel_mouse( );
            float offset_y = input_get_y_rel_mouse( );

            if (offset_x != 0 || offset_y != 0)
            {
                camera->angle.pitch += (offset_x * -camera->sensitive * dt);
                camera->angle.yaw += (offset_y * -camera->sensitive * dt);

                if (camera->angle.yaw > glm_rad(-5.0f))
                {
                    camera->angle.yaw = glm_rad(-5.0f);
                }
                else if (camera->angle.yaw < glm_rad(-70.0f))
                {
                    camera->angle.yaw = glm_rad(-70.0f);
                }
            }

            glm_vec3_lerp(camera->target.data, camera->_next_target.data, 10 * dt, camera->target.data);

            camera->target_distance -= (input_get_mouse_scroll( ) * 1.4f);

            if (camera->target_distance < 1.2f)
            {
                camera->target_distance = 1.2f;
            }

            // TODO: It seems camera.position is not correctly updated or some rounding
            // issue makes the camera move straight to camera.target...
            sm_vec3 nex_position = {0};
            nex_position.x = sinf(camera->angle.pitch) * camera->target_distance * cosf(camera->angle.yaw) + camera->target.x;

            if (camera->angle.yaw <= 0.0f)
            {
                nex_position.y = sinf(camera->angle.yaw) * camera->target_distance * sinf(camera->angle.yaw) + camera->target.y;
            }
            else
            {
                nex_position.y = -sinf(camera->angle.yaw) * camera->target_distance * sinf(camera->angle.yaw) + camera->target.y;
            }

            nex_position.z = cosf(camera->angle.pitch) * camera->target_distance * cosf(camera->angle.yaw) + camera->target.z;

            glm_vec3_lerp(camera->position.data, nex_position.data, 10 * dt, camera->position.data);
        }

        if (camera->mode == SM_CAM_MODE_FREE)
        {
            sm_vec3 pos;

            if (input_scan_key(sm_key_s))
            {
                glm_vec3_scale(camera->target.data, camera->move_speed * dt, pos.data);
                glm_vec3_add(camera->position.data, pos.data, camera->position.data);
            }

            if (input_scan_key(sm_key_w))
            {
                glm_vec3_scale(camera->target.data, camera->move_speed * dt, pos.data);
                glm_vec3_sub(camera->position.data, pos.data, camera->position.data);
            }

            if (input_scan_key(sm_key_d))
            {
                glm_vec3_scale(camera->right.data, camera->move_speed * dt, pos.data);
                glm_vec3_sub(camera->position.data, pos.data, camera->position.data);
            }

            if (input_scan_key(sm_key_a))
            {
                glm_vec3_scale(camera->right.data, camera->move_speed * dt, pos.data);
                glm_vec3_add(camera->position.data, pos.data, camera->position.data);
            }

            if (input_scan_key(sm_key_space))
            {
                glm_vec3_scale(camera->up.data, camera->move_speed * dt, pos.data);
                glm_vec3_add(camera->position.data, pos.data, camera->position.data);
            }
            if (input_scan_key(sm_key_lshift))
            {
                glm_vec3_scale(camera->up.data, camera->move_speed * dt, pos.data);
                glm_vec3_sub(camera->position.data, pos.data, camera->position.data);
            }

            f32 offset_x = input_get_x_rel_mouse( );
            f32 offset_y = input_get_y_rel_mouse( );

            camera->fov -= input_get_mouse_scroll( ) * 100.0f * dt;
            if (camera->fov < 1.0f) camera->fov = 1;
            if (camera->fov > 80.0f) camera->fov = 80.0f;

            camera->angle.yaw += offset_x * camera->sensitive * dt;
            camera->angle.pitch += -offset_y * camera->sensitive * dt;

            if (camera->angle.pitch > 80.0f) camera->angle.pitch = 80.0f;
            if (camera->angle.pitch < -80.0f) camera->angle.pitch = -80.0f;

            if (camera->angle.yaw > 360.0f || camera->angle.yaw < -360.0f) camera->angle.yaw = 0.0f;

            sm_vec3 direction;
            direction.x = cosf(glm_rad(camera->angle.yaw)) * cosf(glm_rad(camera->angle.pitch));
            direction.y = sinf(glm_rad(camera->angle.pitch));
            direction.z = sinf(glm_rad(camera->angle.yaw)) * cosf(glm_rad(camera->angle.pitch));

            glm_vec3_normalize_to(direction.data, camera->target.data);

            sm_vec3 right;
            glm_vec3_cross(camera->target.data, (vec3){0.0f, 1.0f, 0.0f}, right.data);
            glm_vec3_normalize_to(right.data, camera->right.data);

            sm_vec3 up;
            glm_vec3_cross(camera->right.data, camera->target.data, up.data);
            glm_vec3_normalize_to(up.data, camera->up.data);
        }
    }
}
