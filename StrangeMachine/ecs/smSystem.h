#ifndef SM_SCENE_SYSTEM_H
#define SM_SCENE_SYSTEM_H

#include "smpch.h"

#include "core/smCore.h"

#include "ecs/smComponents.h"

typedef struct sm__ecs_query_s {

  sm_component_t id;

  /* size in bytes of the component */
  size_t size;
  size_t offset;

} sm_ecs_query_s;

typedef struct sm__system_iterator_s {
  sm_component_t components;
  size_t length; /* length of the array */
  size_t size;   /* size of each element */

  u32 index;

  u8 query_count;
  sm_ecs_query_s query[16];

  sm_hash_map_iter_u64_s _map_iter;

  void *data;

} sm_system_iterator_s;

typedef struct sm__ecs_world_s sm_ecs_world_s;
typedef void (*system_f)(sm_ecs_world_s *ecs, f32 dt);

void sm_system_mesh_default_update(sm_ecs_world_s *ecs, f32 dt);
void sm_system_camera_default_update(sm_ecs_world_s *ecs, f32 dt);

#endif /* SM_SCENE_SYSTEM_H */
