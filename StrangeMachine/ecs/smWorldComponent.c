#include "smpch.h"

#include "ecs/smComponents.h"

b8 sm_world_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_world_s *world = (sm_world_s *)ptr;

    SM_LOG_TRACE("Writing world...");

    if (!sm_filesystem_write_bytes(fhandle, &world->matrix, sizeof(world->matrix)))
    {
        SM_LOG_ERROR("failed to write world matrix");
        return false;
    }

    return true;
}

b8 sm_world_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_world_s *world = (sm_world_s *)ptr;

    SM_LOG_TRACE("Reading world...");

    if (!sm_filesystem_read_bytes(fhandle, &world->matrix, sizeof(world->matrix)))
    {
        SM_LOG_ERROR("failed to read world matrix");
        return false;
    }
    return true;
}

void sm_world_component_store_matrix(sm_world_s *world, sm_mat4 *new_world)
{
    glm_mat4_copy(world->matrix.data, world->prev_matrix.data);
    glm_mat4_copy(new_world->data, world->matrix.data);
}
