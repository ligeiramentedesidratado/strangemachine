#include "ecs/smComponents.h"
#include "smpch.h"

#include "ecs/smECS.h"

#include "core/smCore.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "ECS"

#define BIT (sizeof(u64) * 8)

sm_chunk_s *sm__ecs_chunk_ctor(sm_component_t archetype)
{

    sm_chunk_s *new_chunk = SM_CALLOC(1, sizeof(sm_chunk_s));

    new_chunk->pool = sm_handle_pool_new( );
    if (!sm_handle_pool_ctor(new_chunk->pool, 4))
    {
        SM_LOG_WARN("failed to create pool for archetype %lu", archetype);
        return NULL;
    }

    for (u32 i = 0; i < BIT; ++i)
    {
        sm_component_t component = archetype & (sm_component_t)(1ULL << i);

        if (component)
        {
            sm_component_view_s view = *component_get_desc(component);

            if (view.aligned && (new_chunk->size & 0xFUL))
            {
                size_t padding_size = (new_chunk->size + 0xFULL) & ~(0xFULL);

                sm_component_view_s padding_view = {0};
                padding_view.size = padding_size - new_chunk->size;
                padding_view.offset = new_chunk->size;

                new_chunk->size += padding_view.size;
                SM_ARRAY_PUSH(new_chunk->view, padding_view);
            }

            view.offset += new_chunk->size;

            new_chunk->size += view.size;
            SM_ARRAY_PUSH(new_chunk->view, view);
        }
    }

    if (new_chunk->size & 0xFULL)
    {
        size_t padding_size = (new_chunk->size + 0xFULL) & ~(0xFULL);
        new_chunk->size += padding_size - new_chunk->size;
    }

    return new_chunk;
}

void sm__ecs_chunk_dtor(sm_chunk_s *chunk)
{

    SM_ECS_CHUNK_ASSERT(chunk);

    sm_handle_pool_dtor(chunk->pool);
    SM_ALIGNED_FREE(chunk->data);
    SM_ARRAY_DTOR(chunk->view);
    SM_FREE(chunk);
}

sm_ecs_world_s *sm_ecs_world_new(void)
{

    sm_ecs_world_s *ecs = (sm_ecs_world_s *)SM_CALLOC(1, sizeof(sm_ecs_world_s));
    SM_ASSERT(ecs);

    return ecs;
}

b8 sm_ecs_world_ctor(sm_ecs_world_s *world, sm_component_t components)
{

    SM_ASSERT(world);

    memcpy((void *)&world->registered_components, &components, sizeof(components));

    component_init( );

    world->map_components = sm_hashmap_new_u64( );
    if (!sm_hashmap_ctor_u64(world->map_components, 16, NULL, NULL))
    {
        SM_LOG_ERROR("Failed to create hashmap");
        return false;
    }

    /* sm_ecs_system_register(ecs, sm_system_mesh_default_update); */
    sm_ecs_system_register(world, sm_system_camera_default_update);

    SM_ARRAY_SET_LEN(world->entities_metadata, 64);

    return true;
}

b8 sm__ecs_world_dtor_cb(sm_component_t key, void *value, void *user_data)
{

    SM_UNUSED(key);
    SM_UNUSED(user_data);

    sm_chunk_s *chunk = value;
    sm__ecs_chunk_dtor(chunk);

    return true;
}

void sm_ecs_world_dtor(sm_ecs_world_s *world)
{

    SM_ASSERT(world);

    if (world->systems) SM_ARRAY_DTOR(world->systems);
    if (world->entities_metadata) SM_ARRAY_DTOR(world->entities_metadata);

    sm_hashmap_for_each_u64(world->map_components, sm__ecs_world_dtor_cb, NULL);
    sm_hashmap_dtor_u64(world->map_components);

    component_teardown( );

    SM_FREE(world);
}

void sm_ecs_do(sm_ecs_world_s *world, f32 dt)
{

    for (size_t i = 0; i < SM_ARRAY_LEN(world->systems); ++i)
        if (world->systems[i]) world->systems[i](world, dt);
}

sm_entity_metadata_s sm_ecs_entity_metadata_new(sm_ecs_world_s *world, sm_component_t archetype)
{

    SM_ASSERT(SM_MASK_CHK_EQ(world->registered_components, archetype));

    sm_chunk_s *chunk = sm_hashmap_get_u64(world->map_components, archetype);
    if (chunk == NULL)
    {
        chunk = sm__ecs_chunk_ctor(archetype);
        SM_ECS_CHUNK_ASSERT(chunk);

        sm_hashmap_put_u64(world->map_components, archetype, chunk);
    }

    sm_handle_t handle = sm_handle_new(chunk->pool);
    SM_ASSERT(handle != SM_INVALID_HANDLE);

    size_t pool_cap = sm_handle_pool_get_cap(chunk->pool);

    if (chunk->length != pool_cap)
    {
        chunk->data = SM_ALIGNED_REALLOC(chunk->data, 16, chunk->size * pool_cap);
        SM_ASSERT(chunk->data);

        memset((u8 *)chunk->data + (chunk->size * chunk->length), 0x0, chunk->size * (pool_cap - chunk->length));
        chunk->length = pool_cap;
    }

    sm_entity_metadata_s entity_metadata = {.archetype = archetype, .handle = handle, .tag = 0x0};

    return entity_metadata;
}

sm_entity_s sm_ecs_entity_new(sm_ecs_world_s *world, sm_component_t archetype)
{

    sm_entity_metadata_s entity_metadata = sm_ecs_entity_metadata_new(world, archetype);

    SM_ARRAY_PUSH(world->entities_metadata, entity_metadata);

    u32 index = (u32)SM_ARRAY_LEN(world->entities_metadata) - 1;

    sm_entity_s entity = (sm_entity_s){.idx = index};

    return entity;
}

void sm_ecs_entity_add_component(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component)
{

    SM_ASSERT(world);

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    SM_ASSERT(SM_ENTITY_METADATA_IS_VALID(entity_metadata));

    if (SM_MASK_CHK(entity_metadata.archetype, component))
    {
        SM_LOG_WARN("entity %u already has %lu component", entity_metadata.handle, component);
        return;
    }

    sm_entity_metadata_s new_entity_metadata = sm_ecs_entity_metadata_new(world, entity_metadata.archetype | component);

    sm_chunk_s *new_chunk = sm_hashmap_get_u64(world->map_components, new_entity_metadata.archetype);
    SM_ECS_CHUNK_ASSERT(new_chunk);
    u32 new_index = sm_handle_index(new_entity_metadata.handle);

    sm_chunk_s *chunk = sm_hashmap_get_u64(world->map_components, entity_metadata.archetype);
    SM_ECS_CHUNK_ASSERT(chunk);
    u32 index = sm_handle_index(entity_metadata.handle);

    for (u32 i = 0; i < SM_ARRAY_LEN(chunk->view); ++i)
    {
        sm_component_view_s *src_view = &chunk->view[i];

        for (u32 j = 0; j < SM_ARRAY_LEN(new_chunk->view); ++j)
        {
            sm_component_view_s *dest_view = &new_chunk->view[j];
            if (dest_view->id == src_view->id)
            {
                void *dest = (u8 *)new_chunk->data + (new_index * new_chunk->size) + dest_view->offset;
                void *src = (u8 *)chunk->data + (index * chunk->size) + src_view->offset;
                memcpy(dest, src, src_view->size);
            }
        }
    }

    memset((u8 *)chunk->data + (index * chunk->size), 0x0, chunk->size);
    sm_handle_del(chunk->pool, entity_metadata.handle);

    world->entities_metadata[entity.idx].archetype = new_entity_metadata.archetype;
    world->entities_metadata[entity.idx].handle = new_entity_metadata.handle;
}

sm_component_t sm_ecs_entity_get_archetype(sm_ecs_world_s *world, sm_entity_s entity)
{

    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    return entity_metadata.archetype;
}

sm_handle_t sm_ecs_entity_get_handle(sm_ecs_world_s *world, sm_entity_s entity)
{

    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    return entity_metadata.handle;
}

u32 sm_ecs_entity_get_tag(sm_ecs_world_s *world, sm_entity_s entity)
{
    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    return entity_metadata.tag;
}

void sm_ecs_entity_set_tag(sm_ecs_world_s *world, sm_entity_s entity, u32 tag)
{

    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    world->entities_metadata[entity.idx].tag = tag;
}

b8 sm_ecs_entity_is_valid(sm_ecs_world_s *world, sm_entity_s entity)
{
    SM_ASSERT(world);

    if (entity.idx >= SM_ARRAY_LEN(world->entities_metadata))
    {
        return false;
    }

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    return SM_ENTITY_METADATA_IS_VALID(entity_metadata);
}

b8 sm_ecs_entity_has_component(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t components)
{
    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    return SM_MASK_CHK_EQ(entity_metadata.archetype, components);
}

b8 sm_ecs_entity_has_any_component(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t components)
{
    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    return SM_MASK_CHK(entity_metadata.archetype, components);
}

void *sm__ecs_component_data(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component)
{

    SM_ASSERT(world);

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    SM_ASSERT(SM_MASK_CHK(entity_metadata.archetype, component));

    sm_chunk_s *chunk = sm_hashmap_get_u64(world->map_components, entity_metadata.archetype);
    SM_ECS_CHUNK_ASSERT(chunk);

    SM_ASSERT(sm_handle_valid(chunk->pool, entity_metadata.handle));

    u32 index = sm_handle_index(entity_metadata.handle);
    SM_ASSERT(index < chunk->length);

    for (u32 i = 0; i < SM_ARRAY_LEN(chunk->view); ++i)
    {
        sm_component_view_s *v = &chunk->view[i];
        if (SM_MASK_CHK(component, v->id)) return (u8 *)chunk->data + (index * chunk->size) + v->offset;
    }

    SM_UNREACHABLE( );
}

void *sm_ecs_component_set(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component)
{
    return sm__ecs_component_data(world, entity, component);
}

void *sm_ecs_component_get_mut(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component)
{
    return sm__ecs_component_data(world, entity, component);
}

const void *sm_ecs_component_get(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component)
{
    return sm__ecs_component_data(world, entity, component);
}

void sm_ecs_system_register(sm_ecs_world_s *world, system_f system)
{

    SM_ASSERT(world);

    SM_ARRAY_PUSH(world->systems, system);
}

sm_system_iterator_s sm_ecs_iter_begin(sm_ecs_world_s *world, sm_component_t components)
{

    sm_system_iterator_s iter = {0};

    sm_hash_map_iter_u64_s map_iter = sm_hash_map_iter_begin_u64(world->map_components);

    iter.components = components;
    iter._map_iter = map_iter;

    return iter;
}

b8 sm_ecs_iter_next(sm_ecs_world_s *world, sm_system_iterator_s *iter)
{

    if (iter->data == NULL || iter->index + 1 >= iter->length)
    {

        while (sm_hash_map_iter_next_u64(world->map_components, &iter->_map_iter))
        {

            sm_component_t key = iter->_map_iter.entry->key;

            if (SM_MASK_CHK_EQ(key, iter->components))
            {

                // if is a new entry generate a new query
                if (iter->_map_iter.new_entry)
                {
                    // reset query count an current index
                    iter->query_count = 0;
                    iter->index = 0;

                    size_t offset = 0;
                    for (u32 k = 0; k < BIT; ++k)
                    {

                        // Get component by component
                        sm_component_t cmp = key & (sm_component_t)(1ULL << k);

                        if (cmp)
                        {
                            // if the key has the component get its description
                            const sm_component_view_s *desc = component_get_desc(cmp);

                            if (SM_MASK_CHK(iter->components, cmp))
                                iter->query[iter->query_count++] = (sm_ecs_query_s){.id = desc->id, .size = desc->size, .offset = offset};

                            offset += desc->size;
                        }
                    }
                };

                sm_chunk_s *chunk = iter->_map_iter.entry->value;
                iter->data = chunk->data;
                iter->length = chunk->length;
                iter->size = chunk->size;
                return true;
            }
        }

        return false;
    }

    iter->index++;
    return true;
}

void *sm_ecs_iter_get_component_mut(sm_system_iterator_s *iter, sm_component_t component)
{

    SM_ASSERT(iter);
    SM_ASSERT(SM_MASK_CHK_EQ(iter->components, component));

    for (u8 i = 0; i < iter->query_count; ++i)
    {

        if (SM_MASK_CHK_EQ(iter->query[i].id, component)) return ((u8 *)iter->data) + (iter->index * iter->size) + iter->query[i].offset;
    }

    return NULL;
}

const void *sm_ecs_iter_get_component(sm_system_iterator_s *iter, sm_component_t component)
{

    return sm_ecs_iter_get_component_mut(iter, component);
}

SM_STRING sm_ecs_entity_to_string(sm_ecs_world_s *world, sm_entity_s entity)
{

    SM_ASSERT(world);
    SM_ASSERT(entity.idx < SM_ARRAY_LEN(world->entities_metadata));

    sm_entity_metadata_s entity_metadata = world->entities_metadata[entity.idx];

    SM_STRING str = SM_MALLOC(256);
    str[0] = '\0';

    for (u32 i = 0; i < BIT; i++)
    {
        sm_component_t c = entity_metadata.archetype & (sm_component_t)(1ULL << i);
        if (c)
        {
            const sm_component_view_s *desc = component_get_desc(c);
            if (*str)
            {
                strcat(str, "|");
            }
            strcat(str, desc->name);
        }
    }

    SM_STRING buf = str;
    while (*buf)
    {
        *buf = (i8)toupper(*buf);
        buf++;
    }

    return str;
}

#undef SM_MODULE_NAME
