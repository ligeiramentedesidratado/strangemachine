#include "smpch.h"

#include "core/smCore.h"
#include "ecs/smComponents.h"

b8 sm_resource_component_write(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{

    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_res_s *resource = (sm_res_s *)ptr;

    SM_LOG_TRACE("Writing resource..");

    SM_LOG_TRACE("uuid: %u", resource->uuid);
    if (!sm_filesystem_write_bytes(fhandle, &resource->uuid, sizeof(resource->uuid)))
    {
        SM_LOG_ERROR("failed to write resource uuid");
        return false;
    }

    return true;
}

b8 sm_resource_component_read(const sm_file_handle_s *fhandle, const void *ptr, const void *user_data)
{
    SM_ASSERT(fhandle);
    SM_ASSERT(ptr);
    SM_UNUSED(user_data);

    sm_res_s *resource = (sm_res_s *)ptr;

    SM_LOG_TRACE("Reading resource..");

    if (!sm_filesystem_read_bytes(fhandle, &resource->uuid, sizeof(resource->uuid)))
    {
        SM_LOG_ERROR("failed to write resource uuid");
        return false;
    }
    SM_LOG_TRACE("uuid: %u", resource->uuid);

    return true;
}
