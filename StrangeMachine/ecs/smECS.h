#ifndef SM_ECS_H
#define SM_ECS_H

#include "smpch.h"

#include "ecs/smComponents.h"
#include "ecs/smEntity.h"
#include "ecs/smSystem.h"

/* ECS allocates memory in "chuncks". A chunck always contains entities of a
 * single archetype */
typedef struct sm__chunk_s
{
    struct sm_handle_pool_s *pool;

    size_t length; /* number of components in the pool */
    size_t size;   /* total size in bytes  */

    SM_ARRAY(sm_component_view_s) view;

    void *data;

} sm_chunk_s;

#define SM_ECS_CHUNK_ASSERT(CHUNK)                  \
    do {                                            \
        SM_ASSERT(CHUNK && "chunk not found");      \
        SM_ASSERT(CHUNK->pool && "pool not found"); \
    } while (0)

typedef struct sm__entity_metadata_s
{
    sm_component_t archetype;
    sm_handle_t handle;
    u32 tag;

} sm_entity_metadata_s;

#define SM_ENTITY_METADATA_INVALID( )              ((sm_entt_s){.archetype = 0x0, .handle = SM_INVALID_HANDLE, .tag = 0xFFFFFFFFu})
#define SM_ENTITY_METADATA_IS_VALID(ENTITY)        ((ENTITY).handle != SM_INVALID_HANDLE && (ENTITY).archetype != 0u)
#define SM_ENTITY_METADATA_CMP_EQ(ENTITY, AGAINST) ((ENTITY).handle == (AGAINST).handle && (ENTITY).archetype == (AGAINST).archetype)

typedef struct sm__ecs_world_s
{
    /* All registered components in the world */
    const sm_component_t registered_components;

    SM_ARRAY(sm_entity_metadata_s) entities_metadata;
    sm_hashmap_u64_m *map_components;
    SM_ARRAY(system_f) systems;

} sm_ecs_world_s;

// ============================= World =============================
sm_ecs_world_s *sm_ecs_world_new(void);
b8 sm_ecs_world_ctor(sm_ecs_world_s *world, sm_component_t components);
void sm_ecs_world_dtor(sm_ecs_world_s *world);

// ============================= Entities =============================
sm_entity_s sm_ecs_entity_new(sm_ecs_world_s *world, sm_component_t archetype);

// After an entity has been created, you can add components. When you do this,
// the archetype of the affected entities change and the ECS must move altered
// data to a new chunk of memory, as well as condense the component arrays in
// the original chunks.
void sm_ecs_entity_add_component(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component);

sm_component_t sm_ecs_entity_get_archetype(sm_ecs_world_s *world, sm_entity_s entity);
sm_handle_t sm_ecs_entity_get_handle(sm_ecs_world_s *world, sm_entity_s entity);
u32 sm_ecs_entity_get_tag(sm_ecs_world_s *world, sm_entity_s entity);
void sm_ecs_entity_set_tag(sm_ecs_world_s *world, sm_entity_s entity, u32 tag);

b8 sm_ecs_entity_is_valid(sm_ecs_world_s *world, sm_entity_s entity);
b8 sm_ecs_entity_has_component(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t components);
b8 sm_ecs_entity_has_any_component(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t components);

// ============================= Components =============================
void *sm_ecs_component_set(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component);
void *sm_ecs_component_get_mut(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component);
const void *sm_ecs_component_get(sm_ecs_world_s *world, sm_entity_s entity, sm_component_t component);

// ============================= Systems =============================
void sm_ecs_system_register(sm_ecs_world_s *world, system_f system);
void sm_ecs_do(sm_ecs_world_s *world, f32 dt);

// ============================= Iterator =============================
sm_system_iterator_s sm_ecs_iter_begin(sm_ecs_world_s *world, sm_component_t components);
b8 sm_ecs_iter_next(sm_ecs_world_s *world, sm_system_iterator_s *iter);
void *sm_ecs_iter_get_component_mut(sm_system_iterator_s *iter, sm_component_t component);
const void *sm_ecs_iter_get_component(sm_system_iterator_s *iter, sm_component_t component);

// ============================= Utility =============================
SM_STRING sm_ecs_entity_to_string(sm_ecs_world_s *world, sm_entity_s entity);

#endif /* SM_ECS_H */
