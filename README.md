![demo world 3D](world3D.gif "Demo world 3D")

An early-stage game engine for creating ps1-like games

### TODO
 - Refactoring & Documentation & Memory leaks fix
 - Rendering
 - Sound system

### Dependencies (only linux for now)
  - SDL2

### Build (only linux for now)
  ```sh
  cmake -S . -B build -D CMAKE_BUILD_TYPE=Debug -D BUILD_EXAMPLES=ON
  cmake --build build
  ```
### Credits

#### third party
  - glad
  - cimgui (imgui)
  - cglm
  - logc
  - stbi

#### thanks to
  - [Raylib](https://github.com/raysan5/raylib)
  - [sx](https://github.com/septag/sx)
  - [WickedEngine](https://github.com/turanszkij/WickedEngine)
