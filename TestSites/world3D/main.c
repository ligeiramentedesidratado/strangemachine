#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "cglm/vec3.h"
#include "core/smApplication.h"
#include "resource/smResource.h"
#include "smCollision.h"
#include "util/colors.h"

#define SM_DEBUG
#include "cimgui/smCimguiImpl.h"

#include "ecs/smComponents.h"
#include "ecs/smECS.h"

#include "math/smMath.h"
#include "renderer/smRenderer.h"

#include "smInput.h"
sm_intersect_result_s sm_resource_scene_intersects_capsule(sm_resource_scene_s *scene, sm_capsule_s capsule);

#define DESIRED_FPS 24.0f

typedef struct
{
    sm_resource_entity_s main_scene;
    struct sm_layer_s *layer;

} lab_s;

void on_attach(void *user_data)
{

    assert(user_data);

    lab_s *lab = (lab_s *)user_data;

    sm_resource_entity_s scene_entt = sm_resource_manager_get_by_name("Scene", SM_RESOURCE_TYPE_SCENE);
    lab->main_scene = sm_resource_scene_make_reference(scene_entt);
    sm_resource_scene_s *scene = sm_resource_manager_get_data(scene_entt);
    // sm_resource_manager_update_cpu( );

    sm_entity_s ett = sm_ecs_entity_new(scene->ecs, SM_CAMERA_COMP);
    u32 idx = sm_resource_scene_new_node(scene);
    sm_resource_scene_add_child(scene, SM_SCENE_ROOT_NODE, idx);
    sm_resource_scene_set_current_camera(scene, ett);

    sm_camera_s *camera = sm_ecs_component_set(scene->ecs, ett, SM_CAMERA_COMP);
    {
        camera->position = sm_vec3_new(0.0f, 10.0f, 10.0f);
        camera->target = sm_vec3_new(0.0f, 0.0f, 0.0f);
        camera->up = sm_vec3_new(0.0f, 1.0f, 0.0f);
        camera->move_speed = 3.0f;
        camera->sensitive = 0.3f;
        camera->target_distance = 3.0f;
        camera->fov = 75.0f;
        camera->angle = sm_vec3_new(0.0f, 0.0f, 0.0f);
        camera->aspect_ratio = 800.f / 600.f;
        camera->projection = SM_CAM_PROJ_PERSPECTIVE;
        camera->mode = SM_CAM_MODE_THIRD_PERSON;
    }

    sm_resource_scene_set_entity(scene, idx, ett);
    const char *camera_name = "camera";
    sm_resource_scene_set_name(scene, idx, camera_name);

    const SM_STRING player = "Player";
    u32 player_idx = sm_resource_scene_get_by_name(scene, player);
    if (player_idx != SM_SCENE_INVALID_NODE)
    {

        sm_entity_s player_ett = sm_resource_scene_get_entity(scene, player_idx);
        assert(SM_ENTITY_IS_VALID(player_ett));

        sm_ecs_entity_add_component(scene->ecs, player_ett, SM_RIGID_BODY_COMP);
        assert(SM_ENTITY_IS_VALID(player_ett));

        sm_rigid_body_s *player_rb = sm_ecs_component_get_mut(scene->ecs, player_ett, SM_RIGID_BODY_COMP);
        {
            sm_shape_u shape;
            sm_transform_s *transform = sm_ecs_component_get_mut(scene->ecs, player_ett, SM_TRANSFORM_COMP);

            player_rb->position.x = transform->position.x = 0.0f;
            player_rb->position.y = transform->position.y = 0.0f;
            player_rb->position.z = transform->position.z = 0.0f;
            player_rb->velocity = sm_vec3_zero( );
            player_rb->force = sm_vec3_zero( );
            player_rb->gravity = sm_vec3_zero( );

            shape.capsule = sm_shape_capsule_new((sm_sphere_s){.center = player_rb->position, .radius = 0.5f}, 1.25f);
            sm_rigid_body_component_set_shape(player_rb, shape, SM_RB_SHAPE_TYPE_CAPSULE);
        }

        const sm_res_s *resource = sm_ecs_component_get(scene->ecs, player_ett, SM_RESOURCE_COMP);
        {

            sm_resource_entity_s entt = sm_resource_manager_get_by_uuid(resource->uuid);
            if (entt.type == SM_RESOURCE_TYPE_MESH)
            {

                sm_resource_mesh_s *mesh = sm_resource_manager_get_data(entt);
                sm_resource_mesh_set_draw_aabb(mesh, false);
            }
        }
    }

    sm_system_iterator_s iter = sm_ecs_iter_begin(scene->ecs, SM_WORLD_COMP);
    while (sm_ecs_iter_next(scene->ecs, &iter))
    {
        sm_world_s *world = sm_ecs_iter_get_component_mut(&iter, SM_WORLD_COMP);
        world->prev_matrix = sm_mat4_identity( );
        world->matrix = sm_mat4_identity( );
    }

    // sm_entity_s player_entt = sm_ecs_entity_new(lab->ecs, SM_MESH_COMP | SM_RIGID_BODY_COMP);
    // u32 player_idx = sm_scene_new_node(lab->scene);
    // sm_scene_add_child(lab->scene, SM_SCENE_ROOT_NODE, player_idx);

    // sm_rigid_body_s *player_rb = sm_ecs_component_set(lab->ecs, player_entt, SM_RIGID_BODY_COMP);
    // {
    //   sm_shape_u shape;
    //   shape.capsule = shapes_capsule_new((sm_sphere_s){.center = sm_vec3_zero( ), .radius = 0.4f}, 1.7f);
    //   sm_rigid_body_component_set_shape(player_rb, shape, SM_RB_SHAPE_TYPE_CAPSULE);
    //   player_rb->position.y = 10.0f;
    // }

    // sm_scene_push_entity(lab->scene, player_idx, player_entt);
    // const char *player_name = "player";
    // sm_scene_set_name(lab->scene, player_idx, player_name);

    /* sm_ecs_system_register(lab->ecs, sm__rigid_body_do); */

    // Initialize viewport and internal projection/modelview matrices
}

void on_detach(void *user_data)
{

    assert(user_data);

    // lab_s *lab = (lab_s *)user_data;
    // sm_scene_dtor(lab->scene);
}

void collision_handle2(sm_resource_scene_s *scene, sm_rigid_body_s *rb, f32 dt)
{
    SM_ASSERT(physics != NULL);
    SM_ASSERT(against != NULL);

    glm_vec3_add(rb->force.data, rb->velocity.data, rb->velocity.data);
    glm_vec3_zero(rb->force.data);

    vec3 original_capsulepos;
    glm_vec3_copy(rb->position.data, original_capsulepos);
    sm_vec3 capsulepos;
    glm_vec3_copy(original_capsulepos, capsulepos.data);

    vec3 c;
    glm_vec3_sub(rb->shape.capsule.tip.data, rb->shape.capsule.base.data, c);
    float height = glm_vec3_norm(c);

    f32 radius = rb->shape.capsule.radius;
    bool ground_intersection = false;
    uint8_t const ccd_max = 5;

    for (uint8_t i = 0; i < ccd_max; ++i)
    {

        vec3 step;
        glm_vec3_scale(rb->velocity.data, 1.0f / ccd_max * (1.0f / DESIRED_FPS), step);
        glm_vec3_add(capsulepos.data, step, capsulepos.data);

        sm_vec3 tip;
        glm_vec3_add(capsulepos.data, sm_vec3_new(0.0f, height, 0.0f).data, tip.data);
        rb->shape.capsule = (sm_capsule_s){.base = capsulepos, .tip = tip, radius};

        sm_intersect_result_s result = sm_resource_scene_intersects_capsule(scene, rb->shape.capsule);

        if (result.valid)
        {

            vec3 r_norm;
            glm_vec3_copy(result.normal.data, r_norm);

            float velocityLen = glm_vec3_norm(rb->velocity.data);

            vec3 velocity_norm;
            glm_vec3_normalize_to(rb->velocity.data, velocity_norm);

            vec3 undesired_motion;
            glm_vec3_scale(r_norm, glm_vec3_dot(velocity_norm, r_norm), undesired_motion);

            vec3 desired_motion;
            glm_vec3_sub(velocity_norm, undesired_motion, desired_motion);

            glm_vec3_scale(desired_motion, velocityLen, rb->velocity.data);

            // Remove penetration (penetration epsilon added to handle infinitely
            // small penetration)
            vec3 penetration;
            glm_vec3_scale(r_norm, result.depth + 0.0001f, penetration);
            glm_vec3_add(capsulepos.data, penetration, capsulepos.data);
        }
    }

    // Gravity collision is separate:
    //	This is to avoid sliding down slopes and easier traversing of
    // slopes/stairs 	Unlike normal character motion collision, surface
    // sliding is not computed
    glm_vec3_add(rb->gravity.data, sm_vec3_new(0, -9.8f, 0).data, rb->gravity.data);

    for (uint8_t i = 0; i < ccd_max; ++i)
    {

        vec3 step;
        glm_vec3_scale(rb->gravity.data, 1.0f / ccd_max * 0.016f, step);
        glm_vec3_add(capsulepos.data, step, capsulepos.data);

        sm_vec3 tip;
        glm_vec3_add(capsulepos.data, sm_vec3_new(0.0f, height, 0.0f).data, tip.data);
        rb->shape.capsule = (sm_capsule_s){.base = capsulepos, .tip = tip, radius};

        sm_intersect_result_s result = sm_resource_scene_intersects_capsule(scene, rb->shape.capsule);

        if (result.valid)
        {

            // Remove penetration (penetration epsilon added to handle infinitely
            // small penetration):
            vec3 penetration;
            glm_vec3_scale(result.normal.data, result.depth + 0.0001f, penetration);
            glm_vec3_add(capsulepos.data, penetration, capsulepos.data);

            // Check whether it is intersecting the ground (ground normal is
            // upwards)
            if (glm_vec3_dot(result.normal.data, sm_vec3_new(0.0f, 1.0f, 0.0f).data) > 0.3f)
            {
                ground_intersection = true;
                glm_vec3_scale(rb->gravity.data, 0, rb->gravity.data);
                break;
            }
        }
    }

#define GROUND_FRICTION 0.5f
#define AIR_FRICTION    0.8f
    if (ground_intersection) glm_vec3_scale(rb->velocity.data, GROUND_FRICTION, rb->velocity.data);
    else glm_vec3_scale(rb->velocity.data, AIR_FRICTION, rb->velocity.data);

#undef GROUND_FRICTION
#undef AIR_FRICTION

    vec3 sub;
    glm_vec3_sub(capsulepos.data, original_capsulepos, sub);
    glm_vec3_add(rb->position.data, sub, rb->position.data);

    /* if (result.valid) { */
    /* debug_draw_line(result.position, vec3_add(result.position, result.normal), vec3_new(1, 0, 0)); */
    /* } */
}

void collision_handle(sm_resource_scene_s *scene, sm_rigid_body_s *rb, f32 dt)
{

    if (dt == 0.0f) return;

    glm_vec3_add(rb->force.data, sm_vec3_new(0.0f, -0.04f, 0.0f).data, rb->force.data);
    // glm_vec3_clamp(rb->force.data, -1.5f, 1.5f);
    glm_vec3_scale(rb->force.data, dt, rb->velocity.data);

    sm_vec3 original_capsulepos = rb->position;
    sm_vec3 capsulepos = original_capsulepos;

    sm_capsule_s capsule = rb->shape.capsule;

    sm_vec3 c;
    glm_vec3_sub(capsule.tip.data, capsule.base.data, c.data);
    f32 height = glm_vec3_norm(c.data);
    f32 radius = capsule.radius;
    b8 ground_intersect = false;

    f32 fixed_update_remain = dt;
    f32 fixed_update_fps = DESIRED_FPS;
    f32 fixed_update_rate = 1.0f / fixed_update_fps;
    f32 fixed_dt = fixed_update_rate / dt;

    while (fixed_update_remain > 0.0f)
    {

        fixed_update_remain = fixed_update_remain - fixed_update_rate;

        sm_vec3 step;
        glm_vec3_scale(rb->velocity.data, fixed_dt, step.data);

        glm_vec3_add(capsulepos.data, step.data, capsulepos.data);

        // sm_sphere_s circle = {.center = capsulepos, .radius = radius};
        // capsule = sm_shape_capsule_new(circle, height);

        sm_vec3 tip;
        glm_vec3_add(capsulepos.data, sm_vec3_new(0.0f, height, 0.0f).data, tip.data);
        capsule = (sm_capsule_s){.base = capsulepos, .tip = tip, radius};

        sm_intersect_result_s result = sm_resource_scene_intersects_capsule(scene, capsule);

        if (result.valid)
        {

            f32 ground_slope = glm_vec3_dot(result.normal.data, sm_vec3_up( ).data);
            f32 slope_threshold = 0.5f;

            if (rb->velocity.y < 0.0f && ground_slope > slope_threshold)
            {
                // Ground intersection, remove falling motion:
                rb->velocity.y = 0.0f;
                glm_vec3_add(capsulepos.data, sm_vec3_new(0.0f, result.depth, 0.0f).data, capsulepos.data);
                glm_vec3_add(capsulepos.data, result.velocity.data, capsulepos.data);
                glm_vec3_scale(rb->force.data, 0.85f, rb->force.data);
                ground_intersect = true;
            }
            else if (ground_slope <= slope_threshold)
            {
                f32 vel_len = glm_vec3_norm(result.velocity.data);
                sm_vec3 vel_n;
                glm_vec3_normalize_to(rb->velocity.data, vel_n.data);

                f32 dot = glm_vec3_dot(vel_n.data, result.normal.data);
                sm_vec3 undesired_motion;
                glm_vec3_scale(result.normal.data, dot, undesired_motion.data);

                sm_vec3 desired_motion;
                glm_vec3_sub(vel_n.data, undesired_motion.data, desired_motion.data);

                if (ground_intersect)
                {
                    desired_motion.y = 0.0f;
                    // glm_vec3_scale(desiredMotion.data, 0.3f, desiredMotion.data);
                }

                glm_vec3_scale(desired_motion.data, vel_len, rb->velocity.data);

                sm_vec3 nd;
                glm_vec3_scale(result.normal.data, result.depth, nd.data);
                glm_vec3_add(capsulepos.data, nd.data, capsulepos.data);
            }
        }

        sm_vec3 sub;
        glm_vec3_sub(capsulepos.data, original_capsulepos.data, sub.data);
        glm_vec3_add(rb->position.data, sub.data, rb->position.data);
    }

#define GROUND_FRICTION 0.5f
#define AIR_FRICTION    0.8f
    if (ground_intersect)
    {
        glm_vec3_scale(rb->velocity.data, GROUND_FRICTION, rb->velocity.data);
    }
    else
    {
        glm_vec3_scale(rb->velocity.data, AIR_FRICTION, rb->velocity.data);
    }

#undef GROUND_FRICTION
#undef AIR_FRICTION

    if (input_scan_key(sm_key_f))
    {
        rb->position = (sm_vec3){0.0f, 5.0f, 0.0f};
        rb->force = sm_vec3_zero( );
        rb->velocity = sm_vec3_zero( );
    }
}

void on_update(void *user_data, f32 dt)
{

    assert(user_data);
    (void)dt;
    if (dt == 0.0f) return;

    lab_s *lab = (lab_s *)user_data;

    sm_resource_scene_s *scene = sm_resource_manager_get_data(lab->main_scene);

    sm_entity_s cam_entt = sm_resource_scene_get_current_camera(scene);
    sm_camera_s *main_camera = sm_ecs_component_get_mut(scene->ecs, cam_entt, SM_CAMERA_COMP);

    const SM_STRING player = "Player";
    u32 player_idx = sm_resource_scene_get_by_name(scene, player);
    sm_entity_s player_ett = {SM_INVALID_HANDLE};
    if (player_idx != SM_SCENE_INVALID_NODE)
    {

        player_ett = sm_resource_scene_get_entity(scene, player_idx);

        assert(sm_ecs_entity_is_valid(scene->ecs, player_ett));
        sm_transform_s *transform = sm_ecs_component_get_mut(scene->ecs, player_ett, SM_TRANSFORM_COMP);
        sm_rigid_body_s *player_rb = sm_ecs_component_get_mut(scene->ecs, player_ett, SM_RIGID_BODY_COMP);
        {

            // Get the left/right/forward/backward.
            // They are mapped to the A,S,D,W keys.
            // The keyboard value will always be -1, 0 or 1
            sm_vec3 input = sm_vec3_new(
                (float)(input_scan_key(sm_key_a) + -input_scan_key(sm_key_d)), 0.f, (float)(input_scan_key(sm_key_w) + -input_scan_key(sm_key_s)));

            sm_vec3 direction;
            glm_vec3_normalize_to(input.data, direction.data);
            float dir_len = glm_vec3_norm(direction.data);
            if (dir_len != 0.0f)
            {

                sm_mat4 view;
                sm_camera_component_get_view(main_camera, view.data);

                sm_vec3 fwd;
                fwd.data[0] = view.data[2][0];
                fwd.data[1] = 0.0f;
                fwd.data[2] = -view.data[2][2];
                sm_vec3 right;
                right.data[0] = -view.data[0][0];
                right.data[1] = 0.0f;
                right.data[2] = view.data[0][2];

                glm_vec3_scale(fwd.data, direction.z, fwd.data);
                glm_vec3_scale(right.data, direction.x, right.data);

                glm_vec3_add(fwd.data, right.data, direction.data);

                glm_vec3_normalize(direction.data);
                float yaw = atan2f(direction.x, direction.z);

                sm_vec4 rotation;

                glm_quatv(rotation.data, yaw, sm_vec3_new(0.0f, 1.0f, 0.0f).data);
                glm_quat_slerp(transform->rotation.data, rotation.data, 15 * dt, transform->rotation.data);
            }

#define SPEED 45.2f
            f32 sprint = input_scan_key(sm_key_lshift) ? 1.5f : 1.00f;

            glm_vec3_scale(direction.data, sprint * SPEED * dt, direction.data);

#undef SPEED

            glm_vec3_add(player_rb->force.data, direction.data, player_rb->force.data);

            collision_handle2(scene, player_rb, dt);

            glm_vec4_copy(sm_vec4_new(player_rb->position.x, player_rb->position.y, player_rb->position.z, 0.0f).data, transform->position.data);

            sm_capsule_s capsule = player_rb->shape.capsule;
            sm_vec3 c;
            glm_vec3_sub(capsule.tip.data, capsule.base.data, c.data);
            f32 height = glm_vec3_norm(c.data);

            sm_vec4 target = transform->position;
            target.y += height + capsule.radius + (0.5f * capsule.radius);

            sm_camera_component_set_target(main_camera, target.data);
        }
        sm_transform_component_set_dirty(transform, true);
    }

    // sm_rigid_body_s *player_rb = sm_ecs_component_get_mut(lab->ecs, player_ett, SM_RIGID_BODY_COMP);
    //   {

    //     if (input_scan_key(sm_key_up))
    //       glm_vec3_add(player_rb->force.data, sm_vec3_new(0.0f, 0.0f, 0.3f).data, player_rb->force.data);

    //     if (input_scan_key(sm_key_down))
    //       glm_vec3_add(player_rb->force.data, sm_vec3_new(0.0f, 0.0f, -0.3f).data, player_rb->force.data);

    //     if (input_scan_key(sm_key_left))
    //       glm_vec3_add(player_rb->force.data, sm_vec3_new(0.3f, 0.0f, 0.0f).data, player_rb->force.data);

    //     if (input_scan_key(sm_key_right))
    //       glm_vec3_add(player_rb->force.data, sm_vec3_new(-0.3f, 0.0f, 0.0f).data, player_rb->force.data);

    //     if (input_scan_key(sm_key_lshift))
    //       glm_vec3_add(player_rb->force.data, sm_vec3_new(0.0f, -0.3f, 0.0f).data, player_rb->force.data);

    //     sm_transform_s *player_transform = sm_ecs_component_get_mut(lab->ecs, player_ett, SM_TRANSFORM_COMP);
    //     {
    //       collision_handle(lab->scene, lab->ecs, player_rb, dt);
    //       glm_vec4_add(player_transform->position.data,
    //                    sm_vec4_new(player_rb->position.x, player_rb->position.y, player_rb->position.z, 0.0f).data,
    //                    player_transform->position.data);
    //       sm_transform_component_set_dirty(player_transform, true);
    //       sm_transform_component_apply_transform(player_transform);
    //     }

    //     glm_vec3_copy(player_rb->position.data, sphere_position.data);
    //   }

    //   sm_mesh_s *player_mesh = sm_ecs_component_get_mut(lab->ecs, player_ett, SM_MESH_COMP);
    //   {
    //     player_aabb = sm_mesh_component_get_aabb(player_mesh);
    //     glm_vec3_add(sphere_position.data, player_aabb.min.data, player_aabb.min.data);
    //     glm_vec3_add(sphere_position.data, player_aabb.max.data, player_aabb.max.data);
    //   }
    // }

    sm_draw_begin( );
    {
        sm_renderer_clear(sm_vec4_new(0.1f, 0.1f, 0.1f, 1.0f));

        sm_renderer2_begin_3d(main_camera);
        {

            sm_gl_set_line_width(4.0f);
            sm_renderer2_draw_line_3D(sm_vec3_zero( ), sm_vec3_new(1.0f, 0.0f, 0.0f), sm_vec4_new(1.0f, 0.0f, 0.0f, 1.0f));
            sm_renderer2_draw_line_3D(sm_vec3_zero( ), sm_vec3_new(0.0f, 1.0f, 0.0f), sm_vec4_new(0.0f, 1.0f, 0.0f, 1.0f));
            sm_renderer2_draw_line_3D(sm_vec3_zero( ), sm_vec3_new(0.0f, 0.0f, 1.0f), sm_vec4_new(0.0f, 0.0f, 1.0f, 1.0f));

            sm_renderer_draw_scene(scene);
            sm_renderer2_draws_phere(sm_vec3_new(0.0f, 4.0f, 0.0f), 1.0f, 16, 16, PURPLE);
            // sm_renderer2_draw_grid(10, 1.0f);
        }
        sm_renderer2_end_3d( );

        sm_renderer2_draw_rectangle(sm_vec2_new(10.0f, 10.0f), sm_vec2_new(45, 10), sm_vec4_new(0.5f, 0.9f, 0.2f, 1.0f)); // green br
        // sm_renderer2_draw_circle(sm_vec2_new(800.0f / 2, 600 / 2.0f), 100.0f, 0, 360, 36, sm_vec4_new(0.0f, 0.4f, 0.7f, 1.0f));
    }
    sm_draw_end( );
}

bool on_event(sm_event_s *event, void *user_data)
{

    assert(user_data);

    lab_s *lab = (lab_s *)user_data;
    (void)lab;

    switch (event->category)
    {
    case SM_CATEGORY_WINDOW:
        if (event->window.type == SM_EVENT_WINDOW_RESIZE)
        {

            sm_resource_scene_s *scene = sm_resource_manager_get_data(lab->main_scene);

            sm_entity_s cam_ett = sm_resource_scene_get_current_camera(scene);
            const sm_camera_s *cam = sm_ecs_component_get(scene->ecs, cam_ett, SM_CAMERA_COMP);
            sm_camera_component_set_aspect_ratio((sm_camera_s *)cam, (f32)event->window.width / (f32)event->window.height);
            return true;
        }
    default: break;
    }
    return false;
}

void for_each_node(sm_resource_scene_s *scene, u32 node_index, void *user_data)
{

    SM_UNUSED(user_data);
    const char *name = sm_resource_scene_get_name(scene, node_index);
    /* sm_entity_s e = sm_scene_get_entity(scene, node_index); */
    igBulletText("%u, %s", node_index, name);
}

void on_gui(void *user_data)
{

    assert(user_data);

    lab_s *lab = (lab_s *)user_data;
    (void)lab;

    static bool show_window = false;

    if (show_window) igShowDemoWindow(&show_window);

    /* igShowDemoWindow(&show_window); */

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings |
                                    ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
    igBegin("Nodes", &show_window, window_flags);

    if (igTreeNode_Str("Nodes"))
    {
        sm_resource_scene_s *scene = sm_resource_manager_get_data(lab->main_scene);
        sm_resource_scene_for_each(scene, 0, for_each_node, NULL);
        igTreePop( );
    }

    igEnd( );
}

int main(void)
{

#if SM_PLATFORM_MOBILE
    printf("SM_PLATFORM_MOBILE\n");
#endif

#if SM_PLATFORM_DESKTOP
    printf("SM_PLATFORM_DESKTOP\n");
#endif

#if SM_PLATFORM_ANDROID
    printf("SM_PLATFORM_DESKTOP\n");
#endif
#if SM_PLATFORM_BSD
    printf("SM_PLATFORM_BSD\n");
#endif
#if SM_PLATFORM_EMSCRIPTEN
    printf("SM_PLATFORM_EMSCRIPTEN\n");
#endif
#if SM_PLATFORM_HURD
    printf("SM_PLATFORM_HURD\n");
#endif
#if SM_PLATFORM_IOS
    printf("SM_PLATFORM_IOS\n");
#endif
#if SM_PLATFORM_LINUX
    printf("SM_PLATFORM_LINUX\n");
#endif
#if SM_PLATFORM_NX
    printf("SM_PLATFORM_NX\n");
#endif
#if SM_PLATFORM_OSX
    printf("SM_PLATFORM_OSX\n");
#endif
#if SM_PLATFORM_PS4
    printf("SM_PLATFORM_PS4\n");
#endif
#if SM_PLATFORM_RPI
    printf("SM_PLATFORM_RPI\n");
#endif
#if SM_PLATFORM_WINDOWS
    printf("SM_PLATFORM_WINDOWS\n");
#endif
#if SM_PLATFORM_WINRT
    printf("SM_PLATFORM_WINRT\n");
#endif
#if SM_PLATFORM_XBOXONE
    printf("SM_PLATFORM_XBOXONE\n");
#endif

    struct sm__application_s *app = sm_application_new( );
    if (!sm_application_ctor(app, "lab"))
    {
        exit(EXIT_FAILURE);
    }

    sm_application_set_desired_fps(app, DESIRED_FPS);

    lab_s *lab = calloc(1, sizeof(lab_s));

    struct sm_layer_s *layer = sm_layer_new( );
    if (!sm_layer_ctor(layer, "entity", lab, on_attach, on_detach, on_update, on_gui, on_event))
    {
        exit(EXIT_FAILURE);
    }
    lab->layer = layer;

    sm_application_push_layer(app, layer);

    sm_application_do(app);

    sm_application_dtor(app);
    sm_layer_dtor(layer);
    free(lab);

    return EXIT_SUCCESS;
}
