#include <stdio.h>

#include "smConversorUtil.h"

#include "core/smCore.h"

b8 conversor_images_dtor_cb(const SM_STRING key, void *value, void *user_data)
{

    SM_UNUSED(user_data);

    SM_STRING_FREE((SM_STRING)key);
    SM_STRING_FREE(value);

    return true;
}

void conversor_print_material_info(const struct aiScene *scene)
{

    for (size_t i = 0; i < scene->mNumMaterials; ++i)
    {

        struct aiMaterial *mat = scene->mMaterials[i];

        struct aiString name;
        aiReturn ret = aiGetMaterialString(mat, AI_MATKEY_NAME, &name);
        /* if (ret == AI_SUCCESS) printf("Material name: %s\n", name.data); */

        printf("====================MAT BEG====================\n");
        printf("Material [%zu] %s\n", i, (ret == AI_SUCCESS) ? name.data : "no name");

        unsigned int count = 0;
        printf("\taiTextureType_\n");
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_DIFFUSE))) printf("\t\tDIFFUSE           %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_DIFFUSE_ROUGHNESS))) printf("\t\tDIFFUSE_ROUGHNESS %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_AMBIENT))) printf("\t\tAMBIENT           %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_AMBIENT_OCCLUSION))) printf("\t\tAMBIENT_OCCLUSION %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_BASE_COLOR))) printf("\t\tBASE_COLOR        %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_EMISSION_COLOR))) printf("\t\tEMISSION_COLOR    %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_CLEARCOAT))) printf("\t\tCLEARCOAT         %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_DISPLACEMENT))) printf("\t\tDISPLACEMENT      %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_EMISSIVE))) printf("\t\tEMISSIVE          %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_HEIGHT))) printf("\t\tHEIGHT            %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_LIGHTMAP))) printf("\t\tLIGHTMAP          %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_METALNESS))) printf("\t\tMETALNESS         %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_NONE))) printf("\t\tNONE              %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_NORMAL_CAMERA))) printf("\t\tNORMAL_CAMERA     %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_NORMALS))) printf("\t\tNORMALS           %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_OPACITY))) printf("\t\tOPACITY           %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_REFLECTION))) printf("\t\tREFLECTION        %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_SHEEN))) printf("\t\tSHEEN             %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_SHININESS))) printf("\t\tSHININESS         %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_SPECULAR))) printf("\t\tSPECULAR          %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_TRANSMISSION))) printf("\t\tTRANSMISSION      %d\n", count);
        if ((count = aiGetMaterialTextureCount(mat, aiTextureType_UNKNOWN))) printf("\t\tUNKNOWN           %d\n", count);

        struct aiColor4D color;
        printf("\tAI_MATKEY_\n");
        if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color) == AI_SUCCESS)
            printf("\t\tCOLOR_AMBIENT     (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
        if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color) == AI_SUCCESS)
            printf("\t\tCOLOR_DIFFUSE     (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
        if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_EMISSIVE, &color) == AI_SUCCESS)
            printf("\t\tCOLOR_EMISSIVE    (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
        if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_REFLECTIVE, &color) == AI_SUCCESS)
            printf("\t\tCOLOR_REFLECTIVE  (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
        if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color) == AI_SUCCESS)
            printf("\t\tCOLOR_SPECULAR    (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
        if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_TRANSPARENT, &color) == AI_SUCCESS)
            printf("\t\tCOLOR_TRANSPARENT (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);

        struct aiString alpha;
        if (aiGetMaterialString(mat, "$mat.gltf.alphaMode", 0, 0, &alpha) == AI_SUCCESS) printf("\t\talphaMode: %s\n", alpha.data);
        if (aiGetMaterialString(mat, "$mat.gltf.alphaCutoff", 0, 0, &alpha) == AI_SUCCESS) printf("\t\talphaCutoff: %s\n", alpha.data);

        int property = 0;
        if (aiGetMaterialInteger(mat, AI_MATKEY_TWOSIDED, &property) == AI_SUCCESS) printf("\t\tTWOSIDED          %s\n", property ? "true" : "false");
        if (aiGetMaterialInteger(mat, AI_MATKEY_BLEND_FUNC, &property) == AI_SUCCESS) printf("\t\tBLEND_FUNC        %s\n", property ? "true" : "false");

        float property2 = 0.0f;
        if (aiGetMaterialFloat(mat, AI_MATKEY_SHININESS, &property2) == AI_SUCCESS) printf("\t\tSHININESS         %f\n", property2);
        if (aiGetMaterialFloat(mat, AI_MATKEY_SHININESS_STRENGTH, &property2) == AI_SUCCESS) printf("\t\tSHININESS_STRENG  %f\n", property2);
        if (aiGetMaterialFloat(mat, AI_MATKEY_OPACITY, &property2) == AI_SUCCESS) printf("\t\tOPACITY           %f\n", property2);

        printf("====================MAT END====================\n\n");
    }
}
