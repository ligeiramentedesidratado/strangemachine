#ifndef SM_CONVERSOR_UTIL_H
#define SM_CONVERSOR_UTIL_H

#include <assimp/material.h>
#include <assimp/scene.h>

#include "core/smCore.h"

#define SMC_MAT4_ASSIMP_TO_SM(SM_MAT4, AI_MAT4)                                                                               \
  SM_MAT4.data[0][0] = AI_MAT4.a1;                                                                                            \
  SM_MAT4.data[0][1] = AI_MAT4.b1;                                                                                            \
  SM_MAT4.data[0][2] = AI_MAT4.c1;                                                                                            \
  SM_MAT4.data[0][3] = AI_MAT4.d1;                                                                                            \
  SM_MAT4.data[1][0] = AI_MAT4.a2;                                                                                            \
  SM_MAT4.data[1][1] = AI_MAT4.b2;                                                                                            \
  SM_MAT4.data[1][2] = AI_MAT4.c2;                                                                                            \
  SM_MAT4.data[1][3] = AI_MAT4.d2;                                                                                            \
  SM_MAT4.data[2][0] = AI_MAT4.a3;                                                                                            \
  SM_MAT4.data[2][1] = AI_MAT4.b3;                                                                                            \
  SM_MAT4.data[2][2] = AI_MAT4.c3;                                                                                            \
  SM_MAT4.data[2][3] = AI_MAT4.d3;                                                                                            \
  SM_MAT4.data[3][0] = AI_MAT4.a4;                                                                                            \
  SM_MAT4.data[3][1] = AI_MAT4.b4;                                                                                            \
  SM_MAT4.data[3][2] = AI_MAT4.c4;                                                                                            \
  SM_MAT4.data[3][3] = AI_MAT4.d4;

void conversor_print_material_info(const struct aiScene *scene);
b8 conversor_images_dtor_cb(const SM_STRING key, void *value, void *user_data);

#endif /* SM_CONVERSOR_UTIL_H */
