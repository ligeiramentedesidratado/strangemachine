#include "smpch.h"

#include "ecs/smECS.h"
#include "resource/smResource.h"

#include "smConversorUtil.h"

#include <assimp/cimport.h>
#include <assimp/color4.h>
#include <assimp/material.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/texture.h>
#include <assimp/version.h>
#include <string.h>

#include "core/data/smHashMap.h"
#include "core/smCore.h"
#include "core/util/smBitMask.h"
#include "core/util/smString.h"

#include "math/smMath.h"
#include "util/colors.h"

#define STBI_MALLOC(sz)        SM_MALLOC(sz)
#define STBI_REALLOC(p, newsz) SM_REALLOC(p, newsz)
#define STBI_FREE(p)           SM_FREE(p)
#define STBI_ASSERT(x)         SM_ASSERT(x)
#define STB_IMAGE_IMPLEMENTATION
#include "vendor/stbi/stb_image.h"

// #define STB_IMAGE_WRITE_IMPLEMENTATION
// #include "vendor/stbi/stb_image_write.h"

typedef struct sm__user_data_s
{
    sm_resource_scene_s *scene;
    sm_ecs_world_s *ecs;
    u32 node;

} sm_user_data;

SM_ARRAY(sm_resource_entity_s) materials = NULL;
sm_hashmap_str_m *images = NULL;

SM_STRING file_path = {0};
SM_STRING filename = {0};
SM_STRING path = {0};

void conversor_process_node(struct aiNode *node, const struct aiScene *scene, sm_user_data u_data);
sm_entity_s conversor_load_mesh(const struct aiScene *ai_scene, const struct aiMesh *ai_mesh, struct aiMatrix4x4 ai_mat4, sm_user_data u_data);

void conversor_print_cb(sm_resource_scene_s *scene, u32 index, void *user_data)
{

    SM_UNUSED(user_data);

    sm_entity_s e = sm_resource_scene_get_entity(scene, index);
    const SM_STRING name = sm_resource_scene_get_name(scene, index);
    printf("%d=>%s\n", index, name);

    if (!sm_ecs_entity_is_valid(scene->ecs, e))
    {
        return;
    }

    SM_STRING s = sm_ecs_entity_to_string(user_data, e);
    sm_handle_t handle = sm_ecs_entity_get_handle(user_data, e);
    sm_component_t archetype = sm_ecs_entity_get_archetype(user_data, e);
    printf("\t->parent: %d, entity: %d, %lu(%s)\n", sm_resource_scene_get_parent(scene, index), handle, archetype, s);
    SM_STRING_FREE(s);

    if (sm_ecs_entity_has_component(user_data, e, SM_RESOURCE_COMP))
    {
        const sm_res_s *res = sm_ecs_component_get(user_data, e, SM_RESOURCE_COMP);
        {
            sm_resource_entity_s mesh_ett = sm_resource_manager_get_by_uuid(res->uuid);
            switch (mesh_ett.type)
            {
            case SM_RESOURCE_TYPE_MESH:
                {
                    sm_resource_mesh_s *mesh = sm_resource_manager_get_data(mesh_ett);
                    printf("\t\t->mesh uuid: %u ", mesh->header.uuid);

                    if (mesh->material != 0)
                    {
                        sm_resource_entity_s material_ett = sm_resource_manager_get_by_uuid(mesh->material);
                        sm_resource_material_s *material = sm_resource_manager_get_data(material_ett);
                        printf(" | material uuid: %u", material->header.uuid);

                        if (material->diffuse_map != 0)
                        {
                            sm_resource_entity_s texture_ett = sm_resource_manager_get_by_uuid(material->diffuse_map);
                            sm_resource_texture_s *texture = sm_resource_manager_get_data(texture_ett);
                            printf(" | texture uuid: %u", texture->header.uuid);
                        }
                    }
                    printf(" | refs: %u\n", sm_rc_load(&mesh->ref_count));
                    break;
                }
            default: SM_LOG_WARN("not handled!");
            }
        }
    }
}

void conversor_process_node(struct aiNode *ai_node, const struct aiScene *ai_scene, sm_user_data u_data)
{

    struct aiMatrix4x4 ai_mat4 = ai_node->mTransformation;
    sm_resource_scene_set_name(u_data.scene, u_data.node, (ai_node->mName.length > 0) ? ai_node->mName.data : "DEFAULT NODE NAME");

    /* process each mesh located at the current node */
    assert(ai_node->mNumMeshes <= 1);
    for (u32 i = 0; i < ai_node->mNumMeshes; ++i)
    {

        /* the node object only contains indices to index the actual objects in the scene. */
        /* the scene contains all the data, node is just to keep stuff organized (like relations between nodes). */
        struct aiMesh *ai_mesh = ai_scene->mMeshes[ai_node->mMeshes[i]];

        sm_entity_s entt = conversor_load_mesh(ai_scene, ai_mesh, ai_mat4, u_data);
        sm_resource_scene_set_entity(u_data.scene, u_data.node, entt);
    }

    if (ai_node->mNumMeshes == 0)
    {
        sm_entity_s entity = sm_ecs_entity_new(u_data.ecs, SM_TRANSFORM_COMP | SM_WORLD_COMP);

        // take the transform component
        sm_transform_s *transform_component = sm_ecs_component_get_mut(u_data.ecs, entity, SM_TRANSFORM_COMP);
        {
            sm_mat4 mat;
            SMC_MAT4_ASSIMP_TO_SM(mat, ai_mat4);

            *transform_component = sm_transform_component_from_mat4(mat);
        }
        sm_resource_scene_set_entity(u_data.scene, u_data.node, entity);
    }

    /* after we've processed all of the meshes (if any) we then recursively process each of the children nodes */
    for (u32 i = 0; i < ai_node->mNumChildren; ++i)
    {
        u32 child_node = sm_resource_scene_new_node(u_data.scene);
        sm_resource_scene_add_child(u_data.scene, u_data.node, child_node);

        sm_user_data u_data_child = u_data;
        u_data_child.node = child_node;

        conversor_process_node(ai_node->mChildren[i], ai_scene, u_data_child);
    }
}

sm_entity_s conversor_load_mesh(const struct aiScene *ai_scene, const struct aiMesh *ai_mesh, struct aiMatrix4x4 ai_mat4, sm_user_data u_data)
{

    sm_entity_s entity = sm_ecs_entity_new(u_data.ecs, SM_TRANSFORM_COMP | SM_WORLD_COMP | SM_RESOURCE_COMP);

    // take the transform component
    sm_transform_s *transform_component = sm_ecs_component_get_mut(u_data.ecs, entity, SM_TRANSFORM_COMP);
    {
        sm_mat4 mat;
        SMC_MAT4_ASSIMP_TO_SM(mat, ai_mat4);

        *transform_component = sm_transform_component_from_mat4(mat);
    }

    sm_resource_mesh_s sm_mesh = {0};

    sm_rc_store(&sm_mesh.ref_count, 1);
    u32 uuid = (u32)(rand( ) + 1);

    i8 sm_mesh_name[256];
    if (ai_mesh->mName.length)
    {
        strncpy(sm_mesh_name, ai_mesh->mName.data, 256);
    }
    else
    {
        sprintf(sm_mesh_name, "mesh%u", uuid);
    }

    sm_mesh.header = sm_resource_header_new(SM_RESOURCE_TYPE_MESH, uuid, sm_mesh_name);

    if (ai_mesh->mVertices != NULL && ai_mesh->mNumVertices > 0)
    {
        SM_ARRAY_SET_LEN(sm_mesh.positions, ai_mesh->mNumVertices);
        memcpy(sm_mesh.positions, ai_mesh->mVertices, ai_mesh->mNumVertices * sizeof(float) * 3);
    }

    if (ai_mesh->mNormals != NULL && ai_mesh->mNumVertices > 0)
    {
        SM_ARRAY_SET_LEN(sm_mesh.normals, ai_mesh->mNumVertices);
        memcpy(sm_mesh.normals, ai_mesh->mNormals, ai_mesh->mNumVertices * sizeof(float) * 3);
    }

    if (ai_mesh->mTextureCoords[0] != NULL && ai_mesh->mNumVertices > 0)
    {
        SM_ARRAY_SET_LEN(sm_mesh.uvs, ai_mesh->mNumVertices);
        for (u32 uv = 0; uv < ai_mesh->mNumVertices; ++uv)
        {
            sm_mesh.uvs[uv].x = ai_mesh->mTextureCoords[0][uv].x;
            sm_mesh.uvs[uv].y = ai_mesh->mTextureCoords[0][uv].y;
        }
    }
    else
    {
        SM_ARRAY_SET_LEN(sm_mesh.uvs, ai_mesh->mNumVertices);
        for (u32 uv = 0; uv < ai_mesh->mNumVertices; ++uv)
        {
            sm_mesh.uvs[uv].x = 1.0f;
            sm_mesh.uvs[uv].y = 1.0f;
        }
    }

    /* get the transformation matrix */

    if (ai_mesh->mColors[0] != NULL && ai_mesh->mNumVertices > 0)
    {
        /* SM_ARRAY_SET_LEN(mesh.colors, ai_mesh->mNumVertices); */
        SM_ALIGNED_ARRAY_NEW(sm_mesh.colors, 16, ai_mesh->mNumVertices);
        for (u32 color = 0; color < ai_mesh->mNumVertices; ++color)
        {
            sm_mesh.colors[color].r = ai_mesh->mColors[0][color].r;
            sm_mesh.colors[color].g = ai_mesh->mColors[0][color].g;
            sm_mesh.colors[color].b = ai_mesh->mColors[0][color].b;
            sm_mesh.colors[color].a = ai_mesh->mColors[0][color].a;
        }
    }
    else
    {
        SM_ARRAY_SET_LEN(sm_mesh.colors, ai_mesh->mNumVertices);
        for (u32 color = 0; color < ai_mesh->mNumVertices; ++color)
        {
            sm_mesh.colors[color] = WHITE;
        }
    }

    /* load indices */
    if (ai_mesh->mFaces != NULL && ai_mesh->mNumFaces > 0)
    {
        for (u32 i = 0; i < ai_mesh->mNumFaces; ++i)
        {
            struct aiFace face = ai_mesh->mFaces[i];
            for (u32 j = 0; j < face.mNumIndices; ++j)
            {
                SM_ARRAY_PUSH(sm_mesh.indices, face.mIndices[j]);
            }
        }
    }

    sm_resource_mesh_set_renderable(&sm_mesh, true);
    sm_resource_mesh_set_on_cpu(&sm_mesh, true);
    sm_mesh.material = sm_resource_manager_get_uuid(materials[ai_mesh->mMaterialIndex]);
    sm_resource_material_make_reference(materials[ai_mesh->mMaterialIndex]);

    sm_resource_entity_s mesh_entt = sm___resource_mock_push(&sm_mesh, SM_RESOURCE_TYPE_MESH);

    sm_res_s *resource = sm_ecs_component_set(u_data.ecs, entity, SM_RESOURCE_COMP);
    {
        resource->uuid = sm_resource_manager_get_uuid(mesh_entt);
        sm_resource_mesh_make_reference(mesh_entt);
    }

    return entity;
}

void print_materials(struct aiNode *ai_node, const struct aiScene *ai_scene)
{

    /* process each mesh located at the current node */
    assert(ai_node->mNumMeshes <= 1);
    for (u32 i = 0; i < ai_node->mNumMeshes; ++i)
    {

        /* the node object only contains indices to index the actual objects in the scene. */
        /* the scene contains all the data, node is just to keep stuff organized (like relations between nodes). */
        struct aiMesh *ai_mesh = ai_scene->mMeshes[ai_node->mMeshes[i]];

        printf("mat: %s, index: %d\n", ai_mesh->mName.data, ai_mesh->mMaterialIndex);
    }

    /* after we've processed all of the meshes (if any) we then recursively process each of the children nodes */
    for (u32 i = 0; i < ai_node->mNumChildren; ++i)
    {

        print_materials(ai_node->mChildren[i], ai_scene);
    }
}

u32 conversor_load_textures(const struct aiScene *ai_scene, const struct aiMaterial *ai_material)
{

    SM_ASSERT(ai_material);

    struct aiString ai_texture_name;
    sm_resource_entity_s sm_texture_entt = SM_RESOURCE_INVALID_ENTITY;

    if (aiGetMaterialTextureCount(ai_material, aiTextureType_DIFFUSE) > 0 &&
        aiGetMaterialTexture(ai_material, aiTextureType_DIFFUSE, 0, &ai_texture_name, NULL, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
    {

        SM_STRING value = sm_hashmap_get_str(images, ai_texture_name.data);
        if (!value)
        {

            sm_resource_texture_s sm_texture = {0};
            u32 channels = 0;
            u32 texture_uuid = (u32)(rand( ) + 1);

            if (ai_texture_name.data[0] == '*')
            {
                i32 index = atoi(&ai_texture_name.data[1]);
                struct aiTexture *tex = ai_scene->mTextures[index];

                if (tex->mHeight == 0)
                {
                    sm_texture.raw_data = stbi_load_from_memory(
                        (void *)tex->pcData, (i32)tex->mWidth, (i32 *)&sm_texture.width, (i32 *)&sm_texture.height, (i32 *)&channels, 0);
                }

                if (!sm_texture.raw_data)
                {
                    printf("[%d] invalid texture data\n", index);
                    exit(1);
                }
            }
            else
            {
                char buf[256];
                strcpy(buf, path);
                strcat(buf, ai_texture_name.data);
                sm_texture.raw_data = stbi_load(buf, (i32 *)&sm_texture.width, (i32 *)&sm_texture.height, (i32 *)&channels, 0);

                if (!sm_texture.raw_data)
                {
                    printf("[%s] invalid texture data\n", buf);
                    exit(1);
                }
            }

            char sm_texture_name[256];
            sprintf(sm_texture_name, "diffuse_map%u", texture_uuid);

            sm_texture.header = sm_resource_header_new(SM_RESOURCE_TYPE_TEXTURE, texture_uuid, sm_texture_name);
            sm_rc_store(&sm_texture.ref_count, 1);

            if (channels == 1) sm_texture.format = SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE;
            else if (channels == 2) sm_texture.format = SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA;
            else if (channels == 3) sm_texture.format = SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8;
            else if (channels == 4) sm_texture.format = SM_TEXTURE_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;

            sm_resource_texture_set_on_cpu(&sm_texture, true);

            SM_ASSERT(!sm_hashmap_put_str(images, SM_STRING_DUP(ai_texture_name.data), SM_STRING_DUP(sm_texture_name)));

            printf("Texture stbi [%s](%d) (%dx%d) | %d\n", sm_texture.header.name, sm_texture.header.uuid, sm_texture.width, sm_texture.height, channels);

            sm_texture_entt = sm___resource_mock_push(&sm_texture, SM_RESOURCE_TYPE_TEXTURE);
        }
        else
        {
            sm_texture_entt = sm_resource_manager_get_by_name(value, SM_RESOURCE_TYPE_TEXTURE);
        }

        sm_resource_texture_make_reference(sm_texture_entt);
    }

    return sm_resource_manager_get_uuid(sm_texture_entt);
}

sm_resource_entity_s conversor_load_default_material( )
{

    sm_resource_material_s sm_material = {0};

    sm_material.header = sm_resource_header_new(SM_RESOURCE_TYPE_MATERIAL, (u32)(rand( ) + 1), "defaultmaterial");

    sm_rc_store(&sm_material.ref_count, 1);
    sm_material.diffuse_map = 0u;
    glm_vec4_copy(GLM_VEC4_ONE, sm_material.diffuse_color.data);
    sm_resource_material_set_twosided(&sm_material, false);

    sm_resource_entity_s material_entt = sm___resource_mock_push(&sm_material, SM_RESOURCE_TYPE_MATERIAL);

    return material_entt;
}

void conversor_load_materials(const struct aiScene *ai_scene)
{

    images = sm_hashmap_new_str( );
    if (!sm_hashmap_ctor_str(images, 16, NULL, NULL))
    {
        printf("failed to create hashmap");
        exit(1);
    }

    sm_resource_entity_s default_material = conversor_load_default_material( );

    for (size_t i = 0; i < ai_scene->mNumMaterials; ++i)
    {

        struct aiMaterial *ai_material = ai_scene->mMaterials[i];

        struct aiString ai_material_name;
        if (aiGetMaterialString(ai_material, AI_MATKEY_NAME, &ai_material_name) == AI_SUCCESS)
        {

            u32 uuid = (u32)(rand( ) + 1);

            char sm_material_name[256];
            snprintf(sm_material_name, 256, "%s%u", ai_material_name.data, uuid);

            struct aiColor4D ai_diffuse_color = {0};
            sm_vec4 sm_diffuse_color = sm_vec4_zero( );
            if (aiGetMaterialColor(ai_material, AI_MATKEY_COLOR_DIFFUSE, &ai_diffuse_color) == AI_SUCCESS)
            {
                sm_diffuse_color.r = ai_diffuse_color.r;
                sm_diffuse_color.g = ai_diffuse_color.g;
                sm_diffuse_color.b = ai_diffuse_color.b;
                sm_diffuse_color.a = ai_diffuse_color.a;
            }

            i32 ai_two_sided = 0;
            if (aiGetMaterialInteger(ai_material, AI_MATKEY_TWOSIDED, &ai_two_sided) != AI_SUCCESS)
            {
                printf("error getting twosided property\n");
            }

            sm_resource_material_s sm_material = {0};

            sm_rc_store(&sm_material.ref_count, 1);
            sm_material.header = sm_resource_header_new(SM_RESOURCE_TYPE_MATERIAL, uuid, sm_material_name);
            sm_material.diffuse_map = conversor_load_textures(ai_scene, ai_material);
            glm_vec4_copy(sm_diffuse_color.data, sm_material.diffuse_color.data);

            sm_resource_material_set_twosided(&sm_material, ai_two_sided);

            printf("material name: %s\n", sm_material.header.name);
            sm_resource_entity_s material_entt = sm___resource_mock_push(&sm_material, SM_RESOURCE_TYPE_MATERIAL);

            SM_ARRAY_PUSH(materials, material_entt);
        }
        else
        {
            SM_ARRAY_PUSH(materials, default_material);
        }
    }
}

void conversor_dummy(const struct aiScene *ai_scene)
{
    sm_resource_manager_init("assets/");

    sm_resource_entity_s dummy_entt = sm_resource_manager_get_by_name(ai_scene->mName.data, SM_RESOURCE_TYPE_SCENE);
    sm_resource_scene_s *scene_dummy = sm_resource_manager_get_data(dummy_entt);
    dummy_entt = sm_resource_scene_make_reference(dummy_entt);

    sm_resource_manager_update_cpu( );

    printf("==========================\n");
    sm_resource_scene_for_each(scene_dummy, 0, conversor_print_cb, scene_dummy->ecs);
    printf("==========================\n");

    sm_resource_manager_teardown( );
}

int main(int argc, char *argv[])
{

    // if (argc < 2)
    // {
    //     printf("Usage: %s <file.{obj,gltf}>\n", argv[0]);
    //     return 1;
    // }

    SM_STRING arg = "assets/exported/MainScene.gltf";
    // SM_STRING arg = argv[1];
    file_path = SM_STRING_DUP(arg);
    SM_STRING f_path = SM_STRING_DUP(arg);

    SM_STRING bsname = basename(f_path);
    SM_STRING drname = dirname(f_path);

    char buf[512] = {0};
    strcat(buf, drname);
    strcat(buf, "/");
    path = SM_STRING_DUP(buf);

    filename = SM_STRING_DUP(bsname);

    SM_STRING_FREE(f_path);
    stbi_set_flip_vertically_on_load(true);

    const struct aiScene *ai_scene = aiImportFile(file_path, aiProcessPreset_TargetRealtime_Fast | aiProcess_RemoveRedundantMaterials);
    if (!ai_scene || ai_scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !ai_scene->mRootNode)
    {
        printf("ASSIMP: %s\n", aiGetErrorString( ));
        exit(1);
    }

    // conversor_print_material_info(ai_scene);
    // print_materials(ai_scene->mRootNode, ai_scene);
    sm___resource_mock_init("assets/");
    conversor_load_materials(ai_scene);

    sm_resource_scene_s sm_scene = {0};
    sm_rc_store(&sm_scene.ref_count, 1);
    u32 sm_scene_uuid = (u32)(rand( ) + 1);

    sm_scene.header = sm_resource_header_new(SM_RESOURCE_TYPE_SCENE, sm_scene_uuid, ai_scene->mName.data);

    sm_ecs_world_s *ecs = sm_ecs_world_new( );
    if (!sm_ecs_world_ctor(ecs, SM_ALL_COMP))
    {
        printf("StrangeMachine: Failed to create ECS");
        exit(EXIT_FAILURE);
    }

    sm_scene.ecs = ecs;

    SM_STRING root_name = ai_scene->mRootNode->mName.data;
    u32 root_node = sm_resource_scene_get_root(&sm_scene);

    if (!(strcmp(root_name, "SM_SCENE_ROOT_NODE") == 0))
    {

        u32 n = sm_resource_scene_new_node(&sm_scene);
        if (sm_resource_scene_add_child(&sm_scene, root_node, n) == SM_SCENE_INVALID_NODE)
        {
            printf("Failed to add child");
            exit(EXIT_FAILURE);
        }
        root_node = n;
    }

    sm_resource_scene_set_name(&sm_scene, root_node, root_name);

    sm_user_data u_data;
    u_data.scene = &sm_scene;
    u_data.ecs = ecs;
    u_data.node = root_node;

    conversor_process_node(ai_scene->mRootNode, ai_scene, u_data);
    sm___resource_mock_push(&sm_scene, SM_RESOURCE_TYPE_SCENE);
    sm___resource_mock_write( );

    printf("==========================\n");
    sm_resource_scene_for_each(&sm_scene, 0, conversor_print_cb, ecs);
    printf("==========================\n");

    // sm_resource_texture_s *tst = sm___resource_get_texture_pointer( );
    // for (size_t i = 0; i < SM_ARRAY_LEN(tst); ++i)
    // {
    //     sm_resource_texture_s *t = &tst[i];
    //     stbi_write_png(t->header.name, t->width, t->height, 4, t->raw_data, 0);
    // }

    sm___resource_mock_teardown( );

    conversor_dummy(ai_scene);

    aiReleaseImport(ai_scene);

    sm_hashmap_for_each_str(images, conversor_images_dtor_cb, NULL);
    sm_hashmap_dtor_str(images);

    SM_ARRAY_DTOR(materials);

    SM_STRING_FREE(path);
    SM_STRING_FREE(file_path);
    SM_STRING_FREE(filename);

    return 0;
}
